package agent;

import features.*;

import Jama.Matrix;
import tetrisengine.*;
import java.io.*;
import java.util.*;


/*
 * A least-squares procedure for policy evaluation
 * using ordinary non-sparse samples
 * @see Samples 
 * @see FeatureExtractor
 */
public class LeastSquares extends PolicyEvaluationGeneric{
	public LeastSquares(FeatureExtractorGeneric f){
		super(f, false, false);
		setName("LS policy evaluation Agent");
	}
	
		
//	public void accumulateAb(List<TransitionPhiBinary> transits){};
	/* Accumulate the data structure of least-squares given samples
	 * @param transits samples collected 
	 */
	@Override
	public void accumulateAb(){
		System.out.println("accumulateAb:"+ getName()+ " --injected with " + transits.size() + " samples");
		assert(transits.size()>0);
		
		double[] phi;
		Matrix phiM;
		int epi=0;
		for(int s=0; s<transits.size(); s++){
			Transition tmp = transits.get(s);
			double reward = tmp.getReward();
			if(reward<0) {
				epi++;
				//System.out.println("Game " + epi+ " ends at "+ (s+1) + "th sample");
			}

			phi = getFeatureVector(tmp.getBoard());
			phiM= new Matrix(phi,1);//a row vector.
			phiM= phiM.transpose();
			//System.out.println("b.rows="+b.getRowDimension()+"; b.columns="+b.getColumnDimension());
			
			//b = b+ phi*return
			//A = A + phi*phi'
//			System.out.println("phiM.getRowDimension()="+phiM.getRowDimension());
//			System.out.println("phiM.getColumnDimension()="+phiM.getColumnDimension());
//			System.out.println("b.getRowDimension()="+b.getRowDimension());
//			System.out.println("b.getColumnDimension()="+b.getColumnDimension());
			b.plusEquals(phiM.times(ret.get(s)));
			A.plusEquals(phiM.times(phiM.transpose()));
		}
	}

    public void accumulateAbOffPolicy(){;}
    
    /*
	@Override
	public void solveWeightsAndEvaluate(){
		super.solveWeightsAndEvaluate();
		System.out.println(this);
	}*/
	
	
	@Override 
	public String toString(){
		String str="####Summary for " + getName() + ":\n";
		str += super.toString();
		return str;
	}
	
}