package agent;

import java.util.*;
import java.io.*;
import tetrisengine.TetrisAction;

public class Samples{
	List<Transition> allActionTrans = new ArrayList<Transition>();// the allActionTrans of taking all the actions at feature vectors phi
	List<Transition> onPolicyTrans = new ArrayList<Transition>();//the online allActionTrans following the policy
	
	public Samples(){};
	
	public void clear(){
		if(allActionTrans.size()>0){
			System.out.println("---------Clearing "+allActionTrans.size()+" all-action samples-------");
			allActionTrans.clear();
		}
		if(onPolicyTrans.size()>0){
			System.out.println("00000000000000000Clearing "+onPolicyTrans.size()+" on-policy samples00000000000000000");
			onPolicyTrans.clear();
		}
	}
	
	public List<Transition>  getAllActionTrans(){return allActionTrans;}
	public Transition  getAllActionTrans(int i){return allActionTrans.get(i);}
	public List<Transition>  getOnPolicyTrans(){return onPolicyTrans;}
	public Transition getOnPolicyTrans(int i){return onPolicyTrans.get(i);}
	
	
	public void addOnPolicy(Transition t){
		onPolicyTrans.add(t);
	}
	
	public void addAllAction(Transition t){
		allActionTrans.add(t);
	}
	
	
	public List<int[][]> getSampleBoards(){
		List<int[][]> res = new ArrayList<int[][]>();
		for(Transition trans: getOnPolicyTrans()){
			res.add(trans.getBoard());
		}
		return res;
	}

	public int[] getSamplePieces(){
		int[] res= new int[getNumOnPolicySamples()];
		int count=0;
		for(Transition trans: getOnPolicyTrans()){
			res[count++] = trans.getPiece();
		}
		return res;
	}

	
	/*compute return for each state in its episode for this agent; only computing for the first numTrainGames games*/
	public static List<Double> compReturn(List<Transition> transits){
		//List<Transition> transits = getOnPolicyTrans();
		List<Double> ret=new ArrayList<Double>();
		assert(transits.size()>0);
		for(int s=0; s<transits.size(); s++){
			double ret_s=0;
			for (int j=s; j<transits.size(); j++) {
				ret_s += transits.get(j).getReward();
				if(transits.get(j).getReward()<0){//ends this episode
					break;
				}
			}
			ret.add(ret_s);
		}
		return ret;
	}

	
	public int getNumAllActionSamples(){return allActionTrans.size();}
	public int getNumOnPolicySamples(){return onPolicyTrans.size();}
	
	public static void saveTransitions(String fileToSaveSamples, List<Transition> transits) throws IOException{
		//String filePathPhi=fileToSaveSamples + "phi.txt";
		String filePathPiece=fileToSaveSamples + "piece.txt";
		
		String filePathPolicy=fileToSaveSamples + "policy.txt";
		//String filePathPhinext=fileToSaveSamples + "phinext.txt";
		
		String filePathAction=fileToSaveSamples + "action.txt";
		String filePathReward=fileToSaveSamples + "reward.txt";
		
		String filePathBoard=fileToSaveSamples + "board.txt";
		//String filePathBoardBeforeErase=fileToSaveSamples + "boardBeforeErase.txt";
		//String filePathBoardAfterErase=fileToSaveSamples + "boardAfterErase.txt";
		
		//BufferedWriter fPhi = new BufferedWriter(new FileWriter(filePathPhi, true));
		BufferedWriter fPiece = new BufferedWriter(new FileWriter(filePathPiece, true));
		BufferedWriter fPolicy = new BufferedWriter(new FileWriter(filePathPolicy, true));
		//BufferedWriter fPhinext = new BufferedWriter(new FileWriter(filePathPhinext, true));
		BufferedWriter fAction = new BufferedWriter(new FileWriter(filePathAction, true));
		BufferedWriter fReward = new BufferedWriter(new FileWriter(filePathReward,true));
		BufferedWriter fBoard = new BufferedWriter(new FileWriter(filePathBoard, true));
		
		//BufferedWriter fPhinextBeforeErase = new BufferedWriter(new FileWriter(filePathPhinextBeforeErase, true));
		//BufferedWriter fBoardBeforeErase = new BufferedWriter(new FileWriter(filePathBoardBeforeErase, true));
		//BufferedWriter fBoardAfterErase = new BufferedWriter(new FileWriter(filePathBoardAfterErase, true));				
		
		for(Transition tp : transits){
			fPolicy.write(tp.getAction() + "\n");
			
			//for(double feature: tp.getPhi()){
//				fPhi.write(feature + " ");
//			}fPhi.write("\n");

			fPiece.write(tp.getPiece() + "\n");
			fAction.write(tp.getAction() + " " + TetrisAction.getPositionFromIndex(tp.getAction()) + " " + TetrisAction.getRotationFromIndex(tp.getAction()) + "\n");
			
			//for(double feature:tp.getPhinext()){
//				fPhinext.write(feature + " ");
//			}fPhinext.write("\n");
			
			//			for(double feature: tp.getPhinextBeforeErase()){
			//				fPhinextBeforeErase.write(feature + " ");
			//			}fPhinextBeforeErase.write("\n");
			
			
			fReward.write(Double.toString(tp.getReward()) + "\n");
			
			int[][] bd=tp.getBoard();
			for(int i=0; i<bd.length; i++){
				for(int j=0; j<bd[i].length; j++){
					fBoard.write(bd[i][j] + " ");//write the matrix in a row
				}
			}
			fBoard.write("\n");
			
			//			bd=tp.getBoardBeforeErase();
			//			for(int i=0; i<bd.length; i++){
			//				for(int j=0; j<bd[i].length; j++){
			//					fBoardBeforeErase.write(bd[i][j] + " ");//write the matrix in a row
			//				}
			//			}
			//			fBoardBeforeErase.write("\n");
			
			//bd=tp.getBoardAfterErase();
			//			for(int i=0; i<bd.length; i++){
			//				for(int j=0; j<bd[i].length; j++){
			//					fBoardAfterErase.write(bd[i][j] + " ");
			//				}
			//			}
			//			fBoardAfterErase.write("\n");
			
		}
		fPolicy.close();
		//fPhi.close();
		fPiece.close();
		fAction.close();
		//fPhinext.close();
		fReward.close();
		fBoard.close();
		
		//fPhinextBeforeErase.close();fBoardBeforeErase.close();
		//fBoardAfterErase.close();
	}
	
};
