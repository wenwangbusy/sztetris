package agent;

import tetrisengine.*;
import features.*;

import java.lang.*;
import java.io.Serializable;

/* this is a condensed of Transition, when the features are binary and sparse
 @see Transition
 */
public class TransitionPhiBinary extends TransitionGeneric{
	int[] phiIndex;//feature indices for the current board
    int[] phinextIndex; ////feature indices for the next board
	
    FeatureExtractorGeneric fex;
    
	public TransitionPhiBinary(){};
	
	public TransitionPhiBinary(FeatureExtractorGeneric fexGiven, int piece, int[] ind, int action, int[] indnext, double reward, boolean absorb){
        super(piece,action, reward, absorb);
        fex = fexGiven;
		phiIndex=new int[ind.length];
		phinextIndex = new int[indnext.length];
		System.arraycopy(ind, 0, phiIndex,0, ind.length);
		System.arraycopy(indnext, 0, phinextIndex,0, indnext.length);
	}
	
	public TransitionPhiBinary(FeatureExtractorGeneric fexGiven, Transition t){
        super(t.getPiece(), t.getAction(), t.getReward(), t.getAbsorb());
        fex=fexGiven;
		phiIndex = ((BinaryFeatureExtractor)fex).getFeatureIndices(t.getBoard());
		phinextIndex = ((BinaryFeatureExtractor)fex).getFeatureIndices(t.getBoardNext());
	}
	
	public int[] getPhiIndex(){return phiIndex;}
	public int[] getPhinextIndex(){return phinextIndex;}
	
	
	public double getV(double [] w){
		return ((BinaryFeatureExtractor)fex).getValue(phiIndex, w);
	}
	
	public double getVnext(double [] w){
		return ((BinaryFeatureExtractor)fex).getValue(phinextIndex, w);
	}
}