package agent;

import tetrisengine.*;

import java.util.*;




/* Transition in RL is in the form of (phi, a, phinext, reward, absorb)
 Here in Tetris, it is 
 (board, piece, tA(position, rotation), boardBeforeErase, boardNext, reward, absorb), wherein boardNext is the board after erasing lines
 @see AfterState
 */
public class Transition extends TransitionGeneric{
	int[][] board;//current board
	int[][] boardNext;//the next board, e.g. after puting tile and after (or before) erasing lines
	
	public Transition(){super();}
	
	public Transition(int[][]bd, int piece, int action, int[][] bdNext, double reward, boolean absorb){
		super(piece, action, reward, absorb);
		board=new int[TetrisGame.getWidth() + 2*TetrisGame.getPADDING()][TetrisGame.getHeight() + 2*TetrisGame.getPADDING()];
		setBoard(bd);
		
		boardNext=new int[TetrisGame.getWidth() + 2*TetrisGame.getPADDING()][TetrisGame.getHeight() + 2*TetrisGame.getPADDING()];
		setBoardNext(bdNext);
	}
	
	public Transition(int[][]bd, int piece, int action, AfterState[] bdNext, double reward, boolean absorb){
		this(bd, piece, action, bdNext[1].getBoard(), reward, absorb);//after erasing
		//this(bd, piece, tA, bdNext[0].getBoard(), reward, absorb);//before erasing
	}
			
	public Transition(Transition t){
		this(t.getBoard(), t.getPiece(),t.getAction(), t.getBoardNext(), t.getReward(), t.getAbsorb());
	}

	public void setBoard(int [][]bd){
		assert(bd!=null && board!=null);
		AfterState.copyBoard(bd, board);
	}

	public void setBoardNext(int [][]bd){
		assert(bd!=null && boardNext!=null);
		AfterState.copyBoard(bd, boardNext);
	}
	
	public int[][] getBoard(){return board;}
	public int[][] getBoardNext(){return boardNext;}
		
    
    @Override
    public String toString(){
        return AfterState.board2Chars(board);
    }
	
}