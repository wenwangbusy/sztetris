package agent;

import tetrisengine.*;
import features.*;

import Jama.Matrix;
import java.io.*;
import java.util.*;


/* bug: The LSTD1 A b should match that of LS. but this is not. 
 * However, the weight vectors of the two methhods are the same. 
 
 * A least-squares temporal difference learning agent for policy evaluation
 * using ordinary non-sparse-feature samples
 * LSTD(1) is the same as LeastSquares
 * @see Samples
 * @see LeastSquares
 * @see FeatureExtractor
 */
public class LSTD extends PolicyEvaluationGeneric{

	boolean debug=false;//true;
	
	/*data structures for the algorithm */
	double lambda;
	
	public LSTD(){};
	
	/* @param fa the agent that collects samples
	 * @param numTrainGames number of games to train this agent
	 */
	public LSTD(FeatureExtractorGeneric f, double lam){
		super(f, false, false);
		setName("LSTD policy evaluation Agent");
		setLambda(lam);
	}
	
	public void setLambda(double lam){
		lambda = lam;
	}
	
    
	@Override
	public void accumulateAb(){
		System.out.println("accumulateAb:"+ getName()+ " --injected with " + transits.size() + " samples");
		int epi=0;
		Matrix z=new Matrix(getNumFeatures(), 1);//eligibility trace vector
		
		//least-squares structures for verification
		Matrix A_ls=null, b_ls =null; 
		if(debug){
			b_ls= new Matrix(getNumFeatures(),1);
			A_ls  = new Matrix(getNumFeatures(),getNumFeatures());
		}
		
		double[] phi, phinext;
		double reward;
		Matrix phiM, phinextM;
		Transition transition_s;//current transition sample
		int begin=-1;
		int end=-1;//begin and end states of the current episode
		
		for(int s=0; s<transits.size(); s++){
			if(s==0){//the first episode
				begin = 0;
			}
			
			transition_s = transits.get(s);
			
			//given the current transition, increment A, b 
			reward = transition_s.getReward();
			phi = getFeatureVector(transition_s.getBoard());
			phiM= new Matrix(phi,1);phiM= phiM.transpose();
			if(s==begin){
				z = phiM;			
			}
			if(reward<0) {//indicating the end of the current episode
				phinextM = new Matrix(getNumFeatures(), 1);//zerolize the terminal features
				end = s;
				begin = s + 1;
				epi++;
				//System.out.println("Episode " + epi+ " ends at "+ (s+1) + "th sample");
				
				if(debug){
					//should be 0; but not!
					System.out.println("the error to least-squares b is " + (b_ls.minus(b)).norm1());
					System.out.println("the error to least-squares A is " + (A_ls.minus(A)).norm1());
					for(int bInd=0;bInd<b.getRowDimension(); bInd++){ 
						if( (b_ls.get(bInd,0)==0 && b.get(bInd,0)!=0) || (b_ls.get(bInd,0)!=0 && b.get(bInd,0)==0) ){
							System.out.print(String.format("b_ls[%d]=%f; ",bInd, b_ls.get(bInd,0)));
							System.out.println(String.format("b[%d]=%f",  bInd, b.get(bInd,0)));
						}
					}
				}
			}else{
				//phinext = getFeatureVector(transition_s.getBoardAfterErase(), 0.0, 4);//the last two arguments do matter for now
				phinext = getFeatureVector(transits.get(s+1).getBoard());
				phinextM= new Matrix(phinext,1);phinextM= phinextM.transpose();	
			}
			b.plusEquals(z.times(reward));
			A.plusEquals( z.times( phiM.minus(phinextM).transpose() ) );
			
			
			if(debug){
				b_ls.plusEquals(phiM.times(ret.get(s)));
				A_ls.plusEquals(phiM.times(phiM.transpose()));
			}
			
			//z = lam*z + phinext 
			z.timesEquals(lambda);
			z.plusEquals(phinextM);
		}
		
	}
	
    /* Take the greedy action at each state board in the transits; observing the next state board and reward.
        Accumulate the A, b given this experiece
     */
    public void accumulateAbOffPolicyBinary(){
        ;
    }
    
    /*
	@Override
	public void solveWeightsAndEvaluate(boolean printBoard){
		super.solveWeightsAndEvaluate(printBoard);
		System.out.println(this);
	}*/
		
	
//	/* reset the trace vector to the first feature vector of some episode
//	 @param phi0 the first feature vector of some episode
//	 */
	//not working. z is still 0 after calling this function. Why? because z will still point to the original object afterward.
//	public void resetTrace(double[] phi0){
//		z = new Matrix(phi0,1);z= z.transpose();
//		System.out.println("in resetTrace, z.norm="+ z.norm1());
//	}

	
	@Override
	public String toString(){
		String str="####Summary for " + getName() + " with lambda="+ lambda + ":\n";
		str += super.toString();
		return str;
	}
	
}