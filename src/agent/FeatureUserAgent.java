package agent;


import tetrisengine.*;
import features.*;

import java.io.*;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.*;

/* update: remove samplesBinary and fex2
 
 /*

/** This is a general agent that is based on function approximation; it has a feature extractor, and a weight vector
 it also has a sensation of the current state of the game
 */
public class FeatureUserAgent extends TetrisAgent {
	/*Policy structures of the agent */
	FeatureExtractorGeneric fex;//features for the agent
    double[] w;//policy parameter vector of the agent
	
	double gamma = 1.0;// 0.1;//1.0;//0.9
	
	/*epsilon greedy parameter */
	double epsilon=0;
	
    /*
	FeatureExtractorGeneric fex2 = new BinaryBertsekasIoffeFeatureExtractorTabular();//features extractor applied to saving the samples
	SamplesBinary samplesBinary;
    */
    
	Samples samples;
	{
        /*
		if(samplesBinary==null)
		{
			samplesBinary=new SamplesBinary();
			System.out.println("samplesBin is null, new it");
		}*/
		if(samples==null)
			samples =new Samples();
	}
	
	String fileToSaveSamples;
	boolean saveOnPolicySamples=false;//if saveOnPolicySamples, will save board
	boolean saveAllActionSamples=false;//whether saving the samples of all the actions
	
	//Random rnd = new Random();
	
	public void initialize(FeatureExtractorGeneric fex, boolean saveOnPolicySamples, boolean saveAllActionSamples){
		this.fex = fex;
		resetWeights();
		//randomizeWeights(w);
		
		setSaveOnPolicySamples(saveOnPolicySamples);
		setSaveAllActionSamples(saveAllActionSamples);
		if(getSaveOnPolicySamples() || getSaveAllActionSamples()){
            System.out.println("Saving samples in non-sparse format");
		}
		//System.out.println("Discount factor was set to " + gamma);
		
	}

	public FeatureUserAgent(){};
	
	public FeatureUserAgent(FeatureExtractorGeneric fex, boolean saveOnPolicySamples, boolean saveAllActionSamples){
		System.out.println("Using " + fex.getName() + " features");
		initialize(fex, saveOnPolicySamples, saveAllActionSamples);
	}
	public FeatureUserAgent(String name, FeatureExtractorGeneric fex, boolean saveOnPolicySamples, boolean saveAllActionSamples){
		super(name);
		System.out.println("Using " + fex.getName() + " features");
		initialize(fex, saveOnPolicySamples, saveAllActionSamples);
	}
    
	public void setFileToSaveSamples(String file){fileToSaveSamples = file;}
    
	public double getGamma(){return gamma;}
	public double getEpsilon(){return epsilon;}
	public void setEpsilon(double e){
        epsilon = e;
        if(getEpsilon()>0)
            System.out.println("Using epsion-greedy exploration: epsilon="+ getEpsilon());
    }
	
	/*
	 @param useBinarySamples use binary samples or not
	 */
    public FeatureUserAgent(String name, FeatureExtractorGeneric fex, String weightFile, boolean saveOnPolicySamples, boolean saveAllActionSamples)
    {
		initialize(fex, saveOnPolicySamples, saveAllActionSamples);
		System.out.println("running agent with features=" + fex.getName());
		randomizeWeights();
        if(!weightFile.equals("")){
            try {
                if(name.equals("CE")){
                    loadCEWeights(weightFile);
                }else if(name.equals("Random")){
                    System.out.println("Random agent has not weight file, not reading");
                }else{
                    loadWeights(weightFile);
                }
            }catch (IOException e) {
                throw new RuntimeException("error reading weight file");
            }
            
            System.out.println("After reading from the weight file, the weight vector is " + getWeights().length + " dimensional:");
            for(int i=0; i<getWeights().length; i++){
                System.out.printf(getWeight(i)+ " ");
            }System.out.println("");
        }else{
            System.out.println("Trainer CE mode doesn't have to load weight file");
        }
    }
	
	public void resetWeights(){
		System.out.println("setting weights to 0");
		w = new double[getNumFeatures()];
	}
	//static Random rnd = new Random();//seed will be the same
	public void randomizeWeights(){
        Random rnd = new Random();
		System.out.println("Initializing weights to random");
		for (int i=0; i<getNumFeatures(); i++){
			w[i]= rnd.nextDouble();//[-1,0]
            //w[i]= -rnd.nextDouble();//[0,1]
        }
	}
	public double getWeight(int i){return w[i];} 
	public void setWeight(int i, double wi){w[i]=wi;} 
	public double[] getWeights(){return w;}
    public void setWeights(double[] w2){
        assert(w.length>0);
        /*for (int i=0; i<getNumFeatures(); i++){
			setWeight(i, w2[i]);
        }*/
        System.arraycopy(w2,0, w,0, w2.length);
    }
	@Override
	public String toString(){
		String res="";
		for(int i=0; i<w.length; i++){
			res = res + w[i] + " ";
		}
		return res;
	}
	public void printWeights(){
		for(int i=0; i<w.length-1; i++){
			System.out.print(w[i] + " ");
		}System.out.println(w[w.length-1]);
	}
	
//	/*Adjust the weight vector according to some method
//	 @param s current state
//	 @param piece current piece
//	 @param stateNext the next state
//	 @param reward the reward or return until stateNext
//	*/
//	public void adjustWeights(AfterState s, int piece, boolean absorb, AfterState stateNextS, double reward){};
	
	/* get the number of actions */
	public int getNactions(){return TetrisGame.getNactions();}
	public void setFeatureExtractor(FeatureExtractorGeneric f){fex=f;}
	public FeatureExtractorGeneric getFeatureExtractor(){return fex;}
	public int getNumFeatures(){return fex.getNumFeatures(); }
	public double[] getFeatureVector(int[][] state){return ((FeatureExtractor)getFeatureExtractor()).getFeatures(state);}
	public int[] getFeatureIndices(int[][] state){
		//System.out.println("Feature is " + getFeatureExtractor().getName());
		return ((SparseFeatureExtractor)getFeatureExtractor()).getFeatureIndices(state);
	}
	public double getValue(int[][] board, double[] weights){
		return fex.getValue(board, weights);
	}
	
    
	public  boolean getSaveOnPolicySamples(){return saveOnPolicySamples;}
	public void setSaveOnPolicySamples(boolean b){saveOnPolicySamples=b;}
	public boolean getSaveAllActionSamples(){return saveAllActionSamples;}
	public void setSaveAllActionSamples(boolean b){saveAllActionSamples=b;}
	
	public Samples getSamples(){return samples;}
	public void clearSamples(){
        System.out.println("FeatureUserAgent:clearSamples()");
        samples.clear();}
	public void setSamples(Samples sam){samples=sam;}
	
	/* Save samples into files according to getSaveAllActionSamples() and getSaveOnPolicySamples()
	 @param fileToSaveSamples header of the files 
	 @throw IOException if fails in writing
	 */
    public void saveSamples(){
		boolean cEsampleOnly=true;
		assert(fileToSaveSamples.length()>0);
	    try{
			if(getSaveAllActionSamples()){
				assert(samples.getNumAllActionSamples()>0);
				System.out.println("There are " +samples.getNumAllActionSamples() + " all-action samples that will be saved to files.");
				Samples.saveTransitions(fileToSaveSamples + "allActions_", samples.getAllActionTrans());
			}
			
			if(getSaveOnPolicySamples()){
				assert(samples.getNumOnPolicySamples()>0);
				System.out.println("There are " + samples.getNumOnPolicySamples() + " on policy samples that will be saved to files.");
				Samples.saveTransitions(fileToSaveSamples + "policyOnly_", samples.getOnPolicyTrans());
				
				//save BertsekasIoffeFeatureExtractorTabular features
//				BertsekasIoffeFeatureExtractorTabular biTab = new BertsekasIoffeFeatureExtractorTabular();
//				String filePathPhi=fileToSaveSamples + "policyOnly_phiBITab.txt";
//				saveFeatures(filePathPhi, biTab, samples.onPolicyTrans);
			}
			samples.clear();
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//public void saveFeatures(String filePathPhi, FeatureExtractor fex, List<Transition> trans) throws IOException{
//		BufferedWriter fPhi = new BufferedWriter(new FileWriter(filePathPhi, true));
//		double[] phi;
//		for(Transition tp : trans){
//			phi = fex.getFeatures(tp.getBoard(), 0.0, 4);
//			for(double feature: phi){
//				fPhi.write(feature + " ");
//			}
//			fPhi.write("\n");
//		}
//		fPhi.close();
//	}
	
	/* save the weight vector
	 	 @param weightFile name of the file that saves the weights
	 */
	public void saveWeights(String weightFile) throws IOException{
		BufferedWriter out;
		try{
			out = new BufferedWriter(new FileWriter(weightFile));
			for (int i=0; i<getNumFeatures()-1; i++){
				out.write(w[i] + " ");
			}
			out.write(w[getNumFeatures()-1]+"\n");
			out.close();
		}catch (IOException e) {
			throw e;
		}	
	}
	
	
	/* Load the weight vector (policy parameter vector) which is produced by some learning algorithm
	 @param weightFile name of the file that saved the weights
	 */
	public void loadWeights(String weightFile) throws IOException
    {
		BufferedReader in;
		try{
			in = new BufferedReader(new FileReader(weightFile));
			String s=in.readLine();
			String[] slist = s.split(" +");
			//System.out.println("slist.length=" + slist.length);
			assert(slist.length==getNumFeatures());
			for (int i=0; i<getNumFeatures(); i++){
				w[i] = Double.parseDouble(slist[i]);
				//System.out.println(slist[i] + ": w[" + i + "]=" + w[i] + " ");
			}
			in.close();
		}catch (IOException e) {
			 //System.out.println("File not found. Using random values.");
			throw e;
		}	
	}
	
	/* Evaluate the agent in a number of games
	 */
	public double evaluate(int nGames, boolean printBoard, boolean printMessage){
		double score_av=0; 
		for(int t=0; t<nGames;t++){
			score_av += TetrisGame.playOneGame(this, printBoard);
		}
		score_av /= nGames;
        if(printMessage)
            System.out.println("in evaluate(), Average score is "+ score_av + " in "+nGames+" games.");
		return score_av;
	}
    public double evaluateRandomRestart(int nGames, boolean printBoard){
		double score_av=0;
        AfterState start;
		for(int t=0; t<nGames;t++){
            start = AfterState.getRandomState();//AfterState.getRandomState() not implemented yet
			score_av += TetrisGame.playOneGame(this, start, printBoard);
		}
		score_av /= nGames;
        System.out.println("in evaluate(), Average score is "+ score_av + " in "+nGames+" games.");
		return score_av;
	}
    //collect samples from a random state from the current samples except for the 1st episode
    public double evaluateCascadingRestart(int nGames, boolean printBoard){
		double score_av=0;
        AfterState start;
        int indexCollectFrom=10;
		for(int t=0; t<nGames;t++){
            if(t>indexCollectFrom){
                System.out.println("episode "+t+", start collecting from a randoms sample. ");
                start = getRandomStateFromSamples();
                score_av += TetrisGame.playOneGame(this, start, printBoard);
            }else{
                System.out.println("episode "+t+", start collecting from the empty board. ");
                score_av += TetrisGame.playOneGame(this, printBoard);
            }
		}
		score_av /= nGames;
        System.out.println("in evaluate(), Average score is "+ score_av + " in "+nGames+" games.");
		return score_av;
	}
    public AfterState getRandomStateFromSamples(){
        List<Transition> trans = getSamples().getOnPolicyTrans();
        Random generator = new Random();
        int index = generator.nextInt(trans.size());
        return new AfterState(trans.get(index).getBoard());
    }
    
    
	/* Load the weight vector (policy parameter vector) which is produced by the CE method
	 @param weightFile name of the file that saves the weights previously
	 @throw IOException 
	 */
    public void loadCEWeights(String weightFile) throws IOException
    {
    	int i, j;
        BufferedReader in=null;

        boolean fileexists = (new File(weightFile)).exists();
        String[] slist;
        if (fileexists)
        {
            try {
                String s, lasts;
                int k = 0;

                try {
                    in = new BufferedReader(new FileReader(weightFile));
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(TetrisGame.class.getName()).log(Level.SEVERE, null, ex);
                }
                s = in.readLine();
                do
                {
                    k++;
                    lasts = s;
                    s = in.readLine();
                } while (s!=null);
                lasts = lasts.replaceAll("  ", " ");
                lasts = lasts.replaceAll("  ", " ");
                lasts = lasts.replaceAll("  ", " ");
                lasts = lasts.replaceAll("  ", " ");
//                System.out.println(lasts);
                slist = lasts.split(" ");
//                for (i=0; i<slist.length; i++)
//                    System.out.print(slist[i]+"; ");
//                System.out.println();
                for (i=0; i<getNumFeatures(); i++)
                    w[i] = Double.parseDouble(slist[i+3]);
                System.out.println("weights Initialized.");
            } catch (IOException ex) {
                //Logger.getLogger(TetrisGame.class.getName()).log(Level.SEVERE, null, ex);
				 System.out.println("File not found. Using random values.");
				throw ex;
            }
        }
        else
        {
            System.out.println("File not found. Using random values.");
			throw new IOException();
        }
    }	

    /* @return TetrisAction the greedy action according to the policy*/
    public int act(){ return putTileGreedy(getPiece());}

	/*Update the agent state and the piece of the game */
	public void getObservation(double reward, int piece, AfterState state){
		setAgentState(state);
		setPiece(piece);
	}
	
	/*Observe the current state and piece, and select an action according to the policy */
	@Override
	public int observeAndAct(double reward, int piece, int[][] board){
		getObservation(reward, piece, new AfterState(board));
		return act();//put piece
	}

	
	@Override
   public  void GameOver(){}

	/**Given the current state and piece, put a tile according to the "best" according to the linearly approximate value function of the policy. 
	 * The goodness is evaluated by the board after the action, which based on Bellman equation
	 @param piece current piece
	 @return TetrisAction the greedy action
will add exploration here
	 */
    public int putTileGreedy(int piece)
    {
        Random rnd = new Random();
        //type = rnd.nextInt(7);
        //UpdateSkyline();
		
		AfterState s = new AfterState(getAgentState());
		//double Vs=getValue(s.getBoard(), getWeights());
		//System.out.println("Vs = " + Vs);
		
//		double[] phi= getFeatureVector(s.getBoard()); 
//		System.out.println("phi = ");
//		for(int i=0; i<phi.length; i++){
//			System.out.println(phi[i]);
//		}
		
        AfterState[] nextsS;
		AfterState nextsBeforeErase;
		AfterState nexts;
		
		double res=TetrisGame.getINF();
		boolean absorb=false;
        double maxVnext = TetrisGame.getINF();
		double value=0;
        Transition trans=null;
		int aind=-1;
		int bestActionInd=-1;
		Transition bestTransition=null;
        
        //System.out.println("getSaveOnPolicySamples()="+getSaveOnPolicySamples());
        
        for (int pos=0; pos<TetrisGame.getWidth(); pos++){
            for (int rot=0; rot<TetrisGame.getNrotations(); rot++){
                aind++;
                nextsS = s.putTile(piece, rot, pos, false);
                nextsBeforeErase = nextsS[0];
                nexts = nextsS[1];//state after erasing
                res = nexts.getReward();
                
                if (res>=0){
                    double Vnext1= getValue(nexts.getBoard(), getWeights());
//					double Vnext2= getValue(nexts.getBoard(), res, 5), getWeights());
//                    value = res + (Vnext1+Vnext2)/2;
                    value = res + Vnext1;
                    absorb = false;
                }
                else{
                    value = TetrisGame.getINF();
                    absorb=true;
                }
                
                //System.out.println(String.format("action=%d, pos=%d, rot=%d, Vsnext = %f\n", aind, pos, rot, value));
                
                if(getSaveAllActionSamples()){
                    trans = new Transition(s.getBoard(), piece, aind, nextsS, res, absorb);
                    samples.addAllAction(trans);
                }
                
                if (value>maxVnext){
                    maxVnext = value;
                    bestActionInd = aind;
                    
                    if(getSaveOnPolicySamples()){
                        if(getSaveAllActionSamples()){//take advantage of previously computed data structure
                            bestTransition = trans;
                        }else{//have to construct the transition
                            bestTransition =  new Transition(s.getBoard(), piece, bestActionInd, nextsS, res, absorb);
                        }
                    }
                }
            }
        }

		
		//reset the best action if using epsilon greedy
		if(rnd.nextDouble()<getEpsilon()){
			int p = rnd.nextInt(TetrisGame.getWidth());
			int r = rnd.nextInt(TetrisGame.getNrotations());
            bestActionInd = TetrisAction.getIndex(p, r);
			nextsS = s.putTile(piece, r, p, false);
			nextsBeforeErase = nextsS[0];
			nexts = nextsS[1];
			res = nexts.getReward();
 
            bestTransition = new Transition(s.getBoard(), piece, bestActionInd, nextsS, res, res<0);
		}else{
			if(bestActionInd==-1){
				//System.out.println(String.format("this state is terminating whatever action to take, largest Vsnext is %f",  maxVnext));
                bestActionInd=0;
				assert((int)res==-1 && absorb);
                //System.out.println("getSaveOnPolicySamples()="+getSaveOnPolicySamples());
				if(getSaveOnPolicySamples()){
                    //System.out.println("Constructing a fake bestTransition at terminal states");
                    nextsS=new AfterState[2];
                    nextsS[0]=s;
                    nextsS[1]=s;
                    bestTransition = new Transition(s.getBoard(), piece, bestActionInd, nextsS, res, absorb);
				}
			}	
		}
					
		if(getSaveOnPolicySamples()){
            samples.addOnPolicy(bestTransition);
		}
        if(bestActionInd<0 || bestActionInd>= TetrisGame.getNactions()){
            System.out.println("bestActionInd="+bestActionInd);
        }
        assert(bestActionInd>=0 && bestActionInd<TetrisGame.getNactions());
        return bestActionInd;
    }
	
	
	
	
}
