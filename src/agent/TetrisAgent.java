/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package agent;

//import java.util.ArrayList;
import tetrisengine.AfterState;
import tetrisengine.TetrisAction;
import tetrisengine.TetrisGame;

/**
 *
 * @author istvanszita
 */
public abstract class TetrisAgent {
    private String name;
    //private static int width, height;//what is this? same as in TetrisGame? yes
	private int piece;//current picece: will move to TetrisAgent
	
    //private int[][] agentstate;//this is board info; bad name
    //public int skyline[];
	AfterState agentState;

    public TetrisAgent()
    {
		int width = TetrisGame.getWidth();
		int height=TetrisGame.getHeight();
        //agentstate = new int[width + 2*TetrisGame.getPADDING()][height + 2*TetrisGame.getPADDING()];
        //skyline = new int[width + 2*TetrisGame.getPADDING()];
		agentState=new AfterState();
    }
	
	public TetrisAgent(String n){
		this();
		name=n;
	}
	
	//this ignores piece. bad design
//    public void newGame(int piece, int[][] state)
//    {
//        AfterState.copyBoard(state, agentstate);
//    }
//	  public void getObservation(double reward, int piece, int[][] state)
//    {
//        AfterState.copyBoard(state, agentstate);
//    }
	
	
	public void setAgentState(AfterState state){agentState=new AfterState(state);}
	public AfterState getAgentState(){return agentState;}
	
	public void setName(String name){this.name=name;}
	public String getName(){return name;}
	public void setPiece(int piece){this.piece=piece;}
	public int getPiece(){return piece;}
	
	
	//the most important method for an agent, each agent must have way to choose an action
    //public abstract TetrisAction act();
	public abstract int observeAndAct(double reward, int piece, int[][] state);
	
    public abstract void GameOver();


}
