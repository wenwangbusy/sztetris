package agent;

import tetrisengine.*;

import java.io.Serializable;
import java.util.*;




/* A very basic class for all transitions */
public class TransitionGeneric implements Serializable{
	int piece;//current piece 
	int action;
	double reward;
	boolean absorb;

	public TransitionGeneric(){};
	
	public TransitionGeneric(int piece, int action, double reward, boolean absorb){
		this.piece=piece;
        this.action=action;
		this.reward= reward;
		this.absorb= absorb;
	}
	
	public TransitionGeneric(Transition t){
		this(t.getPiece(),t.getAction(), t.getReward(), t.getAbsorb());
	}

	public int getPiece(){return piece;}
	public int getAction(){return action;}
	public double getReward(){return reward;}
	public boolean getAbsorb(){return absorb;}
	
    @Override
	public String toString(){
		String res="";
		res += ("piece=" + piece + "\n");
		res += ("reward= " + reward + "\n");
		res += ("absorb=" + absorb + "\n");
		return res;
	}
}