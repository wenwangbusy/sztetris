package agent;

import tetrisengine.*;
import features.*;

import Jama.Matrix;
import java.io.*;
import java.util.*;

/* A general policy evaluation algorithm class
 
 */
public abstract class PolicyEvaluationGeneric extends FeatureUserAgent{
    /*data structures for the algorithm */
	protected List<Transition>transits;
    
    /*Training and testing parameters */
	//if delta=0 then matrix is singular
	static final double[] deltaA={500};//api offline first time used for the figure
	//static final double[] deltaA={1, 10, 100, 1e3, 1e4};
	//static final double[] deltaA={1e2, 1e3, 1e4, 1e5};
	
	/*data structures for the algorithm */
	protected Matrix A, b;
		
	double[][] weightsDeltaA = new double[deltaA.length][getNumFeatures()];
	
	List<Double> ret;//holding the return of the policy for every state until the end of the episode it is in
	
	double deltaBestRMSE; 
	double bestRMSE;
	
	double deltaBestScore;
	double bestScore;
	
	static final int numTestGames = 200;//100;//200; //number of episodes for testing the learned policy
	
	/*results for the algorithm */
	double[] scores=new double[deltaA.length];
	double[] rmse=new double[deltaA.length];//scores and rmse for delta's
	
	public PolicyEvaluationGeneric(){
        transits=new ArrayList<Transition>();
    }
	
	/* @param fex features used for policy evaluation evaluate 
	 */
	public PolicyEvaluationGeneric(FeatureExtractorGeneric f, boolean saveOnPolicySamples, boolean saveAllActionSamples){
		super(f, saveOnPolicySamples, saveAllActionSamples);//not saving any sample for policy evaluation agent
		transits=new ArrayList<Transition>();
        A = new Matrix(getNumFeatures(), getNumFeatures());
		b = new Matrix(getNumFeatures(),1);
		
		System.out.println("deltaA=");
		for(int d=0; d<deltaA.length;d++){
			System.out.print(deltaA[d] + " ");
		}System.out.println("");
		System.out.println("numTestTames=" + numTestGames);
	}
	
	public PolicyEvaluationGeneric(FeatureExtractorGeneric f, boolean saveOnPolicySamples, boolean saveAllActionSamples, List<Transition> transits){
		this(f, saveOnPolicySamples, saveAllActionSamples);
		this.transits = transits;
	}
    
    public void setTransits(List<Transition> given){
        transits= given;
    }
    public List<Transition> getTransits(){
		return transits;
	}
    
    public List<Transition> gettransitsInOnlineSamples(){
		return getSamples().getOnPolicyTrans();
	}
    
	public List<Double> getReturns(){return ret;}
	
	public Matrix getA(){return A;}
	public Matrix getb(){return b;}
	public void resetA(){A=new Matrix(getNumFeatures(), getNumFeatures());}
	public void resetb(){b=new Matrix(getNumFeatures(), 1);}	
	
    public double getb(int x){
        return b.get(x,0);
    }
    public void setb(int x, double val){
        b.set(x, 0, val);
    }
    public double getA(int x, int y){
        return A.get(x,y);
    }
    public void setA(int x, int y, double val){
        A.set(x,y,val);
    }
    
	public static double[] getDeltaA(){return deltaA;}
	
	public double[] getScores(){return scores;}
	public double[] getRMSEs(){return rmse;}

	
	public static int getNumTestGames(){return numTestGames;}
	
	
	/* Solve the linear system, Ax=b, with L2 regularization, factors provided in deltaA
	 * should split evaluation out in the future!
	 */
	public void solveWeightsAndEvaluate(boolean printBoard){
		//System.out.println("\n\n#Evaluating the policy using " + getName() + " with "+ getFeatureExtractor().getName() + " features");
		for(int i=0; i<deltaA.length; i++){
			System.out.println("####deltaA = " + deltaA[i]);
			solveWeights(deltaA[i]);
//			scores[i]=evaluate(getNumTestGames(), false, true);
            scores[i]=evaluate(getNumTestGames(), printBoard, true);
			//rmse[i]=printApproximation();//seems useful. need to add back
		}
		compBestScore();
		compBestRMSE();
	}
	
	public void solveWeights(double delta){
		Matrix A1 = A.plus((Matrix.identity(getNumFeatures(), getNumFeatures())).times(delta)); 
		//System.out.println("Solving the linear system: condition number is "+ A1.cond());
        double[] w = (A1.solve(b)).getColumnPackedCopy();
        setWeights(w);
	}
	
	public double[][] getWeightsDeltaA(){return weightsDeltaA;}
	
	public double getBestScore(){return bestScore;}
	public double getDeltaBestScore(){return deltaBestScore;}
	
	public double getBestRMSE(){return bestRMSE;}
	public double getDeltaBestRMSE(){return deltaBestRMSE;}
	
	
	
	/* Get the largest score across delta */
	public void compBestScore(){
		bestScore = TetrisGame.getINF();
		for(int i=0; i<scores.length; i++){
			if(scores[i]>bestScore){
				bestScore=scores[i];
				deltaBestScore = deltaA[i];
			}
		}
	}
	
	/* Get the minimum rmse across delta */
	public void compBestRMSE(){
		bestRMSE = -TetrisGame.getINF();
		for(int i=0; i<rmse.length; i++){
			if(rmse[i]<bestRMSE){
				bestRMSE=rmse[i];
				deltaBestRMSE =deltaA[i];
			}
		}
	}
	
	/*
	 Print the policy evaluation error
	 */
	public double printApproximation(List<Transition> transits){
		ret = Samples.compReturn(transits);
		int epi=0;
		//double[] phi;
		double err=0;
		int count=0;
        Transition tran=null;
		for(int s=0; s<transits.size(); s++){
			count++;
            tran = transits.get(s);
			double reward = tran.getReward();
			if(reward<0) epi++;
            
			double appr = ((FeatureExtractor)getFeatureExtractor()).getValue(tran.getBoard(), getWeights());
			//System.out.println("true return: " + ret.get(s) + " approximation: " + appr);
			err += (ret.get(s) - appr)*(ret.get(s) - appr);
		}
		System.out.println("used for training: there are " + epi + " episodes and " + count + " samples");
		return Math.sqrt(err/count);
	}
    
    
	@Override
	public String toString(){
		String str= "--Scores:";
		for(int i=0; i<scores.length; i++){
			str += scores[i] + ", ";
		}
		str += "\n--RMSEs:";
		for(int i=0; i<rmse.length; i++){
			str += rmse[i] + ", ";
		}
		str +=  "\n--best delta: " + deltaBestScore + "\n";
		str += "--best score: " + bestScore + "\n";
		return str;
	}
	
    public void printb(){
     for(int i=0; i<getWeights().length; i++){
         System.out.print(getb().get(i,0) + " ");
     }System.out.println("");
    }
    
	/* Update procedure for LSTD structures
     */
	public abstract void accumulateAb();
	
	/* Update procedure for binary (sparse) features */
	//public abstract void accumulateAbBinary();

}
