package agent;

import tetrisengine.*;
import features.*;

import java.lang.*;
import java.io.Serializable;

/* this is a (non-sparse) feature version of Transition
 @see Transition
 */
public class TransitionPhi extends TransitionGeneric{
	double[] phi;
	double[] phinext;
    
    FeatureExtractorGeneric fex;
	
	public TransitionPhi(){};
	
	public TransitionPhi(FeatureExtractorGeneric fexGiven, int piece, double[] ind, int action, double[] indnext, double reward, boolean absorb){
        super(piece, action, reward, absorb);
        fex=fexGiven;
		System.arraycopy(ind, 0, phi,0,ind.length);
		System.arraycopy(indnext, 0, phinext,0,indnext.length);
	}
	
	public TransitionPhi(FeatureExtractor fexGiven, Transition t){
        super(t.getPiece(), t.getAction(), t.getReward(), t.getAbsorb());
        fex=fexGiven;
		phi = ((FeatureExtractor)fex).getFeatures(t.getBoard());
		phinext = ((FeatureExtractor)fex).getFeatures(t.getBoardNext());
	}
	

	public double getV(double [] w){
		return ((FeatureExtractor)fex).getValue(phi, w);
	}

	public double getVnext(double [] w){
		return ((FeatureExtractor)fex).getValue(phinext, w);
	}
}