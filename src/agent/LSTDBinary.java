package agent;

import tetrisengine.*;
import features.*;

import Jama.Matrix;
import java.io.*;
import java.util.*;


/* bug: The LSTD1 A b should match that of LS. but this is not. 
 * However, the weight vectors of the two methhods are the same. 
 
 * An efficient least-squares temporal difference learning agent for policy evaluation for sparse-feature samples
 * LSTDBinary is the same as LSTD with the corresponding non-sparse features
 * @see SamplesBinary
 * @see LeastSquares
 * @see BinaryFeatureExtractor
 */
public class LSTDBinary extends PolicyEvaluationGeneric{
	
	boolean debug=true;
	
	/*data structures for the algorithm */
	double lambda=0;
	
	public LSTDBinary(){};
	
	/* @param fa the agent that collects samples
	 * @param numTrainGames number of games to train this agent
	 */
	public LSTDBinary(FeatureExtractorGeneric f, double lam, boolean saveOnPolicySamples, boolean saveAllActionSamples){
	    super(f, saveOnPolicySamples, saveAllActionSamples);
		setName("LSTDBinary policy evaluation Agent");
		setLambda(lam);
	}
	public LSTDBinary(FeatureExtractorGeneric f, boolean saveOnPolicySamples, boolean saveAllActionSamples){
	    super(f, saveOnPolicySamples, saveAllActionSamples);
		setName("LSTDBinary policy evaluation Agent");
	}
	
	public LSTDBinary(FeatureExtractorGeneric f, boolean saveOnPolicySamples, boolean saveAllActionSamples, List<Transition> tr){
		super(f, saveOnPolicySamples, saveAllActionSamples, tr);
	}
	
	public void setLambda(double lam){
		lambda = lam;
	}
	
	@Override
	public void solveWeightsAndEvaluate(boolean printBoard){
		super.solveWeightsAndEvaluate(printBoard);
		System.out.println(this);
	}
		
	@Override
	public void accumulateAb(){
		System.out.println("accumulateAb:"+ getName()+ " --injected with " + transits.size() + " samples");
		int epi=0;
		Matrix z=new Matrix(getNumFeatures(), 1);
		int[] phiIndex, phinextIndex;
		
		double reward;
		Transition transition_s;//current transition sample
		int begin=-1;
		int end=-1;//begin and end states of the current episode
		
		for(int s=0; s<transits.size(); s++){
			if(s==0){//the first episode
				begin = 0;
			}
			transition_s = transits.get(s);
			reward = transition_s.getReward();
			phiIndex = ((BinaryFeatureExtractor)getFeatureExtractor()).getFeatureIndices(transition_s.getBoard());
			
			if(s==begin){
				for(int ind:phiIndex){
					z.set(ind,0, 1);//z[ind]=1;
				}
			}
			
			if(reward<0) {//indicating the end of the current episode
				phinextIndex=null;
				end = s;
				begin = s + 1;
				epi++;
			}else{
				phinextIndex = ((BinaryFeatureExtractor)getFeatureExtractor()).getFeatureIndices(transits.get(s+1).getBoard());
			}
			
			//b = b + z*r
			//A = A + z*(phi-phinext)' = A + z*phi'- z*phinext'
			
			if(lambda>0){
				b.plusEquals(z.times(reward));
				for(int i=0; i<z.getRowDimension(); i++){
					//A = A + z*phi'
					for(int j=0; j<phiIndex.length; j++){
						A.set(i,phiIndex[j], A.get(i,phiIndex[j]) + z.get(i,0));
					}
					//A = A - z*phinext'					
					if(phinextIndex!=null){
						for(int j=0; j<phinextIndex.length; j++){
							//A.set(i,phinextIndex[j], gamma*( A.get(i,phinextIndex[j]) - z.get(i,0)) );//wrong 
							A.set(i,phinextIndex[j],  A.get(i,phinextIndex[j]) - gamma*z.get(i,0) );
						}
					}
				}
			}else {//z=phi
				for(int i=0; i<phiIndex.length; i++){
					b.set(phiIndex[i],0,  b.get(phiIndex[i],0) + reward);
					//A = A + phi*phi'
					for(int j=0; j<phiIndex.length; j++){
						A.set(phiIndex[i],phiIndex[j],  A.get(phiIndex[i],phiIndex[j]) + 1 );
					}
					//A = A - phi*phinext'					
					if(phinextIndex!=null){
						for(int j=0; j<phinextIndex.length; j++){
							A.set(phiIndex[i],phinextIndex[j],  A.get(phiIndex[i],phinextIndex[j]) - gamma );							
						}
					}
				}
			}
			
			//z = gamma* lam*z + phinext 
			if(lambda>0){
				z.timesEquals(gamma*lambda);
				if(phinextIndex!=null){
					for(int ind: phinextIndex){
						z.set(ind,0, z.get(ind,0)+1);//z[ind] +=  1;
					}
				}
			}else{
				if(phinextIndex!=null){
					for(int ind: phinextIndex){
						z.set(ind,0, 1);
					}
				}
			}
		}
	}
    
	
	@Override
	public String toString(){
		String str="####Summary for " + getName() + " with lambda="+ lambda + ":\n";
		str += super.toString();
		return str;
	}
	
}