package agent;

import java.util.*;
import java.io.*;
import tetrisengine.TetrisAction;

public class SamplesBinary{
	List<TransitionPhiBinary> allActionTrans = new ArrayList<TransitionPhiBinary>();
	List<TransitionPhiBinary> onPolicyTrans = new ArrayList<TransitionPhiBinary>();
	
	public SamplesBinary(){};
    
    /*
    public SamplesBinary(List<TransitionPhiBinary> onPolicyTrans){
        this.onPolicyTrans = onPolicyTrans;
    };*/
	
	public void clear(){
		if(allActionTrans.size()>0){
			System.out.println("---------Clearing "+allActionTrans.size()+" all-action samples-------");
			allActionTrans.clear();
		}
		if(onPolicyTrans.size()>0){
			System.out.println("00000000000000000Clearing "+onPolicyTrans.size()+" on-policy samples00000000000000000");
			onPolicyTrans.clear();
		}
	}
	
	public List<TransitionPhiBinary>  getAllActionTrans(){return allActionTrans;}
	public TransitionPhiBinary  getAllActionTrans(int i){return allActionTrans.get(i);}
	public List<TransitionPhiBinary>  getOnPolicyTrans(){return onPolicyTrans;}
	public TransitionPhiBinary getOnPolicyTrans(int i){return onPolicyTrans.get(i);}
	
	
	public void addOnPolicy(TransitionPhiBinary t){
		onPolicyTrans.add(t);
	}
	
	public void addAllAction(TransitionPhiBinary t){
		allActionTrans.add(t);
	}
		
	/*compute return for each state in its episode for this agent; only computing for the first numTrainGames games*/
	public static List<Double> compReturn(List<TransitionPhiBinary> transits){
		//List<TransitionPhiBinary> transits = getOnPolicyTrans();
		List<Double> ret=new ArrayList<Double>();
		assert(transits.size()>0);
		for(int s=0; s<transits.size(); s++){
			double ret_s=0;
			for (int j=s; j<transits.size(); j++) {
				ret_s += transits.get(j).getReward();
				if(transits.get(j).getReward()<0){//ends this episode
					break;
				}
			}
			ret.add(ret_s);
		}
		return ret;
	}

	
	public int getNumAllActionSamples(){return allActionTrans.size();}
	public int getNumOnPolicySamples(){return onPolicyTrans.size();}
	
	public static void saveTransitions(String fileToSaveSamples, List<TransitionPhiBinary> transits) throws IOException{
		String filePathPhi=fileToSaveSamples + "phi2.txt";
		String filePathPiece=fileToSaveSamples + "piece2.txt";
		String filePathPolicy=fileToSaveSamples + "policy2.txt";
		String filePathPhinext=fileToSaveSamples + "phinext2.txt";
		
		String filePathAction=fileToSaveSamples + "action2.txt";
		String filePathReward=fileToSaveSamples + "reward2.txt";
		
		BufferedWriter fPhi = new BufferedWriter(new FileWriter(filePathPhi, true));
		BufferedWriter fPiece = new BufferedWriter(new FileWriter(filePathPiece, true));		
		BufferedWriter fPolicy = new BufferedWriter(new FileWriter(filePathPolicy, true));
		BufferedWriter fPhinext = new BufferedWriter(new FileWriter(filePathPhinext, true));
		BufferedWriter fAction = new BufferedWriter(new FileWriter(filePathAction, true));
		BufferedWriter fReward = new BufferedWriter(new FileWriter(filePathReward,true));

		for(TransitionPhiBinary tp : transits){
			fPolicy.write(tp.getAction() + "\n");
			
			for(int featureIndex: tp.getPhiIndex()){
				fPhi.write(featureIndex + " ");
			}fPhi.write("\n");
			
			fPiece.write(tp.getPiece() + "\n");
			fAction.write(tp.getAction() + " " + TetrisAction.getPositionFromIndex(tp.getAction()) + " " + TetrisAction.getRotationFromIndex(tp.getAction()) + "\n");
			
			for(int featureIndex:tp.getPhinextIndex()){
				fPhinext.write(featureIndex + " ");
			}fPhinext.write("\n");

			fReward.write(Double.toString(tp.getReward()) + "\n");			
		}
		fPolicy.close();
		fPhi.close();
		fPiece.close();
		fAction.close();
		fPhinext.close();
		fReward.close();
	}
	
};
