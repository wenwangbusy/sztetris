package agent;

import features.*;

import Jama.Matrix;
import tetrisengine.*;
import java.io.*;
import java.util.*;


/*
 * A least-squares procedure for policy evaluation
 * using ordinary sparse samples
 * @see SamplesBinary 
 * @see BinaryFeatureExtractor
 */
public class LeastSquaresBinary extends PolicyEvaluationGeneric{
	public LeastSquaresBinary(FeatureExtractorGeneric f, boolean saveOnPolicySamples, boolean saveAllActionSamples){
		super(f, saveOnPolicySamples, saveAllActionSamples);
		setName("Binary (sparse) LS policy evaluation Agent");
	}

    @Override
	public void accumulateAb(){;}
    /*
	@Override
	public void accumulateAb(){
		//System.out.println("accumulateAb:"+ getName()+ " --injected with " + transitsBin.size() + " samples");
		assert(transitsBin.size()>0);//got to update. no transitsBin any more
		
		Integer[] phiIndex;
		int epi=0;
		for(int s=0; s<transitsBin.size(); s++){
			TransitionPhiBinary tmp = transitsBin.get(s);
			double reward = tmp.getReward();
			if(reward<0) {
				epi++;
			}
			phiIndex = tmp.getPhiIndex();
			
			for(int i=0; i<phiIndex.length; i++){
				b.set(phiIndex[i],0,  b.get(phiIndex[i],0) + ret.get(s) );//increment by return
			}
	
			//A = A + phi*phi'
			for(int i=0; i< phiIndex.length; i++){
				for(int j=0; j< phiIndex.length; j++){
					A.set(phiIndex[i],phiIndex[j],  A.get(phiIndex[i],phiIndex[j]) + 1 );//increment by 1
				}		
			}
			
		}
	}
    */
	
    /*
	@Override
	public void solveWeightsAndEvaluate(boolean printBoard){
		super.solveWeightsAndEvaluate(printBoard);
		System.out.println(this);
	}
	*/
	
	@Override 
	public String toString(){
		String str="####Summary for " + getName() + ":\n";
		str += super.toString();
		return str;
	}
	
}