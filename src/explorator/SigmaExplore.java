package explorator;

import java.util.*;

import api.*;
import agent.*;


//a simple exploration strategy using lstd's weight as mean from Guassian Distribution
public class SigmaExplore{
    final LSTDBinary lstd;
    final int nExplorers;
    final int nEvaluateGames;
    
    final double[] bestPolicy;
    
    public SigmaExplore(LSTDBinary lstd, int nExplorers, int nEvaluateGames){
        this.lstd=lstd;
        this.nExplorers=nExplorers;
        this.nEvaluateGames=nEvaluateGames;
        bestPolicy=new double[lstd.getWeights().length];
        
        lstd.setSaveOnPolicySamples(false);
        lstd.setSaveAllActionSamples(false);
    }
    
    public double[] getBestPolicy(){
        return bestPolicy;
    }
    
    //get the best exploration weight vector from the current lstd policy
    public void computeBestPolicy(double sigma){
        boolean debug=true;
        System.out.println("sigma="+sigma);
        int n=lstd.getWeights().length;
        double scoreMax=-10e10;
        double score=scoreMax;
        int indMax=-1;
        double[] wlstdOriginal = new double[n];
        System.arraycopy(lstd.getWeights(),0, wlstdOriginal,0, n);
        for(int i=0; i<nExplorers; i++){
            if(i>0){
                //setLSTD2ExploreWeights(wlstdOriginal, sigma/(i+1)));//125 rewards can be observed!
                setLSTD2ExploreWeights(wlstdOriginal, sigma);
            }
            score=lstd.evaluate(nEvaluateGames, false, false);
            if(debug){
                System.out.println("exploration "+ i+ ": the policy achieves score="+score);
            }
            if(score>scoreMax){
                scoreMax = score;
                indMax = i;
                System.arraycopy(lstd.getWeights(),0, bestPolicy,0, n);
            }
        }
        if(debug){
            System.out.println("finally, the best score is "+ scoreMax +"; achieved at exploration "+ indMax);
            //System.out.println((FeatureUserAgent)lstd);--still print lstd.toString(). I want to print FeatureUserAgent.toString()
        }
    }
    
    public void setLSTD2ExploreWeights(double[] w, double sigma){
        Random rnd = new Random();
        for(int i=0; i<w.length; i++){
            lstd.setWeight(i, w[i] + 2*(rnd.nextDouble()-0.5) * sigma);
        }
    }

}