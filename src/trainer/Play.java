package trainer;

import agent.*;
import features.*;
import tetrisengine.*;

import java.io.*;
import java.util.Arrays;
import java.util.Random;

import Jama.Matrix;


/** A Play is an agent in a game environment
 */
public abstract class Play{
    TetrisGame game;
    FeatureUserAgent agent;//acting agent which collects samples
    AfterState startState = null;

    public Play(TetrisGame game, FeatureUserAgent agent){
        this.game = game;
        this.agent = agent;
    }
    
    final static int Nruns= TetrisGame.getRUNS();
    final static double[] deltaA = PolicyEvaluationGeneric.getDeltaA();
    double[] scoreAgent = new double[TetrisGame.getRUNS()];//average scores of the agent across runs
    
    /*
	double[][][] scores;//scores[r][e][d] is the (average) score of the policy trained for some API algorithm run r and delta d, given e episodes
    
    */
	
	/* Write the results to files */
	public abstract void writeResults();
	
	/**Play a number of games and record the performances */
    public abstract void evaluate();
    
    @Override
	public String toString(){
		String str="\n\n\n";
		double avg=0;
		for(int i=0; i<scoreAgent.length; i++){
			avg += scoreAgent[i];
		}
		avg /= Nruns;
		str += "--Average score for the agent is " + avg + "\n";
        return str;
    }
}
