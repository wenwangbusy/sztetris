package trainer;

import agent.*;
import features.*;
import tetrisengine.*;
import api.*;

import java.util.*;
import java.io.*;

//exexuting modes
//API_ONLINE: apply api online; API_OFFLINE: apply api offline; EVALUATION: evaluating an agent and not collecting samples; COLLECTION: evaluating an agent and collect samples; PE: evaluating an agent, collecting samples whilst policy evaluation; TRAINCE: train CE algorithm

public class RunInMode{
        Mode mode;
        String algorithm;
        FeatureUserAgent fAgent;
        TetrisGame tg= new TetrisGame(10, 20, 0);
        Main G;
    
        public RunInMode(Mode mode, String algorithm){
            this.mode=mode;
            this.algorithm=algorithm;
            tg.setRUNS(1);
            //tg.setRUNS(10);
			tg.setNumGames(1);
            //tg.setNumGames(100);
            //tg.setNumGames(200);
        }
    
        public static void execute(String input, String algorithm){
            RunInMode runInMode=null;
            if(input.equals("train")){//tested
                runInMode=new RunInMode(Mode.TRAINCE, algorithm);
            }
            else if(input.equals("apionline")){
                runInMode=new RunInMode(Mode.API_ONLINE, algorithm);
            }else if(input.equals("apioffline")){
                runInMode=new RunInMode(Mode.API_OFFLINE, algorithm);
            }else if(input.equals("evaluation")){//tested
                runInMode=new RunInMode(Mode.EVALUATION, algorithm);
            }else if(input.equals("collection")){
                runInMode=new RunInMode(Mode.COLLECTION, algorithm);
            }
            else if(input.equals("pe")){//tested
                runInMode=new RunInMode(Mode.PE, algorithm);
            }
            runInMode.run();
        }

        
        public void run(){
            switch(mode){
                case TRAINCE:
                    System.out.println("Training CE mode...");
                    FeatureExtractorGeneric fex=new BI();
                    fAgent = new FeatureUserAgent(algorithm, fex, "", false, false);
                    G = new Main(tg, algorithm, false, false, false);
                    G.onlinece(tg, fAgent);
                    break;
                case API_ONLINE:
                    System.out.println("Online Approximate policy iteration mode...collecting by self.");
                    G = new Main(tg, "CE", false, false, false);//CE is just for creating the agent, which is never used
                    //APIRunner apiRun = new APIRunner(tg, G.getAgent(), new LSPI(new BinaryBITabular()));
                    //apiRun.runAPI(true);
                    break;
                case API_OFFLINE:
                    System.out.println("Offline Approximate policy iteration mode...using " +algorithm+" to collect.");
                    G = new Main(tg, algorithm, false, false, false);
                    APIRunner apiRun = new APIRunner(tg, G.getAgent(), new OfflineLSPI(10, new LSTDBinary(new BinaryBITabular(), false, false)));
                    //APIRunner apiRun = new APIRunner(tg, G.getAgent(), new OfflineLAMAPI(10, new LSTDBinary(new BinaryBITabular(), false, false)));
                    apiRun.runAPIOffline();
                    break;
                case EVALUATION://the CE policy performs 120-133 during 10 runs of 100 games. pretty large variation in a run (from 40 to 3xx)
                    System.out.println("Evaluating an agent and not collecting samples");
                    G = new Main(tg, algorithm, false, false, false);
                    G.evaluateAgent();
                    break;
                case COLLECTION:
                    System.out.println("Evaluatin an agent and collecting samples");
                    G = new Main(tg, algorithm, true, false, false);
                    G.evaluateAgent();
                    break;
                case PE://use non-baninary features; should improve using binary features
                    FeatureExtractorGeneric fexPE;
                    //fexPE = new BINoNholesTabular();//not working
                    //fexPE = new NholesTabular();//not working; hole features are tested.
                    //fexPE= new BIHolesTabular();//works well! 150 rewards.
                    //fexPE= new HolesTabular();//not working.
                    //fexPE = new HeightDifferenceTabular();
                    //fexPE = new HeightHolesTabular();
                    fexPE = new HeightDifferenceHolesTabular();//also works. e.g., 120 rewards
                    G = new Main(tg, algorithm, true, false, true, fexPE);
                    G.evaluateAgent();
                    break;
            }
        }
    
    
    }