package trainer;

import agent.*;
import features.*;
import tetrisengine.*;

import java.io.*;
import java.util.Arrays;
import java.util.Random;

import Jama.Matrix;


/** Evaluate the performance of an agent (who has a policy), and at the same time collecting on-policy samples for the policy in this agent.
 * Note: it can be extended to a policy evaluation agent (with applyPolicyEvaluation=true)
 * @see TetrisGame
 * @see FeatureUserAgent
 * @see PolicyEvaluation
 */
public abstract class EvaluatorGeneric extends Play{

	/* policy ealuation data structures */
	boolean applyPolicyEvaluation;
	final double[] lambda={0.0};//{1.0};
	//final double[] lambda={0, 0.2, 0.4, 0.6, 0.8, 1.0};
    
	double[][][][] scores;//scores[r][l][e][d] is the (average) score of the LSTD policy trained for run r and lambda l and delta d, given e episodes
		
	//see if LSTD1-binary produces the same weights with LS-nonbinary
	boolean debug=false;//true;
	
	
    double[] deltaA=PolicyEvaluationGeneric.getDeltaA();
    
	/** this evaluates an agent (with a feature extractor and weights learned and saved previously) by playing a number of games, 
	 and getting the average score; averaged over a number of runs
	 @param game Tetris Game
	 @param agent a feature agent, which acts and collects samples according to its weight vector
	 @param applyPolicyEvaluation whether evaluate the running agent or not
	 */
    public EvaluatorGeneric(TetrisGame game, FeatureUserAgent agent, boolean applyPolicyEvaluation)
    {
        super(game, agent);
		this.applyPolicyEvaluation = applyPolicyEvaluation;
		
		if(applyPolicyEvaluation){
			scores = new double[TetrisGame.getRUNS()][lambda.length][game.getNumGames()][deltaA.length];
        }
    }

	@Override
	public String toString(){
		String str=super.toString();
		
		str += "--Average scores for LSTD evaluating the agent: \n";
		for(int run=0; run<TetrisGame.getRUNS(); run++){
			str += "run "+ run + "\n";
			for(int lam=0; lam<lambda.length; lam++){
				str += "---lam "+ lambda[lam] + "\n";
				for(int epi=0; epi<game.getNumGames(); epi++){
					str += "----episode "+ epi + "\n-----";
					for(int del=0; del<deltaA.length; del++){
						str += scores[run][lam][epi][del] + ",";
					}
					str += "\n";
				}
				str += "\n";
			}
			str += "\n";
		}
		return str;
	}
	
	/* Write the results to files */
	public abstract void writeResults();
	
}
