package trainer;

import agent.*;
import features.*;
import tetrisengine.*;

import java.io.*;
import java.util.Arrays;
import java.util.Random;
import java.util.List;

import Jama.Matrix;


/** Evaluate the performance of an agent (who has a policy) using binary (sparse features)
 * @see EvaluatorGeneric
 * @see PolicyEvaluationBinary
 */
public class EvaluatorBinary extends EvaluatorGeneric{	
	LSTDBinary[] lstd;
	
	//least-squares and LSTD policy evaluation (just used for verification of lstd1)
	LeastSquaresBinary ls;
	
    public EvaluatorBinary(TetrisGame game, FeatureUserAgent agent, boolean applyPolicyEvaluation, FeatureExtractorGeneric fex_bin)
    {
		super(game, agent, applyPolicyEvaluation);		
		if(applyPolicyEvaluation){
			lstd = new LSTDBinary[lambda.length];
			for(int lamInd=0; lamInd<lambda.length; lamInd++){
				lstd[lamInd] = new LSTDBinary(fex_bin, lambda[lamInd], false, false);
				if(debug && lambda[lamInd]==1.0){
					ls =new LeastSquaresBinary((BinaryFeatureExtractor)fex_bin, false, false);
				}
			}	
		}
    }

	public void policyEvaluate(List<Transition> transits, int run, int episode){
		for(int lamInd=0; lamInd<lambda.length; lamInd++){
			//lstd[lamInd].receiveSamples(samples);
            lstd[lamInd].setTransits(transits);
			lstd[lamInd].accumulateAb();
			lstd[lamInd].solveWeightsAndEvaluate(false);
			lstd[lamInd].printApproximation(transits);
			if(debug && lambda[lamInd]==1.0){
				if(debug){
					//ls.receiveSamples(samples);
                    ls.setTransits(transits);
					ls.accumulateAb();
					ls.solveWeightsAndEvaluate(false);
					ls.printApproximation(transits);
				}
				Matrix wlstd1 = new Matrix(lstd[lamInd].getWeights(), 1);
				Matrix wls = new Matrix(ls.getWeights(), 1);
				double err=wlstd1.minusEquals(wls).norm1();
				String str= "wls and wlstd1 Error is " + err;
				//System.out.println(str);
				if(err!=0){
					throw new RuntimeException(str);
				}
			}
			
			if(lambda[lamInd]==0.0){
				double[] wlstd0 = lstd[lamInd].getWeights();
				System.out.println("The weights of lstd0 are, ");
				for(int i=0; i<wlstd0.length; i++){
					System.out.print(wlstd0[i] + " ");
				}System.out.println(" ");
			}
			
			for(int del=0; del<deltaA.length; del++){
				scores[run][lamInd][episode][del] = lstd[lamInd].getScores()[del];
			}
		}
		
	}
	
    public void evaluate()
    {
		for(int run=0; run<TetrisGame.getRUNS(); run++){
			System.out.println("\n\n\n------------------Run " + run + "------------------");
			final long startTime = System.currentTimeMillis();
			
			double score;
			for (int epi=0; epi<game.getNumGames(); epi++)
			{
				System.out.println();
				System.out.println();
				System.out.printf("Starting game %d\n", epi);
				if (startState==null)
					score = TetrisGame.playOneGame(agent, false);
				else
					score = TetrisGame.playOneGame(agent, startState, false);
				
				if(applyPolicyEvaluation){//evaluate episode by episode and not save samples for fast performance
					policyEvaluate(agent.getSamples().getOnPolicyTrans(),  run, epi);
					agent.clearSamples();
				}else{
					if(agent.getSaveOnPolicySamples() || agent.getSaveAllActionSamples())
						agent.saveSamples();
				}
				
				System.out.printf("game: %4d out of %4d, \t score is %d\n", epi+1,game.getNumGames(), (int) score);
				scoreAgent[run] += score;
			}
	
//			if((!applyPolicyEvaluation) && (agent.getNumAllActionSamples()>0 || agent.getNumOnPolicySamples()>0) )
//				agent.saveSamples();//save remaining samples if any left	
			
			//for small amount of samples; write a time
			//agent.saveSamples();
			
			scoreAgent[run] /= game.getNumGames();
			System.out.println("Time spent: " + (System.currentTimeMillis()-startTime) );
			System.out.printf("Average agent score for run " + run +": %f\n", scoreAgent[run]);
		}
		
		if(applyPolicyEvaluation){
			System.out.println(this);
			writeResults();
		}
    }
    
    @Override
    public void writeResults(){;}
}
