package trainer;

import agent.*;
import features.*;
import tetrisengine.*;

import java.io.*;
import java.util.Arrays;
import java.util.Random;
import java.util.List;

import Jama.Matrix;


/** Evaluate the performance of an agent using non-binary(sparse) features 
 * @see LSTD
 */
public class Evaluator extends EvaluatorGeneric{
	LSTD[] lstd;
	
	//least-squares and LSTD policy evaluation (just used for verification of lstd1)
	LeastSquares ls;
	
	/*
	 @param fexPE non-sparse features do policy evaluation for the agent
	 */
    public Evaluator(TetrisGame game, FeatureUserAgent agent, boolean applyPolicyEvaluation, FeatureExtractorGeneric fexPE)
    {
        super(game, agent, applyPolicyEvaluation);
		
		if(applyPolicyEvaluation){
			if(debug){
				ls =new LeastSquares(fexPE);
			}
			lstd = new LSTD[lambda.length];
			for(int lamInd=0; lamInd<lambda.length; lamInd++){
                assert(fexPE!=null);
				lstd[lamInd] = new LSTD(fexPE, lambda[lamInd]);
			}	
		}
    }

	
	/* Evaluating the policy of the agent
	 @param samples Samples in the form of Transition
	 @param run run index
	 @param episode episode index
	 @see Transition
	 */
	public void policyEvaluate(List<Transition> transits, int run, int episode){
		for(int lamInd=0; lamInd<lambda.length; lamInd++){
			//lstd[lamInd].receiveSamples(samples);
            lstd[lamInd].setTransits(transits);
			lstd[lamInd].accumulateAb();
			lstd[lamInd].solveWeightsAndEvaluate(false);//print boards of game playing for the policy evaluation agent
			if(debug && lambda[lamInd]==1.0){
				//ls.receiveSamples(samples);
                ls.setTransits(transits);
				ls.accumulateAb();
				ls.solveWeightsAndEvaluate(false);
				Matrix wlstd1 = new Matrix(lstd[lamInd].getWeights(), 1);
				Matrix wls = new Matrix(ls.getWeights(), 1);
				double err=wlstd1.minusEquals(wls).norm1();
				String str= "wls and wlstd1 Error is " + err;
				//System.out.println(str);
				if(err!=0){
					throw new RuntimeException(str);
				}
			}
			
			for(int del=0; del<deltaA.length; del++){
				scores[run][lamInd][episode][del] = lstd[lamInd].getScores()[del];
			}
		}
		
	}

	@Override
    public void evaluate()
    {
		for(int run=0; run<TetrisGame.getRUNS(); run++){
			System.out.println("\n\n\n------------------Run " + run + "------------------");
			
			double score;
			for (int epi=0; epi<game.getNumGames(); epi++)
			{
				System.out.println();
				System.out.println();
				System.out.printf("Starting game %d\n", epi);
				if (startState==null)
					//score = TetrisGame.playOneGame(agent, false);//this boolean switch print board for the collection agent
					score = TetrisGame.playOneGame(agent, true);
				else
					score = TetrisGame.playOneGame(agent, startState, false);
				
				if(applyPolicyEvaluation){//evaluate episode by episode
					policyEvaluate(agent.getSamples().getOnPolicyTrans(),  run, epi);
					agent.clearSamples();
				}else{	
					if(agent.getSaveOnPolicySamples()||agent.getSaveAllActionSamples())
						agent.saveSamples();
				}
				
				System.out.printf("game: %4d out of %4d, \t score is %d\n", epi+1,game.getNumGames(), (int) score);
				scoreAgent[run] += score;
			}
	
//			if((!applyPolicyEvaluation) && (agent.getNumAllActionSamples()>0 || agent.getNumOnPolicySamples()>0) )
//				agent.saveSamples();//save remaining samples if any left	
			
			//for small amount of samples; write a time
			//agent.saveSamples();
			
			scoreAgent[run] /= game.getNumGames();
			System.out.printf("Average agent score for run " + run +": %f\n", scoreAgent[run]);
		}
		
		if(applyPolicyEvaluation){
			System.out.println(this);
			writeResults();
		}
    }
    
    @Override
    /* Write the results to files */
	public void writeResults(){
		try{
			//write the agent peformance
			String file = "scores" + game.getNumGames()+ "epis_agent.txt";
			BufferedWriter fScoresAgent = new BufferedWriter(new FileWriter(file, true));
			for(double sc:scoreAgent)
				fScoresAgent.write(sc + " ");
			fScoresAgent.close();
			
			if(applyPolicyEvaluation){
				//write policy evaluation results: can you use toString()? should be yes.
				for(int lam=0; lam<lambda.length; lam++){
					String fileScores0 = "scores" + game.getNumGames() + "_lstdlam"+ lambda[lam] + "_" + lstd[lam].getFeatureExtractor().getName()+"_";
					for(int del=0; del<deltaA.length; del++){
						String fileScores = fileScores0 + "deltaindex"+del + ".txt";
						BufferedWriter fScores = new BufferedWriter(new FileWriter(fileScores, true));
						for(int run=0; run<TetrisGame.getRUNS(); run++){
							for(int epi=0; epi<game.getNumGames(); epi++){
								fScores.write(scores[run][lam][epi][del] + " ");
							}
							fScores.write("\n");
						}
						fScores.close();
					}
				}
			}
			
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
    
}
