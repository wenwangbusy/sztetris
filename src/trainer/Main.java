package trainer;

import agent.*;
import features.*;
import tetrisengine.*;
//import api.*;

import java.util.*;
import java.io.*;


/* Run an agent with some features, whose weights are saved previously in some file. 
 * Collect samples
 * One has the option of online policy evaluation, which has two modes
	* non-sparse features
	* sparse(binary) features
 
 One can do the following: 
 Evaluate an agent, Save samples
 0. Run Cross Entropy algorithm
	java -jar -ea dist/sztetris.jar train ce 
 1. Evaluate the performance of an agent without saving any sample
    java -jar -ea dist/sztetris.jar  CE NOTsaveOnPolicySamples  NOTsaveAllActionSamples  CE_BI_run01_formatlab.txt 1 notPE notBinarySamples
 2. Evaluate the performance of an agent and save non-sparse-feature format samples
	java -jar -ea dist/sztetris.jar  CE saveOnPolicySamples  NOTsaveAllActionSamples  CE_BI_run01_formatlab.txt 1 notPE notBinarySamples
 3. similar to 2, but save all-action samples for each state board along the way
	java -jar -ea dist/sztetris.jar  CE NOTsaveOnPolicySamples  saveAllActionSamples  CE_BI_run01_formatlab.txt 1 notPE notBinarySamples
 4. similar to 3, but also save on-policy samples
	java -jar -ea dist/sztetris.jar  CE saveOnPolicySamples  saveAllActionSamples  CE_BI_run01_formatlab.txt 1 notPE notBinarySamples
 	java -jar -ea dist/sztetris.jar  CE saveOnPolicySamples  saveAllActionSamples  CE_BI_run01_formatlab.txt 1 notPE BinarySamples
 5. Evaluate the performance of an agent and save sparse-feature format samples
	java -jar -ea dist/sztetris.jar  CE saveOnPolicySamples  NOTsaveAllActionSamples  CE_BI_run01_formatlab.txt 1 notPE  BinarySamples
	java -jar -ea dist/sztetris.jar  CE saveOnPolicySamples  saveAllActionSamples  CE_BI_run01_formatlab.txt 100 notPE  BinarySamples 
 Policy evaluation:
 set deltaA in PolicyEvaluationGeneric
 set lambda in EvaluatorGeneric (also set the number of runs)
 use saveOnPolicySamples to save samples following the agent
 6. Evaluate the agent, save on-policy samples, and do policy evaluation using non-binary features
	java -jar -ea dist/sztetris.jar  CE saveOnPolicySamples  notsaveAllActionSamples  CE_BI_run01_formatlab.txt 1 applyPolicyEvaluation notBinary
	java -jar -ea dist/sztetris.jar  CE saveOnPolicySamples  notsaveAllActionSamples  CE_BI_run01_formatlab.txt 10 applyPolicyEvaluation BinarySamples 
 
 API
 7.  
 use LAMAPI-Accurate: 
 collect samples
	CE samples:
		java -jar -ea dist/sztetris.jar  CE saveOnPolicySamples  saveAllActionSamples  CE_BI_run01_formatlab.txt 10 notPE BinarySamples
		java -jar -ea dist/sztetris.jar api CE_BI_run01_formatlab.txt10epis_samples_allActions_ 
	Random samples (appending to CE sample file)
		java -jar -ea dist/sztetris.jar  Random saveOnPolicySamples  saveAllActionSamples noWeightFile 100 notPE BinarySamples
    
	java -jar -ea dist/sztetris.jar api NOaddOnPolicySamples CE_BI_run01_formatlab.txt10epis_samples_allActions_ Random100epis_samples_allActions_
	java -jar -ea dist/sztetris.jar api 
 
 */
public class Main {

	private TetrisGame game;
	private FeatureUserAgent fAgent;
	EvaluatorGeneric evaluator;
	
	
    public Main(TetrisGame game, String algorithm, boolean saveOnPolicySamples, boolean saveAllActionSamples, boolean applyPolicyEvaluation){
        this(game, algorithm, saveOnPolicySamples, saveAllActionSamples, applyPolicyEvaluation, new BIHolesTabular());
    }
    
	public Main(TetrisGame game, String algorithm, boolean saveOnPolicySamples, boolean saveAllActionSamples, boolean applyPolicyEvaluation, FeatureExtractorGeneric fexPE){
		this.game = game;
        
        createAgent(algorithm, saveOnPolicySamples, saveAllActionSamples);
        evaluator= new Evaluator(game, fAgent, applyPolicyEvaluation, fexPE);
		
        /*
		if(algorithm.equals("Random")){
			fAgent.setFileToSaveSamples("Random" + game.getNumGames()+ "epis_samples_");
		}else{
			fAgent.setFileToSaveSamples(weightFile+ game.getNumGames()+ "epis_samples_");
		}*/
	}
	
    public void createAgent(String algorithm, boolean saveOnPolicySamples, boolean saveAllActionSamples){
        FeatureExtractorGeneric fex;
        String weightFile="";
        if(algorithm.equals("CE")){
            fex = new BI();
            //FeatureExtractor fex = new KernelFeatureExtractor(tg, 983);
            weightFile="CE_BI_run01_formatlab.txt";
        }
        else if(algorithm.equals("LSTD_evalCE")){
            fex = new BIHolesTabular();
            weightFile="CE_BI_run01_formatlab.txt";
        }else if(algorithm.equals("Random")){
            fex = new BI();
        }
        else{
            throw new RuntimeException("only CE, LSTD_evalCE, Random algorithms are supported.");
        }
		//collect samples and apply policy evaluation in consistent sample format
		fAgent = new FeatureUserAgent(algorithm, fex, weightFile, saveOnPolicySamples, saveAllActionSamples);
    }
    
    public FeatureUserAgent getAgent(){return fAgent;}
    
	public void evaluateAgent(){
        evaluator.evaluate();//evaluate the agent, collect and save samples	
	}

	public static void onlinece(TetrisGame tg, FeatureUserAgent fAgent)
    {
        //Trainer trainer;
        //TetrisGame game = new TetrisGame(10, 20);
        //FeatureExtractor fex = new BertsekasIoffeFeatureExtractor(game);
		
        Options options = new Options();
        options.put("fex", fAgent.getFeatureExtractor());
        options.put("alpha", 1e-1);
        options.put("beta",  10e-2);
        options.put("epsilon", 0.1);
        options.put("gamma", 0.99);
		
		int run=4;
        Trainer trainer = new CEtrainer(tg, fAgent, run);
        int numgenerations = 100;
        for (int it=0; it<numgenerations; it++)
            trainer.playGeneration();
    }
    
	public static void main(String[] args) {
        RunInMode.execute(args[0], args[1]);
        
        /*
		if(args[0].equals("train")){
			System.out.println("Training mode...");
		}else if(args[0].equals("api")){
			System.out.println("Approximate policy iteration mode...");
			
			LAMAPIAccurateBinaryBertTabular lamapiAccurateBin = new LAMAPIAccurateBinaryBertTabular(new BinaryBertsekasIoffeFeatureExtractorTabular(), true, false, true);

			String weightFile="weightsFromOnlineAdapt.txt";
			assert(args.length>1);
                        
			if(args[1].equals("online")){
					System.out.println("----Online Adapting mode---...Right now this is the best");
			}else{
				System.out.println("----Offline API mode-----");
                
                boolean useOnlineLAMAPIPolicyToCollect=true;
				
                int Nruns=1;
                double[] deltaA = PolicyEvaluationGeneric.getDeltaA();
                double[][][] scores_run_delta_iter = new double[Nruns][deltaA.length][SampleCollectorBinary.getNumIterations()];
                int[][][] totalSamples_run_delta_iter = new int[Nruns][deltaA.length][SampleCollectorBinary.getNumIterations()];
								
				int numEpisodes = 100;//500; //50;//100: out of memory for epsilon=0
				
				//study the effect of epsilon for sample collection
				//double[] epsilons = new double[]{0, 0.1, 0.3, 0.5, 0.7, 0.9, 1.0};
				double[] epsilons = new double[]{0.0};//first get one case done well
                double[] w0_lamapiOnline=new double[lamapiAccurateBin.getWeights().length];
                List<Transition> samples=null;
                List<TransitionPhiBinary> samplesBin=null;
				for(int ind_eps=0; ind_eps<epsilons.length; ind_eps++){
					double epsilon = epsilons[ind_eps];
					System.out.println("using epsion="+epsilon + " to collect samples");
					for(int run=0;run<Nruns;run++){
						System.out.println("Run "+(run+1) );
						if(useOnlineLAMAPIPolicyToCollect){
                            System.out.println("Collect samples using lamapiAccurateBin with an initial policy learned from online lamapi");
                            if(run==0){
                                try{
                                    lamapiAccurateBin.loadWeights(weightFile);
                                    //w0_lamapiOnline=lamapiAccurateBin.getWeights();
                                    System.arraycopy(lamapiAccurateBin.getWeights(),0, w0_lamapiOnline,0, lamapiAccurateBin.getWeights().length);
                                    System.out.println("w0_lamapiOnline.length="+w0_lamapiOnline.length+", w0_lamapiOnline[0]="+w0_lamapiOnline[0]);
                                }
                                catch (IOException e) {
                                    System.out.println(e.getMessage());
                                }
                            }else{
                                lamapiAccurateBin.setWeights(w0_lamapiOnline);
                            }
                            lamapiAccurateBin.clearSamples();
                            lamapiAccurateBin.setSaveOnPolicySamples(true);
                            lamapiAccurateBin.setSaveAllActionSamples(false);
                            lamapiAccurateBin.setUseBinarySamples(true);
                            lamapiAccurateBin.setEpsilon(epsilon);
                            System.out.println("in main(), Collecting " + numEpisodes+ " episodes for the policy learned online");
                            lamapiAccurateBin.evaluate(numEpisodes, false);
                            samples = lamapiAccurateBin.gettransitsInOnlineSamples();
                            System.out.println("in main(), samples.size="+samples.size());
                            samplesBin = lamapiAccurateBin.gettransitsBinInOnlineSamples();
                            System.out.println("in main(), samplesBin.size="+samplesBin.size());
                        }else{
                            System.out.println("Collecting using CE policy. ");
//                            samples = ;
//                            samplesBin = ;
                        }
                        
						LSPI lspi = new LSPI(new BinaryBertsekasIoffeFeatureExtractorTabular(), true, samples, samplesBin);
						if(useOnlineLAMAPIPolicyToCollect)
                            lspi.setInitialPolicy(w0_lamapiOnline);
                        
						boolean useOnlineSamples=false;//true;
						lspi.api(useOnlineSamples);
						
						scores_run_delta_iter[run] = lspi.getAllDeltaScores();
						totalSamples_run_delta_iter[run] = lspi.getAllDeltaTotalSamples();
					}
                    
                    //write scores
					try{
						for(int d=0; d<deltaA.length; d++){
							String filename = "lspi_delta"+deltaA[d]+ "From"+numEpisodes+"EpisOfsampleswitheps"+lamapiAccurateBin.getEpsilon();
							BufferedWriter fScores = new BufferedWriter(new FileWriter(filename, true));
							for(int run=0; run<Nruns; run++){//getNruns?
								for(int iter=0;iter<SampleCollectorBinary.getNumIterations(); iter++){
									fScores.write(scores_run_delta_iter[run][d][iter] + " ");
									//fSamples.write(totalSamples_run_delta_iter[run][d][iter] + " ");
								}
								fScores.write("\n");
								//fSamples.write("\n");
							}
							fScores.close();
							//fSamples.close();
						}
					}catch(IOException e){
						e.printStackTrace();
					}
			}
                
            }
		}
		*/
        
		//lstdSolve();
        //evalLSTD();

//        Trainer trainer;
//        TetrisGame game = new TetrisGame(10, 20);
//        FeatureExtractor fex;
//
//        fex = new BertsekasIoffeFeatureExtractor(game);
//        Evaluator evaluator = new Evaluator(fex, 0, 100);
//        evaluator.isLogging = true;
//        evaluator.evaluate();
    }

}
