package trainer;

import agent.*;
import features.*;
import tetrisengine.*;



import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Locale;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
//import javax.swing.JScrollBar;
//import javax.swing.JScrollPane;
//import javax.swing.JTextArea;

import Jama.Matrix;

/**
 *
 * @author szityu
 */
public class CEtrainer implements Trainer {
    Random rnd;
    
    final int POPULATIONSIZE = 1000;
    final double ALPHA = 0.6;
    final double RHO = 0.01;
    
    double [][] wlist;
    double [] scorelist;
    double [] truescorelist;
    double [] sortedscores;
    double [] M, Mnew;
    double [] S, Snew;
    double smean;
    
    int generation;
    //int it;
    boolean fileexists;
    
    TetrisGame game;
    FeatureUserAgent agent;
	
    //JTextArea textarea;
    PrintStream fileForMatlab, fileForHuman;
	
	double[] w_track;//tracking the top weight vectors; actually Mmean is a tracking
    
	Matrix A, b;
//	final double deltaA=100;
//	final double deltaA=1;
	final double deltaA=10000;
	//	final double deltaA=100000;
	double[] w_ls; //try to learn a least squares fit of the return of CE policy
	
	
    public CEtrainer(TetrisGame tg, FeatureUserAgent fAgent, int run)
    {
        int i;
        rnd = new Random();
		
		//reference copy: shallow 
        game = tg;
        agent = fAgent;

        FeatureExtractorGeneric fex = fAgent.getFeatureExtractor();
        int nFeatures = fAgent.getNumFeatures();
        
        w_track = new double[nFeatures];
        w_ls  = new double[nFeatures];
		
		wlist = new double[POPULATIONSIZE][nFeatures];
        scorelist = new double[POPULATIONSIZE];
        truescorelist = new double[POPULATIONSIZE];
        sortedscores = new double[POPULATIONSIZE];
        M = new double[nFeatures];
        S = new double[nFeatures];
        Mnew = new double[nFeatures];
        Snew = new double[nFeatures];
        //game.LoadInitValues();
        for (i = 0; i < nFeatures; i++) {
            //M[i] = 0.0;
            S[i] = 100.0;
        }
        //M[2*game.width] = -50.0;
        

		A = Matrix.identity(nFeatures,nFeatures);//new Matrix(getNumFeatures(), getNumFeatures());
		A.timesEquals(deltaA);
		b = new Matrix(nFeatures,1);
		
        generation = 0;
        int it = 0;

        String fname = String.format("CE_%s_run%02d_formatlab.txt", fex.getName(), run);
        System.out.println(fname);
        fileexists = (new File(fname)).exists();
        if (!fileexists)//create the files if they not exist
        {
            try
            {
                fileForMatlab = new PrintStream(fname);
            }
            catch (FileNotFoundException ex)
            {
                Logger.getLogger(CEtrainer.class.getName()).log(Level.SEVERE, null, ex);
            }
            fname = String.format("CE_%s_run%02d_forhuman.txt", fex.getName(), run);
            try
            {
                fileForHuman = new PrintStream(fname);
            }
            catch (FileNotFoundException ex)
            {
                Logger.getLogger(CEtrainer.class.getName()).log(Level.SEVERE, null, ex);
            }
            fileForHuman.printf((Locale)null,
                    "Training data for %d*%d table, %d basis functions \n---------------------------\n\n",
                    game.getWidth(), game.getHeight(), agent.getNumFeatures());
            fileForMatlab.printf((Locale)null,
                    "%d %d %d\n",
                    game.getWidth(), game.getHeight(), agent.getNumFeatures());
        }else{
            throw new RuntimeException("You may want to delete file "+fname);
        }
		
		fileexists = true;
    }
    
    
    FeatureUserAgent getAgent(){return agent;}
    
    @Override
    protected void finalize()
    {
        if (!fileexists)
        {
            fileForMatlab.close();
            fileForHuman.close();
        }
    }
    
   // public CEtrainer(int width,  FeatureExtractor fex, int run, JTextArea textarea)
//    {
//        this(width, fex, run);
//        this.textarea = textarea;
//    }
    
        // quicksort a[left] to a[right]
    public void quicksort(double[] a, int left, int right) {
        if (right <= left) return;
        int i = partition(a, left, right);
        quicksort(a, left, i-1);
        quicksort(a, i+1, right);
    }

    // partition a[left] to a[right], assumes left < right
    @SuppressWarnings("empty-statement")
    private static int partition(double[] a, int left, int right) {
        int i = left - 1;
        int j = right;
        while (true) {
            while (a[++i] > a[right])      // find item on left to swap
                ;                               // a[right] acts as sentinel
            while (a[right] > a[--j])      // find item on right to swap
                if (j == left) break;           // don't go out-of-bounds
            if (i >= j) break;                  // check if pointers cross
            exch(a, i, j);                      // swap two elements into place
        }
        exch(a, i, right);                      // swap with partition element
        return i;
    }

 
    // exchange a[i] and a[j]
    private static void exch(double[] a, int i, int j) {
//        exchanges++;
        double swap = a[i];
        a[i] = a[j];
        a[j] = swap;
    }
    
	//look at interface?
    public void playGeneration()
    {
        int i, j;
        String str;
        double[] lambda = new double[1];
        
        if (!fileexists){
			System.out.println("File doesn't exist. quit.");
			return;
		}
		
		//evaluating w_track
		agent.setWeights(w_track);
		double score_track = game.playOneGame(agent, false);
		System.out.println("w_track is");
		System.out.println(agent);
		System.out.println("score for w_track is " + score_track);
		System.out.println("Now fitting w_track and get w_ls");
		
		//for(i=0; i<10; i++){
		//	System.out.println("Run " + i);
			learnOneGame(new AfterState());//learn w_ls
		//}
		
		//evaluating w_ls
		agent.setWeights(w_ls);
		double score_ls = game.playOneGame(agent, false);
		System.out.println("w_ls is");
		System.out.println(agent);
		System.out.println("one run score for w_ls is " + score_ls);
		
		
        for (int it=0; it<POPULATIONSIZE; it++)
        {
            for (i=0; i<agent.getNumFeatures(); i++)
            {
                wlist[it][i] = Math.sqrt(S[i])*rnd.nextGaussian() + M[i];
            }
            agent.setWeights(wlist[it]);
            // add a small random value to make keys unique
            //scorelist[it] = game.playNSteps(wlist[it], 1000) + 0.001*rnd.nextDouble();
//            scorelist[it] = game.playOneGameCapped(wlist[it], 10000, lambda) + 0.001*rnd.nextDouble();
            scorelist[it] = game.playOneGame(agent, false);
			
            truescorelist[it] = scorelist[it];
            sortedscores[it] = scorelist[it];
            str = "gen."+generation+"  #"+it+"   "+(int)(truescorelist[it]) 
                    + "  (approx: " + (int)(scorelist[it]) + ")";
            System.out.println(str);
        }
		
		
		
        
        //sort, get gamma, update M,S
        quicksort(sortedscores, 0, POPULATIONSIZE-1);
        
        int NELITE = (int) (POPULATIONSIZE * RHO);
        double GAMMA = sortedscores[NELITE-1];
        //double noise = Math.max(0,50*(1-generation/50.0));
        double noise = 0;
        smean = 0;
        for (i=0; i<agent.getNumFeatures(); i++)
        {
            Mnew[i] = 0;
            for (int it=0; it<POPULATIONSIZE; it++)
                if (scorelist[it]>=GAMMA){
                    Mnew[i] += wlist[it][i];
					w_track[i] = w_track[i] + 0.1*(wlist[it][i] -  w_track[i]);
				}
            Mnew[i] /= NELITE;

            Snew[i] = 0;
            for (int it=0; it<POPULATIONSIZE; it++)
                if (scorelist[it]>=GAMMA)
                    Snew[i] += (wlist[it][i]-Mnew[i])*(wlist[it][i]-Mnew[i]);
            Snew[i] /= NELITE;
            Snew[i] += noise;
            
            M[i] = (1-ALPHA)*M[i] + ALPHA*Mnew[i];
            S[i] = (1-ALPHA)*S[i] + ALPHA*Snew[i];
            smean += S[i];
        }
        smean /= agent.getNumFeatures();

        double avg = 0;
        for (int it=0; it<POPULATIONSIZE; it++)
        {
            avg += sortedscores[it];
        }
        avg /= POPULATIONSIZE;
        System.out.println("--------------------------------------");
        str = "gen.#"+generation+" \tmeanS:"+ String.format((Locale)null,"%8.4f ", smean) 
                + "\t  avgScore:"+(int)(avg) ;
        System.out.println(Arrays.toString(Mnew));
        System.out.println(Arrays.toString(Snew));

       // if (textarea != null)
//        {
//            textarea.append("w"+game.width+"  "+str+"\n");
//            JScrollPane scrollpane = (JScrollPane) textarea.getParent().getParent();
//            JScrollBar scrollbar = scrollpane.getVerticalScrollBar();        
//            textarea.validate();
//            scrollbar.validate();
//            scrollbar.setValue(scrollbar.getMaximum());
//            scrollbar.validate();
//                    
//        }
        System.out.println("w"+game.getWidth()+"  "+str);
        fileForHuman.println(str);
        str = String.format((Locale)null,"%d %8.4f %d ", generation, smean, (int)(sortedscores[0]));
        fileForMatlab.print(str);
        
        str = "";
        for (i=0; i<agent.getNumFeatures(); i++)
            str = str+ String.format((Locale)null, "%8.4f ", M[i]);
        System.out.println(str);
        fileForMatlab.print(str);
        str = "";
        for (i=0; i<agent.getNumFeatures(); i++)
            str = str+ String.format((Locale)null,"%8.4f ", S[i]);
        System.out.println(str);
        fileForMatlab.print(str);
        System.out.println("--------------------------------------");
        fileForMatlab.println();
        
        
        generation++;
    }
	
	
	//learn a least-squares fit for w_track
	public double learnOneGame(AfterState fromState){
		double totalRew = 0.0;
		AfterState state = fromState;//initial state
		int step=0;
		AfterState[] stateNextS;
		AfterState stateNext;
		
		int nGames=100;
		int piece;
		//TetrisAction tA;
        int action;
		double ret;
		double res=0;
		
		
		//System.out.println("In fitting w_ls learnOneGame---starting from state:");
		//System.out.println(state);
		while (true){	
			step++;
			System.out.println(String.format("\n\n#step %d:", step));
			
			//System.out.println("Agent weights are");
			//System.out.println(toString());
			
			//TD1
			double ret_ave = 0;
			for(int k=0; k<10; k++){
				ret_ave += TetrisGame.playOneGame(agent, state, false);
			}
			ret_ave /= 10; 
			System.out.println("   Average return form this state is " + ret_ave);
			if(step%5==0){
				System.out.println("Condition number of A is " + A.cond());
			}
			adjustWeightsLS(state, 0, true, new AfterState(), ret_ave);
			
			piece= TetrisGame.getNewPiece();
			action = agent.observeAndAct(res, piece, state.getBoard());
			stateNextS = state.putTile(piece, action, true);
			stateNext = stateNextS[1];
			res = stateNext.getReward();
			
			//TD0
			//adjustWeightsLS(state, piece, res<0, stateNext, res);
			
			
			//System.out.println(String.format("---take action %d(position:%d,rotation:%d)   ----reward:%f", tA.getIndex()+1, tA.getPosition(),tA.getRotation(), res));
			
			//			if(agent.getSaveOnPolicySamples()){
			//				System.out.println("Last state should be the same as: ");
			//				System.out.println(AfterState.board2Chars(agent.getSamples().getOnPolicyTrans(step-1).getBoard()));
			//			}
			
			totalRew += res;
			
			if (res<0){
				//GameOver();
				break;
			}
			
			state = stateNext;
		}
		System.out.println("total rewards = " + totalRew);
		return totalRew;
	}

	
	public void adjustWeightsLS(AfterState s, int piece,  boolean absorb, AfterState sNext, double rewards){
		double[] phi = agent.getFeatureVector(s.getBoard());
		double[] phinext = agent.getFeatureVector(sNext.getBoard());
		
		Matrix phiM = new Matrix(phi,1);
		phiM = phiM.transpose();
		Matrix phinextM;
		if(absorb){
			A.plusEquals(phiM.times(phiM.transpose()));
		}else{
			phinextM = new Matrix(phinext,1);
			phinextM = phinextM.transpose();
			A.plusEquals(phiM.times((phiM.minus(phinextM)).transpose()));
		}
		b.plusEquals(phiM.times(rewards));
		Matrix wM= A.solve(b);
		w_ls = wM.getColumnPackedCopy();
	}
}
