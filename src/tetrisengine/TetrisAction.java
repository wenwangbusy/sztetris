package tetrisengine;

import java.io.Serializable;

/** An action is (position, rotation)
 *  position is where to put the current piece
 *  rotation is how (in which rotation) to put the current piece
 */
public class TetrisAction implements Serializable {
    private int position;
    private int rotation;

    public TetrisAction(int position, int rotation){
        setPosition(position);
        setRotation(rotation);
    }
	public TetrisAction(){
		this(0,0);
	}
	public TetrisAction(TetrisAction a){
		this(a.getPosition(), a.getRotation());
	}
	
	public int getPosition(){return position;}
	public int getRotation(){return rotation;}
	public void setPosition(int pos){assert(0<=pos && pos<TetrisGame.getWidth()); position=pos;}
	public void setRotation(int rot){assert(0<=rot && rot<TetrisGame.getNrotations()); rotation=rot;}
	
	/* get the action index of the TetrisAction
	 * @return index of the TetrisAction in [0, NumberOfActions-1]
	 */
	/*
	 //slow
	public int getIndex(){
		int index=0;
		for(int pos=0; pos<TetrisGame.getWidth(); pos++){
			for(int rot=0; rot<TetrisGame.getNrotations(); rot++){
				if(pos==getPosition() && rot==getRotation()){
					return index;
				}
				index++;
			}
		}
		assert(0<=index && index<TetrisGame.getNactions());
		return index;
	}*/
	public int getIndex(){
		return getIndex(getPosition(), getRotation());
	}
    public static int getIndex(int pos, int rot){
        int index = rot + pos*TetrisGame.getNrotations();
        assert(0<=index&& index<TetrisGame.getNactions());
        return index;
    }
	
	//a factory method 
	public static TetrisAction getAction(int actionIndex){
		int pos = getPositionFromIndex(actionIndex);
		int rot = getRotationFromIndex(actionIndex);
		assert(rot == actionIndex%TetrisGame.getNrotations());
		return new TetrisAction(pos, rot);
	}
    public static int getPositionFromIndex(int actionIndex){
        assert(actionIndex>=0 && actionIndex<TetrisGame.getNactions());
        int pos = actionIndex/TetrisGame.getNrotations();
        assert(pos>=0 && pos<TetrisGame.getWidth());
        return pos;
    }
    public static int getRotationFromIndex(int actionIndex){
        assert(actionIndex>=0 && actionIndex<TetrisGame.getNactions());
        int rot = actionIndex - getPositionFromIndex(actionIndex)*TetrisGame.getNrotations();
        assert(rot>=0 && rot<TetrisGame.getNrotations());
        return rot;
    }
	
	
	@Override
    public String toString(){
        return String.format("a=(position:%d, rotation:%d)", getPosition(),getRotation());
    }
}
