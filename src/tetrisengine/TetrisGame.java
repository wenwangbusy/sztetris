package tetrisengine;

import agent.*;
import features.*;

import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.logging.*;

/*** Game dynamics class
 * @see FeatureUserAgent
 */
public class TetrisGame {
    private final static int PADDING = 3;//public static fileds are bad. will change.
    private final static int NTETROMINOES = 7;
    private final static int NROTS = 4;
    private final static int T_WALL  = 8;    
    private final static int T_EMPTY = 0;
    private final static Color[] tilecolor = 
    {
        Color.WHITE,
        Color.BLUE,
        Color.CYAN,
        Color.GRAY,
        Color.GREEN,
        Color.MAGENTA,
        Color.RED,
        Color.ORANGE,
        Color.BLACK
    };
	private final static double INF = -1e10;

	
    public static int[][][][] tiles = new int[NTETROMINOES][getNrotations()][4][4];
    public static int[][][]   tilebottoms = new int[NTETROMINOES][getNrotations()][4];
    
    private static int width, height; /* size of the tetris domain*/

    //private AfterState state;
	
    //private Random rnd;
    
	BufferedReader in;
    
	int numGames;
	static int RUNS;
	
	
	/* @param w width of the game
	 * @param h height of the game	
	 * @param numGames number of games to play
	 */
    public TetrisGame(int w, int h, int numGames)
    {
		int i, j, x, y;

        width=w;
		height=h;
		setNumGames(numGames);

        //state = new AfterState();
        //rnd = new Random();

        int [][] tile;
        
        for (i=0; i<NTETROMINOES; i++)
        {
            for (j=0; j<getNrotations(); j++)
            {
                tile = generateTile(i,j);
                for (x=0; x<getNrotations(); x++)
                {
                    int last = -100;
                    for (y=0; y<getNrotations(); y++)
                    {
                        tiles[i][j][x][y] = tile[x][y];
                        if (tile[x][y] != 0)
                            last = y;
                    }
                    tilebottoms[i][j][x] = last;
                }
                //tiles[i][j].clone(generateTile(i,j));
            }
        } 
		
		
    }
	
	public void setNumGames(int n){
		this.numGames=n;
	}
	public int getNumGames(){return numGames;}
	
	public static void setRUNS(int runs){ RUNS = runs;}
	public static int getRUNS(){ return RUNS;}

	public static int getPADDING(){return PADDING;}
	public static int getNTETROMINOES(){return NTETROMINOES;}
	public static int getT_WALL(){return T_WALL;}
	public static int getT_EMPTY(){return T_EMPTY;}
	public static int getHeight(){return height;}
	public static int getWidth(){return width;}
	public static int getNrotations(){return NROTS;}
	public static int getNactions(){return getNrotations()* getWidth();}
	public static double getINF(){return INF;}
	    
	public static int getDimX(){return  getWidth() + 2*getPADDING();}
	public static int getDimY(){return  getHeight() + 2*getPADDING();}
	public static int getBoardSize(){return getDimX()*getDimY();}
	
    public static final int[] scoretable = {0,1,2,3,4};
	
	public static double[] getBoardVector(int[][] bd){
		double[] bd_v = new double[getBoardSize()];
		int count=0;
		for(int i=0; i<bd.length; i++){
			for(int j=0; j<bd[i].length; j++){
				bd_v[count++] = bd[i][j];
			}
		}
		assert(count==bd_v.length);
		return bd_v;
	}
	
	
	/*
	 get the surface (top two) layers for each column; 
	 basically setting the other layers to constant (1)
	 */
	public static int[][] getSurfaceBoard(int[][] bd){
		final int constantPosition=1;
		int i,j;
		for(i=0; i<getWidth(); i++)//column i
        {
			//looks for the heighest brick in this column? so the original is on the top of the board
            for(j=0; j<getHeight(); j++)//row j (counting from the top)
            {
                if (bd[i+getPADDING()][j+getPADDING()] != 0)
                    break;
            }
			int height = getHeight()-j;
			if(height<=2)
				continue;
			
			for(int k=height-2; k>0; k--){
				bd[i+getPADDING()][(getHeight()-k) +getPADDING()] = constantPosition;
			}
		}
		return bd;
	}
	
	public static double[][] getBoardFromVector(int[] bdV){
		assert(bdV.length==getBoardSize());
		double[][] res = new double[getDimX()][getDimY()];
		int count=0;
		for(int i=0; i<getDimX(); i++){
			for(int j=0; j<getDimY(); j++){
				res[i][j] = bdV[count++];
			}
		}
		return res;
	}
	
	
	/** generate a tile based on the current piece and rotation; a tile is 
	 * @param piece type of piece coming in, 0:I; 1:L; 2:J; 3:T; 4:Z; 5:S; 6:0; piece and rotation is both represented in 16 bits 
	 * @param rot rotations of the piece, ranging from 0 to 3
	 */
    int [][] generateTile(int piece, int rot)
    {
        int [][] t;
        switch (piece)
        {
            case 0: 
                t = new int[][]{{1, 1, 1, 1}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}}; 
                break;
            case 1: 
                t = new int[][]{{1, 1, 1, 0}, {1, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}}; 
                break;                
            case 2: 
                t = new int[][]{{1, 1, 1, 0}, {0, 0, 1, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}}; 
                break;                
            case 3: 
                t = new int[][]{{1, 1, 1, 0}, {0, 1, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}}; 
                break;                
            case 4: 
                t = new int[][]{{1, 1, 0, 0}, {0, 1, 1, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}}; 
                break;                
            case 5: 
                t = new int[][]{{0, 1, 1, 0}, {1, 1, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}}; 
                break;                
            case 6: 
                t = new int[][]{{1, 1, 0, 0}, {1, 1, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}}; 
                break; 
            default: // non-existent piece
                t = new int[][]{{1, 1, 1, 1}, {1, 1, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}}; 
                break; 
        }
        
        
        int[][] t2 = new int[getNrotations()][getNrotations()];
        int x, y;
        switch (rot)
        {
            case 0:
                for (x=0; x<getNrotations(); x++)
                    for (y=0; y<getNrotations(); y++)
                        t2[x][y] = t[x][y];
                break;
            case 1:
                // 1110      0000     
                // 0100      1000       
                // 0000  ->  1100          
                // 0000      1000          
                for (x=0; x<getNrotations(); x++)
                    for (y=0; y<getNrotations(); y++)
                        t2[x][y] = t[y][3-x];
                break;
            case 2:
                for (x=0; x<getNrotations(); x++)
                    for (y=0; y<getNrotations(); y++)
                        t2[x][y] = t[3-x][3-y];
                break;
            case 3:
                for (x=0; x<getNrotations(); x++)
                    for (y=0; y<getNrotations(); y++)
                        t2[x][y] = t[3-y][x];
                break;                
        }
                    
        int emptyrow = 0;
        int emptycol = 0;
        
        // determine number of empty columns
        outerloop1:
        for (x=0; x<getNrotations(); x++)
        {
            for (y=0; y<getNrotations(); y++)   
            {
                if (t2[x][y] != 0)
                    break outerloop1;
            }
            emptycol++;
        }
        
        // determine number of empty rows
        outerloop2:
        for (y=0; y<getNrotations(); y++)
        {
            for (x=0; x<getNrotations(); x++)   
            {
                if (t2[x][y] != 0)
                    break outerloop2;
            }
            emptyrow++;
        }
        
        int[][] t3 = new int[getNrotations()][getNrotations()];
        
        for (x=emptycol; x<getNrotations(); x++)
        {
            for (y=emptyrow; y<getNrotations(); y++)
                t3[x-emptycol][y-emptyrow] = t2[x][y];
        }
        return t3;
    }

    public static int getNewPiece(){
		Random rnd = new Random();
        //return rnd.nextInt(7);
        return rnd.nextInt(2)+4;
    }
    
    public static double playOneGame(FeatureUserAgent agent, boolean printBoard){
        return playOneGame(agent, new AfterState(), printBoard);
    }
	/** play a game with the given agent; also record the feature vectors phi and collect samples from phi; note agent's state keeps updating
	 @param agent the given agent (must have some policy)
	 @param fromState the starting state for the game
	 @return the total rewards for the game
	 */
    public static double playOneGame(FeatureUserAgent agent, AfterState fromState, boolean printBoard)
    {
        double res=0;
        double totalRew = 0.0;
        AfterState state = fromState;//initial state
		int step=0;
		AfterState[] stateNextS;
		AfterState stateNext;
		
        while (true){	
			step++;
            int piece = getNewPiece();
            
			state.decolorize();
			
//			System.out.println(state);
			//System.out.println("the Surface of the state is ");
			//System.out.println(new AfterState(getSurfaceBoard(state.getBoard()))); 
			
			
			int action= agent.observeAndAct(res, piece, state.getBoard());
		
			stateNextS = state.putTile(piece, action, true);
			stateNext = stateNextS[1];
			res = stateNext.getReward();
			
			
			if(printBoard){
				//for shooting a video
				try {
					Thread.sleep(700);
				} catch(InterruptedException ex) {
					Thread.currentThread().interrupt();
				}
				if(printBoard){
					System.out.println(String.format("\n\n#step %d:", step));
					System.out.println("---the new piece is " + piece);
				}
				System.out.println(String.format("---take action %d(position:%d,rotation:%d)   ----reward:%f", action, TetrisAction.getPositionFromIndex(action),TetrisAction.getRotationFromIndex(action), res));
	//			System.out.println("Before erasing line if any: ");
	//			System.out.println(stateNextS[0]);
				//to see the new piece better, use the following
				//System.out.println("After erasing line if any (next state) ");
				System.out.println(stateNext);
				/*
                System.out.println("Feature is ");
                double[] phinextInd = agent.getFeatureVector(stateNext.getBoard());
                for(int i=0; i<phinextInd.length; i++)
                    if(phinextInd[i]!=0.0)
                        System.out.println(i+":"+phinextInd[i]+",");
				 */
            }
			
			totalRew += res;
			
			if (res<0){
                agent.GameOver();
                break;
            }

			state = stateNext;
        }
        //System.out.println("total rewards = " + totalRew);
        return totalRew;
    }

    
}
 