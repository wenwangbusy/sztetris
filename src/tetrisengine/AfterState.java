package tetrisengine;

import java.util.*;
import java.io.Serializable;

/** AfterState is the game status update class
 * @see TetrisGame
 * 
 */
public class AfterState implements Serializable {
		
	private int board[][];
    private int skyline[];
	
	private int lastLandingHeight=0;
    private double reward=0;  //this is actuallyt the last reward; lastResult is replaced by reward
    private TetrisAction lastAction=new TetrisAction();
	
	private final static int PADDING = TetrisGame.getPADDING();
	private final static int width = TetrisGame.getWidth();
	private final static int height = TetrisGame.getHeight();
	
	public AfterState(){
		board=new int[TetrisGame.getDimX()][TetrisGame.getDimY()];
		skyline=new int[TetrisGame.getDimX()];
		clear();
	}
	
	/* a copy constructor */
    public AfterState(AfterState s) 
    {
		this();
        copyBoard(s.board, board);
		//board = s.getBoard();//not correct, why? array copy. careful. 
		//skyline = s.getSkyline();
		for(int i=0; i<skyline.length; i++){
			skyline[i] = s.skyline[i];
		}
    }

    public AfterState(int [][] b)
    {
		this();
        copyBoard(b, board);
		//board = b;
        updateSkyline();
    }
    
    
    public static AfterState getRandomState(){
        AfterState s=new AfterState();
        //set s to random state
        throw new RuntimeException();
        //return s;
    }
    

	public void setLastAction(int pos, int rot){lastAction.setPosition(pos); lastAction.setRotation(rot);}
	public double getReward(){return reward;}
	public void setReward(double rew){reward=rew;}
	public int[][] getBoard(){return board;}
    //public void setBoard(int[][] given){board=given;}//deep copy?
	public int getBoard(int i, int j){return board[i][j];}
	public int[] getSkyline(){return skyline;}

	
	//this clears the board?
    public int[][] clear()
    {
    	int i, j;
		for(i=0; i<board.length; i++)
			for(j=0; j<board[i].length; j++)
				board[i][j] = TetrisGame.getT_WALL();
		
		for(i=0; i<width; i++)
			for(j=0; j<height+PADDING; j++)
				board[i+PADDING][j] = TetrisGame.getT_EMPTY();
			updateSkyline();//?
		
		return board;
	}


	//what is this?
    public void updateSkyline()
    {
        int i, j;
        for(i=0; i<skyline.length; i++)
        {
            for(j=0; j<board[i].length; j++)
            {
                if (board[i][j] != 0)
                    break;
            }
            skyline[i] = j;
        }
    }

    public static void copyBoard(int[][] fromBoard, int[][] toBoard)
    {
		assert(fromBoard!=null);
		assert(toBoard!=null);
        int i,j;
    	for(i=0; i<fromBoard.length; i++)
        	for(j=0; j<fromBoard[i].length; j++)
            	toBoard[i][j] = fromBoard[i][j];
    }

    public static int[][] copyBoardwithAlloc(int[][] fromBoard)
    {
        int[][] toBoard = new int[fromBoard.length][fromBoard[0].length];
        copyBoard(fromBoard, toBoard);
        return toBoard;
    }

	public AfterState[] putTile(int type, int actionIndex, boolean bupdateSkyline){
		return putTile(type, TetrisAction.getRotationFromIndex(actionIndex), TetrisAction.getPositionFromIndex(actionIndex), bupdateSkyline);
	}
	
/** the game engine, which updates the board with a new piece at a certain position and rotation 
 * @param type the new piece
 * @param rot rotation of the piece
 * @param pos position of the piece
 * @param bupdateSkyline whether update skyline or not
 * @return [StatebeforeErasing, StateAfterErasing] 
 * speed can be improved by removing those new
 */
   public AfterState[] putTile(int type, int rot, int pos, boolean bupdateSkyline)
    {
		
		AfterState[] res = new AfterState[2];
		
        int[][] tile = TetrisGame.tiles[type][rot];
        int ofs = 10000;
        int x, y;

        AfterState s = new AfterState(this);
		
        for(x=0; x<4; x++)
        {
            ofs = Math.min(ofs, skyline[x+pos+PADDING]-TetrisGame.tilebottoms[type][rot][x]-1);
        }
        if (ofs<PADDING)   // failure: does not fit in there
        {
            s.setReward(-1);
			res[0] = s;
			res[1] = s;
            return res;
        }
        for(x=0; x<4; x++)
            for (y=0; y<4; y++)
                if (tile[x][y] != 0)
                    s.board[x+pos+PADDING][y+ofs] = type+1;
        s.lastLandingHeight = ofs;

		
		AfterState sBeforeErasing = new AfterState(s);
		res[0] = sBeforeErasing;
		
		s.setReward((double)(s.eraseLines()));
		
	//	if(s.getReward()>0){
//			System.out.println("making one or two lines. Before erasing");
//		}else{
//			System.out.println("not making any line. state after erasing should be unchanged.");
//		}
//		System.out.println(sBeforeErasing);
		
		s.setLastAction(pos,rot);
		
        if (bupdateSkyline)
            s.updateSkyline();
        
		res[1] = s;
		
		return res;
    }

	/** erase one or more lines if they are full 
	 * @return number of erased lines
	*/
    public int eraseLines()
    {
        int x, y, y2;
        int nErased = 0; 
        boolean isFull; //a line (row) is full or not
        int debugy = 0;
        for (y=height-1; y>=0; y--)
        {
            debugy ++;
            isFull = true;
            for (x=0; x<width; x++)
            {
                if (board[x+PADDING][y+PADDING] == 0) //no brick
                {
                    isFull = false;
                    break;
                }
            }
            if (isFull)
            {
                for(y2=y; y2>=0; y2--)
                {
                    for (x=0; x<width; x++)
                        board[x+PADDING][y2+PADDING] = board[x+PADDING][y2-1+PADDING];
                }
                y++; //check that line again
                nErased++;
            }
        }
        return nErased;
    }

    // used for pretty printing
    // by default, all tiles have different colors (different numbers in the board)
    // decolorizing is used to stand out the new tiles; see TetrisGame.playOneGame()
    public void decolorize()
    {
        for (int y=0; y<height; y++)
        {
            for (int x=0; x<width; x++)
            {
                if (board[x+PADDING][y+PADDING] != TetrisGame.getT_EMPTY())
                {
                    board[x+PADDING][y+PADDING] = 9;
                }
            }
        }
    }

	private static final char[] chars = " 0123456#x".toCharArray();
	
	
	/*the board starts from top 
	 */
    public static String board2Chars(int[][] bd){
		StringBuilder s = new StringBuilder();
		
        for (int y=0; y<height+1; y++)//a row
        {
            for (int x=-1; x<width+1; x++)//a column
            {
                s.append(chars[bd[x+PADDING][y+PADDING]]);
            }
            s.append("\n");
        }
        return s.toString();
	}
	
	/* return a string representing each board position with a character
	 */
	@Override
    public String toString(){return board2Chars(board);}

}
