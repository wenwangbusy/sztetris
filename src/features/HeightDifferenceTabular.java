package features;

import tetrisengine.TetrisGame;

/** This is a lookup table representation of the BIHoles features
 @see BIHoles
 */
public class HeightDifferenceTabular extends FeatureExtractor {
	
	private static HeightDifference fex=new HeightDifference();//without static not compliling: supertype constructor
	private final static int nGrids=TetrisGame.getHeight()+1;// BI feature value ranges from [0, TetrisGame.getHeight()]
	
    public HeightDifferenceTabular(){
		super(fex.getNumFeatures()*nGrids, "HeightDifferenceTabular");//number of features is 21*21: 21 BI features, each feature ranging from [0,20]
    }


    @Override
    public double[] getFeatures(int[][] board)
    {
		double[] res=new double[getNumFeatures()];
		double[] phiBI = fex.getFeatures(board);
		int start;
		for(int i=0; i<phiBI.length; i++){
			start = i*nGrids;
			res[start + (int)phiBI[i]]=1;
		}
		return res;
	}

}