package features;

import tetrisengine.TetrisGame;

/** This is a lookup table representation of the BIHoles features
 @see BIHoles
 */
public class BIHolesTabular extends FeatureExtractor {
	
	private static BIHoles fex=new BIHoles();//without static not compliling: supertype constructor
	private final static int nGrids=TetrisGame.getHeight()+1;// BI feature value ranges from [0, TetrisGame.getHeight()]
	
    public BIHolesTabular(){
		super(fex.getNumFeatures()*nGrids, "BIHolesTabular");//number of features is 21*21: 21 BI features, each feature ranging from [0,20]
    }


    @Override
    public double[] getFeatures(int[][] board)
    {
		double[] res=new double[getNumFeatures()];
		double[] phiBI = fex.getFeatures(board);
		int start;
		for(int i=0; i<phiBI.length; i++){
			start = i*nGrids;
			res[start + (int)phiBI[i]]=1;
		}
		return res;
	}

}