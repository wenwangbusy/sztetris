package features;

import tetrisengine.TetrisGame;

/** This is a lookup table representation of the Holes features
 * @see FeatureExtractor
 */
public class HolesTabular extends FeatureExtractor {
	
	private final static Holes fex = new Holes();//without static not compliling: supertype constructor
	private final static int nGrids=TetrisGame.getHeight();// number of holes ranges from [0, TetrisGame.getHeight()-1]
	
    public HolesTabular(){
		super(fex.getNumFeatures()*nGrids, "HolesTabular");
    }

    @Override
    public double[] getFeatures(int[][] board)
    {
		double[] res=new double[getNumFeatures()];
		double[] phiBI = fex.getFeatures(board);
		int start;
		for(int i=0; i<phiBI.length; i++){
			start = i*nGrids;
			res[start + (int)phiBI[i]]=1;
		}
		return res;
	}

    public String toString(){ return fex.toString();}
}