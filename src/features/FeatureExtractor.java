package features;

import java.util.List;
import java.util.ArrayList;
import agent.*;

/** 
 * A feature extractor(non-sparse )
 * 
 */
public class FeatureExtractor extends FeatureExtractorGeneric{
	public FeatureExtractor(int nFeatures, String name){
		super(nFeatures, "FeatureExtractor");
	}
	
	public double getValue(double[] phi, double[] w){
		if(phi.length != w.length){
			throw new RuntimeException("Error. phi.length=" + phi.length + ", but w.length="+w.length);
		}
		double v=0;
		for(int i=0; i<phi.length; i++){
			v = v + phi[i]*w[i];	
		}
		return v;
	}
    
    public List<TransitionPhi> getTransitions(List<Transition> transits){
        List<TransitionPhi> res = new ArrayList<TransitionPhi>();
        //System.out.println("in getTransitions(), transits.size()="+transits.size()+", first sample is ");
        System.out.println(transits.get(1));
        for(Transition t: transits){
            assert(t!=null);
            res.add(new TransitionPhi(this, t));
        }
        /*
        Transition t=null;
        for(int i=0; i<transits.size(); i++){
            t=transits.get(i);
            if(t==null)
                System.out.println("i="+i);
            res.add(new TransitionPhi(this, t));
        }
         */
        return res;
    }

    
	@Override
	public double getValue(int[][] board, double[] w){
		return getValue(getFeatures(board), w);
	}
			
	public double[] getFeatures(int[][] board){return new double[0];}
}
