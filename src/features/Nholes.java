package features;

import tetrisengine.TetrisGame;

/** the total number of hole features. 
 */
public class Nholes extends FeatureExtractor {

	public final static int numFeatures=1;
	public Nholes(){
		super(numFeatures, "BIExtended");
	}

	@Override
    public double[] getFeatures(int[][] board)
    {
		double[] phi = new double[getNumFeatures()];
        int[] heights = new int[TetrisGame.getWidth()];
        int maxh = 0;
        int nholes = 0;
		//int[] nholesOfColumn = new int[TetrisGame.getWidth()];
		
        int i, j;
        for(i=0; i<TetrisGame.getWidth(); i++)//column i
        {
			//looks for the heighest brick in this column? so the original is on the top of the board
            for(j=0; j<TetrisGame.getHeight(); j++)//row j
            {
                if (board[i+TetrisGame.getPADDING()][j+TetrisGame.getPADDING()] != 0)
                    break;
            }
            heights[i] = TetrisGame.getHeight()-j;
            for(; j<TetrisGame.getHeight(); j++)
            {
                if (board[i+TetrisGame.getPADDING()][j+TetrisGame.getPADDING()] == 0){
                    //nholesOfColumn[i]++;
					nholes++;
				}
            }
//            phi[OFS_HEIGHTS+i] = heights[i];
//            if (i>0)
//            {
//                phi[OFS_DELTAS + i-1] = Math.abs(heights[i]-heights[i-1]);
//            }
//            if (heights[i]>maxh)
//                maxh = heights[i];
        }
//        phi[OFS_MISC + 0] = TetrisGame.getHeight()-maxh;
        phi[0] = nholes;
		//System.out.println("the number of holes is "+ phi[0]);
        return phi;
    }
	

}
