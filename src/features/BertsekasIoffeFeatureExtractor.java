package features;

import tetrisengine.TetrisGame;

/** The class of BI features, 
 * @see TetrisGame
 */
public class BertsekasIoffeFeatureExtractor extends FeatureExtractor {

    private static int OFS_HEIGHTS=0;
    private static int OFS_DELTAS=OFS_HEIGHTS + TetrisGame.getWidth();
    private static int OFS_MISC= OFS_DELTAS + (TetrisGame.getWidth()-1);

	
	public BertsekasIoffeFeatureExtractor(){
		super((TetrisGame.getWidth() + (TetrisGame.getWidth()-1) + 2)*1, "BI");
	}
	


	/*original BI features
	 total number of features is  21
	 The first 10 (=Width) features are the height of the columns. 
	 Features 11 -- 19 are the difference between successive columns. 
	 The last two features are the largest room left, and the number of holes. 
	 
	 Maybe should change the maximum height to maximum room
	 */
    @Override
    public double[] getFeatures(int[][] board)
    {
		double[] phi = new double[getNumFeatures()];
		
        int[] heights = new int[TetrisGame.getWidth()];
        int maxh = 0;
        int nholes = 0;
        int coltrans = 0;
        int rowtrans = 0;
        int welldepth = 0;
        int maxwd = 0;
        int holedepth = 0;
        int rh, lh;

        int i, j;
        for(i=0; i<TetrisGame.getWidth(); i++)//column i
        {
			//looks for the heighest brick in this column? so the origin is on the top of the board
            for(j=0; j<TetrisGame.getHeight(); j++)//row j
            {
                if (board[i+TetrisGame.getPADDING()][j+TetrisGame.getPADDING()] != 0)
                    break;
            }
            heights[i] = TetrisGame.getHeight()-j;
            for(; j<TetrisGame.getHeight(); j++)
            {
                if (board[i+TetrisGame.getPADDING()][j+TetrisGame.getPADDING()] == 0){
					nholes++;
				}
            }
            phi[OFS_HEIGHTS+i] = heights[i];
            if (i>0)
            {
                phi[OFS_DELTAS + i-1] = Math.abs(heights[i]-heights[i-1]);
            }
            if (heights[i]>maxh)
                maxh = heights[i];
        }
        phi[OFS_MISC + 0] = TetrisGame.getHeight()-maxh;
        phi[OFS_MISC + 1] = nholes;//the last feature is the number of holes

        return phi;
    }
	
}
