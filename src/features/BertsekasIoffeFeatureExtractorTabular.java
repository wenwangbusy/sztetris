package features;

//import tetrisengine.TetrisGame;

/** This is a lookup table representation of the BI features
 * @see BertsekasIoffeFeature
 * @see BertsekasIoffeFeatureExtended
 */
public class BertsekasIoffeFeatureExtractorTabular extends FeatureExtractor {
	
	private static BertsekasIoffeFeatureExtractorExtended fexBI=new BertsekasIoffeFeatureExtractorExtended();//without static not compliling: supertype constructor
	private final static int MaxBIPlusOne=21;//maximum BI feature value =20
	
    public BertsekasIoffeFeatureExtractorTabular(){
		super(fexBI.getNumFeatures()*MaxBIPlusOne, "BITabular");//number of features is 21*21: 21 BI features, each feature ranging from [0,20] 
		fexBI = new BertsekasIoffeFeatureExtractorExtended();
    }


    @Override
    public double[] getFeatures(int[][] board)
    {
		double[] res=new double[getNumFeatures()];
		double[] phiBI = fexBI.getFeatures(board);
		int start;
		for(int i=0; i<phiBI.length; i++){
			start = i*MaxBIPlusOne;
			res[start + (int)phiBI[i]]=1;
		}
		return res;
	}

}