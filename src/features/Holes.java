package features;

import tetrisengine.TetrisGame;

/** Hole features, including,
 the total number of hole features across columns;
 the number of hole features in each column; 
 and the maximum height
 */
public class Holes extends FeatureExtractor {

	public final static int numFeatures= 1 + TetrisGame.getWidth(); //2 + TetrisGame.getWidth();
	public Holes(){
		super(numFeatures, "Holes");
	}

	@Override
    public double[] getFeatures(int[][] board)
    {
		double[] phi = new double[getNumFeatures()];
        int[] heights = new int[TetrisGame.getWidth()];
        int maxh = -1;
        int nholes = 0;
		int[] nholesOfColumn = new int[TetrisGame.getWidth()];
		
        int i, j;
        for(i=0; i<TetrisGame.getWidth(); i++)//column i
        {
			//looks for the heighest brick in this column? so the original is on the top of the board
            for(j=0; j<TetrisGame.getHeight(); j++)//row j
            {
                if (board[i+TetrisGame.getPADDING()][j+TetrisGame.getPADDING()] != 0)
                    break;
            }
            heights[i] = TetrisGame.getHeight()-j;
            for(; j<TetrisGame.getHeight(); j++)
            {
                if (board[i+TetrisGame.getPADDING()][j+TetrisGame.getPADDING()] == 0){
                    nholesOfColumn[i]++;
					nholes++;
				}
            }
            if (heights[i]>maxh)
                maxh = heights[i];
        }
        phi[0] = TetrisGame.getHeight()-maxh;
//        phi[1] = nholes;
//		for(i=2; i<TetrisGame.getWidth()+2; i++)
//			phi[i] = nholesOfColumn[i-2];
		for(i=1; i<TetrisGame.getWidth()+1; i++)
			phi[i] = nholesOfColumn[i-1];
		
        return phi;
    }
}
