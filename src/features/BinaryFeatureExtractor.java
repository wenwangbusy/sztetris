package features;

import java.util.*;
import agent.*;


/** This is a binary(0/1) feature class */
public class BinaryFeatureExtractor extends SparseFeatureExtractor{
	
	public BinaryFeatureExtractor(int nFeatures, String name){
		super(nFeatures, "BinaryFeatureExtractor");
	}
	
	public double getValue(int[] phiIndex, double[] w){
		double v=0;
		for(int ind: phiIndex){
			v += w[ind];
		}
		return v;
	}

	@Override
	public double getValue(int[][] board, double[] w){
		return getValue(getFeatureIndices(board), w);
	}
    
    public List<TransitionPhiBinary> getBinaryTransitions(List<Transition> transits){
        List<TransitionPhiBinary> res = new ArrayList<TransitionPhiBinary>();
        for(Transition t: transits){
            assert(t!=null);
            res.add(new TransitionPhiBinary(this, t));
        }
        return res;
    }
    
    //might have a problem!
    public int[] getFeatureIndices(int[][] board){
        throw new RuntimeException("This shouldn't be called. ");
    }
}