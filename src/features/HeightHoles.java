package features;

import tetrisengine.TetrisGame;

/**
 the first 10 columns are the height features;
 plus one column for the max room, and 
 the last 10 columns are the number of hole features. 
 * @see TetrisGame
 */
public class HeightHoles extends FeatureExtractor {

	public final static int numFeatures = 2 * TetrisGame.getWidth() + 1;
	public HeightHoles(){
		super(numFeatures, "HeightHoles");
	}

	@Override
    public double[] getFeatures(int[][] board)
    {
		double[] phi = new double[getNumFeatures()];
        int[] heights = new int[TetrisGame.getWidth()];
        int maxh = 0;
        //int nholes = 0;
		int[] nholesOfColumn = new int[TetrisGame.getWidth()];
       
        int i, j;
        for(i=0; i<TetrisGame.getWidth(); i++)//column i
        {
			//looks for the heighest brick in this column? so the original is on the top of the board
            for(j=0; j<TetrisGame.getHeight(); j++)//row j
            {
                if (board[i+TetrisGame.getPADDING()][j+TetrisGame.getPADDING()] != 0)
                    break;
            }
            heights[i] = TetrisGame.getHeight()-j;
            for(; j<TetrisGame.getHeight(); j++)
            {
                if (board[i+TetrisGame.getPADDING()][j+TetrisGame.getPADDING()] == 0){
                    nholesOfColumn[i]++;
					//nholes++;
				}
            }
            phi[i] = heights[i];
            if (heights[i]>maxh)
                maxh = heights[i];
        }
        phi[i++] = TetrisGame.getHeight()-maxh;
        //phi[OFS_MISC + 1] = nholes;//the number of holes
		for(int m=i; m<i+10; m++){
			phi[m] = nholesOfColumn[m-i];
		}
        return phi;
    }
	

}
