package features;

import tetrisengine.TetrisGame;

/** HeightDifference and Hole features
 */
public class HeightDifferenceHoles extends FeatureExtractor {

	public final static int numFeatures= TetrisGame.getWidth() +  TetrisGame.getWidth() + 1;
	public HeightDifferenceHoles(){
		super(numFeatures, "HeightDifferenceHoles");
	}

	@Override
    public double[] getFeatures(int[][] board)
    {
		double[] phi = new double[getNumFeatures()];
        int[] heights = new int[TetrisGame.getWidth()];
        int maxh = 0;
		int[] nholesOfColumn = new int[TetrisGame.getWidth()];
		
        int i, j;
        for(i=0; i<TetrisGame.getWidth(); i++)//column i
        {
			//looks for the heighest brick in this column? so the original is on the top of the board
            for(j=0; j<TetrisGame.getHeight(); j++)//row j
            {
                if (board[i+TetrisGame.getPADDING()][j+TetrisGame.getPADDING()] != 0)
                    break;
            }
            heights[i] = TetrisGame.getHeight()-j;
            for(; j<TetrisGame.getHeight(); j++)
            {
                if (board[i+TetrisGame.getPADDING()][j+TetrisGame.getPADDING()] == 0){
                    nholesOfColumn[i]++;
				}
            }
            //phi[OFS_HEIGHTS+i] = heights[i];
            if (i>0)
            {
                phi[i-1] = Math.abs(heights[i]-heights[i-1]);
            }
            if (heights[i]>maxh)
                maxh = heights[i];
        }
        phi[i-1] = TetrisGame.getHeight()-maxh;
		for(int m=i; m<i+TetrisGame.getWidth(); m++){
			phi[m] = nholesOfColumn[m-i];
		}
		
        return phi;
    }
	

}
