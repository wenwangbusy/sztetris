package features;

import tetrisengine.TetrisGame;

/** Height Difference features
 */
public class HeightDifference extends FeatureExtractor {

	public final static int numFeatures=TetrisGame.getWidth();
	public HeightDifference(){
		super(numFeatures, "HeightDifference");
	}

	@Override
    public double[] getFeatures(int[][] board)
    {
		double[] phi = new double[getNumFeatures()];
        int[] heights = new int[TetrisGame.getWidth()];
        int maxh = 0;
		
        int i, j;
        for(i=0; i<TetrisGame.getWidth(); i++)//column i
        {
			//looks for the heighest brick in this column? so the original is on the top of the board
            for(j=0; j<TetrisGame.getHeight(); j++)//row j
            {
                if (board[i+TetrisGame.getPADDING()][j+TetrisGame.getPADDING()] != 0)
                    break;
            }
            heights[i] = TetrisGame.getHeight()-j;
            if (i>0)
            {
                phi[i-1] = Math.abs(heights[i]-heights[i-1]);
            }
            if (heights[i]>maxh)
                maxh = heights[i];
        }
        phi[i-1] = TetrisGame.getHeight()-maxh;
        return phi;
    }
	

}
