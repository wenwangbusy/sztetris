package features;

/** 
 * A feature extractor (not binary)
 * 
 */
public abstract class FeatureExtractorGeneric{
    private int nFeatures;
    private String name;

	public FeatureExtractorGeneric(int nFeatures, String name){
		setNumFeatures(nFeatures);
		setName(name);
		System.out.println("using " + getName() + " features");
		System.out.println(String.format("Number of features: %d ", nFeatures));
	}

	public void setName(String name){this.name=name;}
	public String getName(){return name;}
	
	public int getNumFeatures(){return nFeatures;}
	public void setNumFeatures(int nFeatures){this.nFeatures=nFeatures;}
	
	public abstract double getValue(int[][] board, double[] w);
}
