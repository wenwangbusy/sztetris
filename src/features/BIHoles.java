package features;

import tetrisengine.TetrisGame;

/** The class of extneded BI features.
 extended with the number of hole in each column; 
 removed the total number of hole features.
 total number of features is 21 + 10 -1 = 30;
 the first 20 columns are the same as BI features;
 the last 10 columns are the number of hole features. 
 * @see TetrisGame
 * @see BIFeatureExtractor
 * @see BITabular
 */
public class BIHoles extends FeatureExtractor {

    private static int OFS_HEIGHTS=0;
    private static int OFS_DELTAS=OFS_HEIGHTS + TetrisGame.getWidth();
    private static int OFS_MISC= OFS_DELTAS + (TetrisGame.getWidth()-1);

	public final static int numFeatures=( (TetrisGame.getWidth() + (TetrisGame.getWidth()-1) + 2)*1  -1) + TetrisGame.getWidth();
	public BIHoles(){
		super(numFeatures, "BIExtended");
	}

	@Override
    public double[] getFeatures(int[][] board)
    {
		double[] phi = new double[getNumFeatures()];
        int[] heights = new int[TetrisGame.getWidth()];
        int maxh = 0;
        //int nholes = 0;
		int[] nholesOfColumn = new int[TetrisGame.getWidth()];
		
        int i, j;
        for(i=0; i<TetrisGame.getWidth(); i++)//column i
        {
			//looks for the heighest brick in this column? so the original is on the top of the board
            for(j=0; j<TetrisGame.getHeight(); j++)//row j
            {
                if (board[i+TetrisGame.getPADDING()][j+TetrisGame.getPADDING()] != 0)
                    break;
            }
            heights[i] = TetrisGame.getHeight()-j;
            for(; j<TetrisGame.getHeight(); j++)
            {
                if (board[i+TetrisGame.getPADDING()][j+TetrisGame.getPADDING()] == 0){
                    nholesOfColumn[i]++;
					//nholes++;
				}
            }
            phi[OFS_HEIGHTS+i] = heights[i];
            if (i>0)
            {
                phi[OFS_DELTAS + i-1] = Math.abs(heights[i]-heights[i-1]);
            }
            if (heights[i]>maxh)
                maxh = heights[i];
        }
        phi[OFS_MISC + 0] = TetrisGame.getHeight()-maxh;
        //phi[OFS_MISC + 1] = nholes;//the last feature is the number of holes
		
		for(i=0; i<10; i++){
			//phi[OFS_MISC+2 + i] = nholesOfColumn[i];
			phi[OFS_MISC+1 + i] = nholesOfColumn[i];
		}
		
        return phi;
    }
	

}
