package features;

import tetrisengine.TetrisGame;
import java.util.*;

/** This is a binary(0/1) encoding of feature of BIHoles
 * The class takes advantage of the sparsity, which is advantageous in computation and I/O
 * @see BertsekasIoffeFeatureExtractorTabular
 */
public class BinaryBITabular extends BinaryFeatureExtractor{
	private static BIHoles fexBI=new BIHoles();//without static not compliling: supertype constructor
	private final static int MAXBIPLUSONE = TetrisGame.getHeight() + 1;//the largest (maximum) BI feature value =TetrisGame.getHeight() which is 20 for (10, 20) game
	
    public BinaryBITabular(){
		super(fexBI.getNumFeatures()*MAXBIPLUSONE, "BinaryBITab");
		//super(15*MAXBIPLUSONE, "BinaryBITab");//hole only features
		fexBI = new BIHoles();
    }

	/*get the indices for the nonzero entries (1) in the feature vector
	 */
//	@Override
//    public Integer[] getFeatureIndices(int[][] board)
//    {
//		List<Integer> resL = new ArrayList<Integer>();
//
//		double[] phiBI = fexBI.getFeatures(board);
//		int start;
//		for(int i=0; i<phiBI.length; i++){
//			start = i*MAXBIPLUSONE;
//			//res[start + (int)phiBI[i]]=1;
//			resL.add(start + (int)phiBI[i]);
//		}
//		return resL.toArray(new Integer[resL.size()]);
//	}
	/* Use only the holes; partion into bins of width 1*/
	@Override
    public int[] getFeatureIndices(int[][] board)
    {
		double[] phiBI = fexBI.getFeatures(board);
		int[] res = new int[phiBI.length];
		int start;
		for(int i=0; i<phiBI.length; i++){
			start = (i-0)*MAXBIPLUSONE;
			res[i] = start + (int)phiBI[i];
		}
        return res;
	}
	
	public double[] getFeatures(int[] phiIndex){
		double[] res = new double[getNumFeatures()];
		for(int i=0; i<phiIndex.length; i++){
			res[phiIndex[i]] = 1.0; 
		}
		return res;
	}
	
	
}