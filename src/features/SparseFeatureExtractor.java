package features;

/*
 A sparse feature class. 
 */
public abstract class SparseFeatureExtractor extends FeatureExtractorGeneric{
	
	//do you have continue writing these constructors on and on?
	public SparseFeatureExtractor(int numFeatures, String name){
		super(numFeatures, "SparseFeatureExtractor");
	}	

	//public static double getValue(int[] phiIndex, double[] w){};
	
	public abstract int[] getFeatureIndices(int[][] board);
}
