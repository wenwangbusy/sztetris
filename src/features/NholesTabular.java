package features;

import tetrisengine.TetrisGame;

/** This is a lookup table representation of the Nholes features
 @see Nholes
 */
public class NholesTabular extends FeatureExtractor {
	
	private static Nholes fex=new Nholes();//without static not compliling: supertype constructor
	private final static int nGrids=TetrisGame.getWidth() * (TetrisGame.getHeight()-1);// [0, nGrids-1] is the range of total number of holes
	
    public NholesTabular(){
		super(fex.getNumFeatures()*nGrids, "NholesTabular");//number of features is 21*21: 21 BI features, each feature ranging from [0,20]
    }


    @Override
    public double[] getFeatures(int[][] board)
    {
		double[] res=new double[getNumFeatures()];
		double[] phi = fex.getFeatures(board);
		int start;
		for(int i=0; i<phi.length; i++){
			start = i*nGrids;
			res[start + (int)phi[i]]=1;
		}
		return res;
	}

}