package features;

import tetrisengine.TetrisGame;

/** Use the board directly as features
 * @see TetrisGame
 */
public class BoardFeatureExtractor extends FeatureExtractor {

    public BoardFeatureExtractor(){
        super(TetrisGame.getBoardSize(), "Board");
    }

	@Override
    public double[] getFeatures(int[][] board)
    {
		//return TetrisGame.getBoardVector(board);//use the original board
		return TetrisGame.getBoardVector(TetrisGame.getSurfaceBoard(board));//use only the board surfaces
    }

}
