close all; clc;
dir = '/Users/admin/sztetris/';
fphi=[dir 'samplesCE_BI_run00_phi.txt'];
fpiece=[dir 'samplesCE_BI_run00_piece.txt'];
fa=[dir 'samplesCE_BI_run00_action.txt'];
fphinext=[dir 'samplesCE_BI_run00_phinext.txt'];
fr=[dir 'samplesCE_BI_run00_reward.txt'];
fabs=[dir 'samplesCE_BI_run00_absorb.txt'];

% fid = fopen(fpiece);
% piece=fscanf(fid, '%d');
% fclose(fid);
% size(piece)
% 
% fid = fopen(fabs);
% absorb=fscanf(fid, '%d');
% fclose(fid);
% size(absorb)
% 
% fid = fopen(fr);
% reward=fscanf(fid, '%f');
% fclose(fid);
% size(reward)
% 
% 
% fid = fopen(fa);
% action = fscanf(fid, '%d %d %d', [3,inf]);
% fclose(fid);
% size(action)
% actIndex = action(1,:) +1;
% unique(actIndex)
% 
% 
% fid = fopen(fphi);
% Phi = fscanf(fid, '%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f ', [21,inf]);
% fclose(fid);
% size(Phi)
% 
% fid = fopen(fphinext);
% Phinext = fscanf(fid, '%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f ', [21,inf]);
% fclose(fid);
% size(Phinext)

w_CE=[-3.4668 -8.5783 -15.6468 -6.1543 -9.8836 1.4784 -16.2427 -9.3328 -3.4838 -9.9378...
    -15.8604 0.1275 -23.8577 0.8403 -8.8838 2.0852 -32.9442 1.6128 -26.2846 2.8297 -57.4184]';
[maxwCE, ind_maxwCE] = max(w_CE);
[minwCE, ind_minwCE] = min(w_CE);

Nactions=40;
Nfeatures = size(Phi,1);
states = [1:Nactions:size(Phi,2)];
gamma =0% 0% 1;
deltaA=-1;%-0.1%-1;%-0.1;


terms=find(absorb==1);
Phinext(:,terms) = zeros(Nfeatures, length(terms));
sum(reward(terms)) == -length(terms)

% for i=1:size(Phi,2)
%     samples(i).state =  
% end

T=10;
err=zeros(T,1);
w{4}= w_CE;% rand(size(Phi,1), 1);
w{5}= w_CE;
for iter=1%:T
    A{4}=deltaA* eye(length(w{4}), length(w{4}));
    b{4}=zeros(length(w{4}),1);
    A{5}=deltaA* eye(length(w{4}), length(w{4}));
    b{5}=zeros(length(w{4}),1);    
    for ind=1:length(states)
        s = states(ind);
        phi_s = Phi(:,s);
        pie = piece(s);
        
%        V_s = phi_s'* w;
%         Vmax=-1e10;
%         amax=-1;
%         for aind=0:Nactions-1
%             if absorb(s+aind)==1
%                 phinext_sa =zeros(Nfeatures,1);
%                 %fprintf('s+aind=%d is terminating\n',  s+aind);
%             else
%                 phinext_sa = Phinext(:,s+aind);
%             end
%             Vnext = reward(s+aind) + gamma * phinext_sa'*w;
%             if(Vnext>Vmax)
%                 Vmax = Vnext;
%                 amax=aind;
%             end
%         end
        
        [Vmax, amax]=max( reward(s:s+Nactions-1)+ gamma* ( Phinext(:,s:s+Nactions-1)'*w{4}  + Phinext(:,s:s+Nactions-1)'*w{5} )/2);
        b{pie} = b{pie} + phi_s*reward(s+ amax-1);
        A{pie} = A{pie} + phi_s*(gamma* Phinext(:,s+ amax-1)  - phi_s)';
%         w_new = -A\b;
%         w = w_new;
    end
    w_new{pie} = -A{pie}\b{pie};
%     err(iter) = norm(w{4}-w_new{4});
    w{pie} = w_new{pie};
end
err
w{4}
w{5}
% [maxw, ind_maxw] = max(w)
% [minw, ind_minw] = min(w)


% fweight=[dir 'w_CE_BI_run00.txt'];
% fid=fopen(fweight,'w');
% fprintf(fid, '%s', num2str(w'));
% fclose(fid);

% rapp=Phi'*w;
% figure;hold all;
% plot(reward,'-b.');
% plot(rapp,'-k+');

% figure;hold all;
% plot(w_CE, 'b.'); 
% plot(w, 'k+'); 


