%so far the learned w performs very poor.
%directions: 1. samples are not enough? 2. fitting Vs? first try to see the
%quality of Vs?
%evaluating board positions following the CE policy, i.e, trying to approximate V^{CE policy}
close all;
homedir = '/Users/admin/sztetris/';
dir =[homedir 'samplesCE_BI_run00_']%[homedir 'samplesRandom_BI_100episodes_'];

%using CE samples
% fPhiCE= [dir 'phi1.txt'];
% faCE = [dir 'action1.txt'];
% frewardCE = [dir 'reward1.txt'];
% fPhinextCE= [dir 'phinext1.txt'];
% fid = fopen(frewardCE);
% rewardCE= fscanf(fid, '%f');
% fclose(fid);
% size(rewardCE)
% min(rewardCE)
% max(rewardCE)
% 
% fid = fopen(fPhiCE);
% PhiCE=fscanf(fid, '%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f ', [21,inf]);
% fclose(fid);
% size(PhiCE)
% PhiCE = PhiCE';
% 
% fid = fopen(faCE);
% actionCE= fscanf(fid, '%d %d %d', [3,inf]);
% fclose(fid);
% size(actionCE)
% actionIndexCE= actionCE(1,:)+1;
% min(actionIndexCE)
% max(actionIndexCE)
% %assert(norm(actionIndexCE-policy)==0);
% 
% fid = fopen(fPhinextCE);
% PhinextCE=fscanf(fid, '%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f ', [21,inf]);
% fclose(fid);
% size(PhinextCE)
% PhinextCE = PhinextCE';


w_CE=[-3.4668 -8.5783 -15.6468 -6.1543 -9.8836 1.4784 -16.2427 -9.3328 -3.4838 -9.9378...
    -15.8604 0.1275 -23.8577 0.8403 -8.8838 2.0852 -32.9442 1.6128 -26.2846 2.8297 -57.4184]';

Nactions=40;
Nfeatures = length(w_CE);
% gamma =0.1%
% gamma=0.5
%  gamma=0.9
% gamma=0
gamma=1

% deltaA=-100
% deltaA=-1000
% deltaA=-100000
deltaA=0
% deltaA=-10
% deltaA=-0.1;

MIN=-1e10;

%method 2 (lstd0)
I=eye(Nfeatures, Nfeatures);
lambda=[0 0.4 1];
sum_columns= (sum(PhinextCE,2));
failures=find(sum_columns==0);
numEpisodes=length(failures);%number of games 
% numEpisodes=200;

fprintf('There are %d episodes/games in the samples\n', numEpisodes);
for ind_lam=1:length(lambda)
    lam = lambda(ind_lam)
    
    A{ind_lam} = zeros(Nfeatures, Nfeatures);
    b{ind_lam} = zeros(Nfeatures,1);
    for ind_epi=1:numEpisodes
            
        if(ind_epi==1)
            sbegin=1;
        else
            sbegin=send+1;
        end
        send=failures(ind_epi);
%         fprintf('The %d game starts from %d line to %d line (failure line)\n', ind_epi, sbegin, send);
        z=PhiCE(sbegin,:)';
        for s=sbegin:send
            phi_s = PhiCE(s,:)';

            phi_snext = PhinextCE(s,:)';
            r_s = rewardCE(s);
            
%             phi_snext = 0.5*PhinextCE(s,:)';
%             r_s = 0.5*rewardCE(s);
            
%                 r_s = 10*rewardCE(s)
%             if(r_s==-1) r_s=-10; end
%               if(r_s==1 || r_s==2) r_s=r_s*10; end

            A{ind_lam} = A{ind_lam} + z*(gamma*phi_snext-phi_s)';
            b{ind_lam} = b{ind_lam} + z*r_s;
            z = gamma*lam*z + phi_snext;
        end
    end
    wlstd{ind_lam}= -(A{ind_lam}+deltaA*I)\b{ind_lam};

    fweight=[homedir 'wlstd' num2str(lam)  '_CE_BI_run00.txt'];
    fid=fopen(fweight,'w');
    fprintf(fid, '%s', num2str(wlstd{ind_lam}'));
    fclose(fid);
end


%lstd0
b0=PhiCE'* rewardCE;
A0 = PhiCE' * (gamma*PhinextCE-PhiCE);
wlstd0=-(A0+deltaA*I)\b0;
fweight=[homedir 'wlstd0fullepisodes'  '_CE_BI_run00.txt'];
fid=fopen(fweight,'w');
fprintf(fid, '%s', num2str(wlstd0'));
fclose(fid);
% assert(norm(b0-b{1})==0)
% assert(norm(A0-A{1})==0)

figure;hold all;
plot(w_CE, '-b.'); 
plot(50*wlstd{1}, '-k+'); 
plot(50*wlstd{2}, '-gx'); 
plot(50*wlstd{length(wlstd)}, '-ro'); 
grid on
xlabel('Feature position');
ylabel('Weight');