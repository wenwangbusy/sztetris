package api;
import java.util.*;
import features.*;
import agent.*;
import Jama.*;
import tetrisengine.*;



/* This is a special LSPI algorithm that uses state features. 
 It takes advantage of a simulator in obtaining the transition following taking an action at a feature vector. 
 */
public class LSPI extends APIGeneric{
	
    public LSPI(FeatureExtractorGeneric f){
        super(f, false, false, new ArrayList<Transition>(), "LSPI");
    }
	public LSPI(FeatureExtractorGeneric f, List<Transition> tr){
		super(f, false, false, tr, "LSPI");
		System.out.println("In LSPI(), samples.size="+samples.size());
	}

	//greedification: get phinextIndex and absorb
	@Override
	public int greedyResult(int[][] board, int piece){
		int aGreedy=getGreedyAction(board,  piece);
        AfterState state = new AfterState(board);
		AfterState[] stateNextS = state.putTile(piece, aGreedy, true);
		phinextIndex = ((BinaryFeatureExtractor)(lstd.getFeatureExtractor())).getFeatureIndices(stateNextS[1].getBoard());
        reward=stateNextS[1].getReward();
        if(reward==-1.0){
            absorb = true;
            //System.out.println("state is absorbing in greedyResult.");
        }else{
            absorb = false;
        }
		return aGreedy;
	}
    
	public FeatureUserAgent api(List<Transition> samples){ return null;}
	
    int[] phinextIndex;
    
    @Override
    /*
	 greedify LSTD through greedification on the samples
	 */
	public void greedifyLSTD(boolean online){
        boolean debug=false;//true;
		if(online){
			samples =  lstd.gettransitsInOnlineSamples();
		}
        //do not collect samples in greedyResult()
        lstd.setSaveOnPolicySamples(false);
        lstd.setSaveAllActionSamples(false);
        
		System.out.println("samples size in greedifyLSTD(boolean online) is "+samples.size());
		assert(samples.size()>0);
		Transition tr;
		int[] phiIndex;
		double totRewards=0;
        //AfterState s = new AfterState();
		for(int ind=0; ind<samples.size(); ind++){
			//System.out.println("Sample ind=" + ind+", samples.size()="+samples.size());
            
			tr = samples.get(ind);
			phiIndex = ((BinaryFeatureExtractor)(lstd.getFeatureExtractor())).getFeatureIndices(tr.getBoard());
			int aGreedy=greedyResult(tr.getBoard(), tr.getPiece());
            
            assert(phinextIndex!=null);
			assert(reward!=-100);
			
			/*
			 AfterState state=new AfterState(tr.getBoard());
			 TetrisAction aGreedy=getGreedyAction(state,  tr.getPiece());
			 AfterState[] stateNextS = state.putTile(tr.getPiece(), aGreedy, true);
			 phinextIndex = ((BinaryFeatureExtractor)getFeatureExtractor()).getFeatureIndices(stateNextS[1].getBoard());
			 double reward = stateNextS[1].getReward();
			 */
			
			totRewards += reward;
            /*
             int Nshow;
             if(samples.size()>1e5)
             Nshow=10000;
             else
             Nshow=1000;
             if(ind%Nshow==0){
             System.out.println("Sample " + ind);
             System.out.println("Taking action " + aGreedy+": position="+TetrisAction.getPositionFromIndex(aGreedy)+"; rotation="+TetrisAction.getRotationFromIndex(aGreedy) );
             System.out.println("The reward is " + reward);
             }*/
            
			//copied from LSTDBinary::greedifyAb()---code should be reused rather than copied!!!
			for(int i=0; i<phiIndex.length; i++){
                lstd.setb(phiIndex[i], lstd.getb(phiIndex[i]) + reward);
				//A = A + phi*phi'
				for(int j=0; j<phiIndex.length; j++){
                    lstd.setA(phiIndex[i],phiIndex[j],  lstd.getA(phiIndex[i],phiIndex[j]) + 1 );
				}
				
                //A = A - gamma* phi*phinext'
                if(!absorb){
                    for(int j=0; j<phinextIndex.length; j++){
                        lstd.setA(phiIndex[i],phinextIndex[j],  lstd.getA(phiIndex[i],phinextIndex[j]) - lstd.getGamma() );
                    }
                }else{
                    if(debug && i==0){
                        System.out.print("Sample "+ ind+" is a terminal state, meaning that phinext=0; the nozero(1) index of phinextIndex is:");
                        for(int x: phinextIndex)
                            System.out.print(x);
                        System.out.println();
                        System.out.println("The board is ");
                        System.out.println(tr);
                        System.out.println("the action is " + aGreedy+": position="+TetrisAction.getPositionFromIndex(aGreedy)+"; rotation="+TetrisAction.getRotationFromIndex(aGreedy) );
                    }
                }
			}
            
            if(tr.getAbsorb()){
                System.out.println("seeing an absorbing state; sking the transtion from the absorbing state");
                ind++;
            }
            
		}
		//System.out.println("total Rewards in greedifyAb() is "+totRewards);
	}
    
    /* get phinextIndex and reward according to exploration with weights near the greedification
     */
    /*
    @Override
    public int greedyResultWexplore(AfterState state, int piece){
        int nExplorers=10;
        int deviation=1;
        lstd.setWeights(getBestExploreWeights(nExplorers, deviation));
        return greedyResult(state, piece);
    }*/
    
    
    
    /*
    public double[] getExploreWeights(double[] w, double sigma){
        double[] res = new double[w.length];
        Random rnd = new Random();
        for(int i=0; i<w.length; i++){
            res[i] = w[i] + 2*(rnd.nextDouble()-0.5) * sigma;
        }
        return res;
    }*/
        
	
	//@Override
	public int getGreedyAction(int[][] board, int piece){
        return lstd.observeAndAct(0.0,piece, board);
	}
    
    @Override
    public void learnModel(){;}
}
