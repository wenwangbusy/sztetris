package trainer;

import agent.*;
import features.*;
import tetrisengine.*;
import api.*;

import java.io.*;
import java.util.Arrays;
import java.util.Random;
import java.util.List;

import Jama.Matrix;


/* Apply API with an algorithmic agent (binary features) given input samples collected by another agent.
 This is an immutable class.
 */
public final class APIRunner{
    final FeatureUserAgent collectionAgent;
    final APIGeneric algorithm;
    final TetrisGame game;
    
    final double[][][] scores;//scores[run][delta][iter]
    final double[][][] errors;//iteration error [run][delta][iter]
    
    final int nRuns;
    final double[] deltaA;
    final int nIterations;
    
    public APIRunner(TetrisGame game, FeatureUserAgent agent, APIGeneric algorithm){
        this.game = game;
        this.algorithm=algorithm;
        nRuns = game.getRUNS();
        deltaA = algorithm.getDeltaA();
        nIterations = algorithm.getNumIterations();
        scores = new double[nRuns][deltaA.length][nIterations+1];
        errors = new double[nRuns][deltaA.length][nIterations];
        collectionAgent = agent;
    }
    
    public void runAPIOffline(){
        for(int run=0; run<nRuns;run++){
            System.out.println("\n@@@@@@@@@@@@ Run "+ run +"@@@@@@@@@@@@");
            collectSamples(0);
            algorithm.setSamples(collectionAgent.getSamples().getOnPolicyTrans());
            
            //algorithm.apiOfflineNoExploration();
            algorithm.apiOfflineExploration();
            
            algorithm.getAllDeltaErrors(errors[run]);
            algorithm.getAllDeltaScores(scores[run]);
            collectionAgent.clearSamples();
        }
        writeResults();
    }
    
    //recollect samples; on-policy sample collection should be split 
    public void collectSamples(double epsilon){
        collectionAgent.setSaveOnPolicySamples(true);
        collectionAgent.setSaveAllActionSamples(false);
        collectionAgent.setEpsilon(epsilon);
        System.out.println("\n\ncolllecting "+ game.getNumGames()+" episodes.");
        collectionAgent.evaluate(game.getNumGames(), false, true);
        //collectionAgent.evaluateWithRandomRestart(game.getNumGames(), false);
        //collectionAgent.evaluateCascadingRestart(game.getNumGames(), false);
        assert(collectionAgent.getSamples()!=null);
        System.out.println("After collectSamples, agent.getSamples().getOnPolicyTrans().size()="+collectionAgent.getSamples().getOnPolicyTrans().size());
    }
    
    public double[][][] getScores(){
        return scores;
    }
    public double[][][] getErrors(){
        return errors;
    }
    
    @Override
    public String toString(){
        String str="";
        str += "--Average scores for algorithm: \n";
		for(int run=0; run<nRuns; run++){
			str += "run "+ run + ":\n";
            for(int del=0; del<deltaA.length; del++){
                str += "----DeltaA= "+ deltaA[del] + "\n-----";
                for(int iter=0; iter<nIterations; iter++){
                    str += scores[run][del][iter] + ",";
                }
                str += "\n";
            }
			str += "\n";
		}
		return str;
    }
    
    /* Write the results to files */
	public void writeResults(){
        try{
            BufferedWriter fScores=null;
            BufferedWriter fErrors=null;
            String fileScores0 = "scores" + game.getNumGames() + "_algorithm" + "_";
            String fileErrors0 = "errors" + game.getNumGames() + "_algorithm" + "_";
            for(int del=0; del<deltaA.length; del++){
                String fileScores = fileScores0 + "deltaindex"+del + ".txt";
                String fileErrors = fileErrors0 + "deltaindex"+del + ".txt";
                fScores = new BufferedWriter(new FileWriter(fileScores, false));
                fErrors = new BufferedWriter(new FileWriter(fileErrors, false));
                for(int run=0; run<nRuns; run++){
                    for(int iter=0; iter<scores[run][del].length; iter++)
                        fScores.write(scores[run][del][iter] + " ");
                    fScores.write("\n");
                    for(int iter=0; iter<errors[run][del].length; iter++)
                        fErrors.write(errors[run][del][iter] + " ");
                    fErrors.write("\n");
                }
                fScores.close();
                fErrors.close();
            }
        }catch (IOException e) {
			e.printStackTrace();
		}
    }
}