package api;

import java.util.*;

import agent.*;
import features.*;
import tetrisengine.*;
//import explorator.*;

import Jama.*;

/* Offline LSPI that uses LSTD for policy evaluation
 @see API
 */
public abstract class OfflineAPI extends API{
    
    public OfflineAPI(String name, int numIterations,  UpdateLSTD lstdBehavior){
        super(name, PolicyEvaluationGeneric.getDeltaA(), numIterations, lstdBehavior);
    }
    
    @Override
    /*given an agent and samples, output the agent with an improved API accordingly
    by "offline", the samples are fixed
     @param agent the initial agent for API
     @samples the samples collected
    */
    public FeatureUserAgent api(List<Transition> samples){
            
        System.out.println("Starting "+ getName());
		System.out.println("In api(), number of sample transitions: "+samples.size());
        
        lstdBehavior.learnModel(samples);
        
        int nFeatures=lstdBehavior.getLSTD().getWeights().length;
        
        double[] w0 = new double[nFeatures];
        System.arraycopy(lstdBehavior.getLSTD().getWeights(),0, w0,0, w0.length);
        
        double[] w_old = new double[nFeatures];
		for(int d=0; d<getDeltaA().length; d++){
			double delta = deltaA[d];
			System.out.println("\n\n###### Delta = " + delta + "##############\n");
			
            //reset the policy
            lstdBehavior.getLSTD().setWeights(w0);
            
			for(int iter=0; iter<getNumIterations(); iter++){
				System.out.println("\n\n------- " +getName()+" Iteration " + (iter+1));
				final long startTime = System.currentTimeMillis();
				
				lstdBehavior.getLSTD().resetA();
                lstdBehavior.getLSTD().resetb();
                
                lstdBehavior.preUseModel();
                
                if(iter==0){
                    System.out.println("\n###Evaluate performance of the initial policy.");
                    scoresOfDeltaAIteration[d][iter] = lstdBehavior.getLSTD().evaluate(lstdBehavior.getLSTD().getNumTestGames(), false, true);
                }
                
                System.out.println("\n###Greeify: update LSTD's A, b according to greedification");
				
                lstdBehavior.greedify(samples);
                
                System.out.println("\n###Solve LSTD's weight vector according to A, b");
                System.arraycopy(lstdBehavior.getLSTD().getWeights(),0, w_old,0, nFeatures);
                lstdBehavior.getLSTD().solveWeights(delta);
                errWeights[d][iter] = calculateL1Error(w_old);
                
                System.out.println("\n###Evaluate performance of the induced LSTD policy");
                double scoreLast = scoresOfDeltaAIteration[d][iter];
                double score = lstdBehavior.getLSTD().evaluate(lstdBehavior.getLSTD().getNumTestGames(), false, true);
                if(score>scoreLast){
                    scoresOfDeltaAIteration[d][iter+1] =  score;
                }else{
                    System.out.println("Switching back to the policy of last iteration");
                    lstdBehavior.getLSTD().setWeights(w_old);
                    scoresOfDeltaAIteration[d][iter+1] =  scoreLast;
                }
                
                System.out.println(String.format("------Summary: Average score %f, iteration error: %f\n", scoresOfDeltaAIteration[d][iter+1], errWeights[d][iter]));
                System.out.println("Time spent: " + (System.currentTimeMillis()-startTime)/1000 + " seconds");
            }
        }
        return lstdBehavior.getLSTD();
    }
    
}
