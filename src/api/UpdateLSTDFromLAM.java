package api;

import java.util.*;

import agent.*;
import features.*;
import tetrisengine.*;
//import explorator.*;

import Jama.*;

/* Offline LAMAPI that uses LSTD for policy evaluation
 
 Use LAMAPI for action selection, and UpdateLSTD's greedifyLSTD() for constructing the policy's data structures (lstd)
 
 @see LAMAPI
 */
public final class UpdateLSTDFromLAM extends UpdateLSTD{
    
    public UpdateLSTDFromLAM(PolicyEvaluationGeneric lstd){
        super(lstd);
    }
    
    public int selectAction(int[][] board, int piece){
        LAMAPI lamapi = new LAMAPI(getLSTD());
        return lamapi.greedyActionWithLAM(board, piece);
    }
    
}
