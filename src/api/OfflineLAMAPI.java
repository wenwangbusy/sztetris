package api;

import java.util.*;

import agent.*;
import features.*;
import tetrisengine.*;
//import explorator.*;

import Jama.*;

/* Offline LSPI that uses LSTD for policy evaluation
 @see OfflineAPI
 */
public final class OfflineLAMAPI extends OfflineAPI{
    
    public OfflineLAMAPI(int numIterations, PolicyEvaluationGeneric lstd){
        super("Offline-LAMAPI", numIterations, new UpdateLSTDFromLAM(lstd));
    }
    
  }
