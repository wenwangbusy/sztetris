package trainer;

import agent.*;
import features.*;
import tetrisengine.*;
import api.*;

import java.io.*;
import java.util.Arrays;
import java.util.Random;
import java.util.List;

import Jama.Matrix;


/* Apply API with an algorithmic agent (binary features) given input samples collected by another agent.
 This is an immutable class.
 */
public final class APIRunner{
    final FeatureUserAgent collectionAgent;
    final API api;
    final TetrisGame game;
    
    final double[][][] scores;//scores[run][delta][iter]
    final double[][][] errors;//iteration error [run][delta][iter]
    
    final int nRuns;
    final double[] deltaA;
    final int nIterations;
    
    public APIRunner(TetrisGame game, FeatureUserAgent agent, API api){
        this.game = game;
        this.api = api;
        nRuns = game.getRUNS();
        deltaA = api.getDeltaA();
        nIterations = api.getNumIterations();
        scores = new double[nRuns][][];
        errors = new double[nRuns][][];
        collectionAgent = agent;
    }
    
    public void getResults(int run, double[][] sc, double[][] er){
        scores[run] = new double[sc.length][sc[0].length];
        errors[run] = new double[er.length][er[0].length];
        for(int i=0; i<sc.length; i++){
            System.arraycopy(sc[i],0,scores[run][i],0, sc[i].length);
            System.arraycopy(er[i],0,errors[run][i],0, er[i].length);
        }
    }

    public void runAPIOffline(){
        for(int run=0; run<nRuns;run++){
            System.out.println("\n@@@@@@@@@@@@ Run "+ run +"@@@@@@@@@@@@");
            collectSamples(0);
            
            api.api(collectionAgent.getSamples().getOnPolicyTrans());
            
            getResults(run,  api.getScores(),api.getIterationErrors());
            collectionAgent.clearSamples();
        }
        writeResults();
    }
    
    //recollect samples; on-policy sample collection should be split 
    public void collectSamples(double epsilon){
        collectionAgent.setSaveOnPolicySamples(true);
        collectionAgent.setSaveAllActionSamples(false);
        collectionAgent.setEpsilon(epsilon);
        System.out.println("\n\ncolllecting "+ game.getNumGames()+" episodes.");
        collectionAgent.evaluate(game.getNumGames(), false, true);
        //collectionAgent.evaluateWithRandomRestart(game.getNumGames(), false);
        //collectionAgent.evaluateCascadingRestart(game.getNumGames(), false);
        assert(collectionAgent.getSamples()!=null);
        System.out.println("After collectSamples, agent.getSamples().getOnPolicyTrans().size()="+collectionAgent.getSamples().getOnPolicyTrans().size());
    }
    
    public double[][][] getScores(){
        return scores;
    }
    public double[][][] getErrors(){
        return errors;
    }
    
    @Override
    public String toString(){
        String str="";
        str += "--Average scores for algorithm: \n";
		for(int run=0; run<nRuns; run++){
			str += "run "+ run + ":\n";
            for(int del=0; del<deltaA.length; del++){
                str += "----DeltaA= "+ deltaA[del] + "\n-----";
                for(int iter=0; iter<nIterations; iter++){
                    str += scores[run][del][iter] + ",";
                }
                str += "\n";
            }
			str += "\n";
		}
		return str;
    }
    
    /* Write the results to files */
	public void writeResults(){
        try{
            BufferedWriter fScores=null;
            BufferedWriter fErrors=null;
            String fileScores0 = "scores" + game.getNumGames() + api.getName() + "_";
            String fileErrors0 = "errors" + game.getNumGames() + api.getName()+ "_";
            for(int del=0; del<deltaA.length; del++){
                String fileScores = fileScores0 + "deltaindex"+del + ".txt";
                String fileErrors = fileErrors0 + "deltaindex"+del + ".txt";
                fScores = new BufferedWriter(new FileWriter(fileScores, false));
                fErrors = new BufferedWriter(new FileWriter(fileErrors, false));
                for(int run=0; run<nRuns; run++){
                    for(int iter=0; iter<scores[run][del].length; iter++)
                        fScores.write(scores[run][del][iter] + " ");
                    fScores.write("\n");
                    for(int iter=0; iter<errors[run][del].length; iter++)
                        fErrors.write(errors[run][del][iter] + " ");
                    fErrors.write("\n");
                }
                fScores.close();
                fErrors.close();
            }
        }catch (IOException e) {
			e.printStackTrace();
		}
    }
}