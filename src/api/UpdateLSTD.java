package api;

import java.util.List;
import agent.*;
import tetrisengine.*;
import features.*;
//import explorator.*;


/* Different methods of update LSTD
 */
public abstract class UpdateLSTD extends UpdateLSTDBasic implements UpdateLSTDInterface{
    
    public UpdateLSTD(PolicyEvaluationGeneric lstd){
        super(lstd);
    }
    
    public class GreedyNext extends GreedyNextBasic{
        int[] phinextIndex;
        public GreedyNext(int aGreedy, int[] phinextIndex, double reward, boolean absorb){
            super(aGreedy, reward, absorb);
            this.phinextIndex=phinextIndex;
        }
        public int[] getPhinext(){return phinextIndex;}
    }

    //update lstd using the samples
    public GreedyNext greedyResult(int[][] board, int piece){
        int aGreedy = selectAction(board,  piece);
        AfterState state = new AfterState(board);
		AfterState[] stateNextS = state.putTile(piece, aGreedy, true);
		int[] phinextIndex = ((BinaryFeatureExtractor)(lstd.getFeatureExtractor())).getFeatureIndices(stateNextS[1].getBoard());
        double reward = stateNextS[1].getReward();
        boolean absorb;
        if(reward == -1.0){
            absorb = true;
            if(debug)
                System.out.println("state is absorbing in greedyResult.");
        }else{
            absorb = false;
        }
		return new GreedyNext(aGreedy, phinextIndex, reward, absorb);
    }
    
    public void greedify(List<Transition> samples){
        for(int ind=0; ind<samples.size(); ind++){
            Transition tr = samples.get(ind);
			int[] phiIndex = ((BinaryFeatureExtractor)(lstd.getFeatureExtractor())).getFeatureIndices(tr.getBoard());
			GreedyNext gnext = greedyResult(tr.getBoard(), tr.getPiece());
            int aGreedy = gnext.getAction();
            int[] phinextIndex = gnext.getPhinext();
            double reward = gnext.getReward();
            boolean absorb = gnext.getAbsorb();
            
            
            //copied from LSTDBinary::greedifyAb()---code should be reused rather than copied!!!
			for(int i=0; i<phiIndex.length; i++){
                lstd.setb(phiIndex[i], lstd.getb(phiIndex[i]) + reward);
				//A = A + phi*phi'
				for(int j=0; j<phiIndex.length; j++){
                    lstd.setA(phiIndex[i],phiIndex[j],  lstd.getA(phiIndex[i],phiIndex[j]) + 1 );
				}
				
                //A = A - gamma* phi*phinext'
                if(!absorb){
                    for(int j=0; j<phinextIndex.length; j++){
                        lstd.setA(phiIndex[i],phinextIndex[j],  lstd.getA(phiIndex[i],phinextIndex[j]) - lstd.getGamma() );
                    }
                }else{
                    if(debug && i==0){
                        System.out.print("Sample "+ ind+" is a terminal state, meaning that phinext=0; the nozero(1) index of phinextIndex is:");
                        for(int x: phinextIndex)
                            System.out.print(x +" ");
                        System.out.println();
                        System.out.println("The board is ");
                        System.out.println(tr);
                        System.out.println("the greedy action is " + aGreedy+": position="+TetrisAction.getPositionFromIndex(aGreedy)+"; rotation="+TetrisAction.getRotationFromIndex(aGreedy) );
                    }
                }
			}
            
        }
    }
    
}