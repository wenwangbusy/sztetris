package api;
import java.util.*;

import agent.*;


public interface APIInterface{
    
    //given an lstd and samples, output the agent with an improved API accordingly
    public FeatureUserAgent api(List<Transition> samples);
    
}