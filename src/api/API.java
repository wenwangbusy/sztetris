package api;

import agent.*;

import java.util.*;

import Jama.Matrix;

/* A general wrapper of api algorithms
 @see APIInterface
 */
public abstract class API implements APIInterface{

    final boolean debug = false;

    final UpdateLSTD lstdBehavior;
    
    final String name;
    final double[] deltaA;
    final int numIterations;

    final double[][] errWeights;//error of API
    final double[][] scoresOfDeltaAIteration;//scores of API

    public API(String name, double[] deltaA, int numIterations, UpdateLSTD lstdBehavior){
        this.name=name;
        this.lstdBehavior=lstdBehavior;
        this.deltaA=deltaA;
        this.numIterations=numIterations;
        errWeights = new double[deltaA.length][numIterations];
        scoresOfDeltaAIteration = new double[deltaA.length][numIterations+1];//count the inital policy performance
    }

    public String getName(){return name;}
    public double[] getDeltaA(){return deltaA;}
    public int getNumIterations(){return numIterations;}
    public double[][] getIterationErrors(){return errWeights;}
    public double[][] getScores(){return scoresOfDeltaAIteration;}
    public UpdateLSTD getUpdateLSTD(){return lstdBehavior;}
    
    //learn a model using the samples (a fake name for model-free)
    public void learnModel(List<Transition> samples){;}
    
    //preprocessing with the model (a fake name for model-free)
    public void preUseModel(){;}
    
    public double calculateL1Error(double[] w){
        assert(w.length==lstdBehavior.getLSTD().getWeights().length && w.length>0);
        double iterErr=0;
        for(int i=0; i<w.length; i++){
            iterErr += Math.abs(lstdBehavior.getLSTD().getWeight(i) - w[i]);
        }
        return iterErr;
    }
    
    public String toString(Matrix A){
        String res="";
        for(int i=0; i<A.getRowDimension(); i++){
            for(int j=0;j<A.getColumnDimension();j++){
                res += A.get(i,j)+", ";
            }
            res += "\n";
        }
        return res;
    }
}