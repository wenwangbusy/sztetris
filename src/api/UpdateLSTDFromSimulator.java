package api;

import agent.*;
//import explorator.*;

//update LSTD from simulated experience (real transitions given actions according to the simulator)
public class UpdateLSTDFromSimulator extends UpdateLSTD{
    
    public UpdateLSTDFromSimulator(PolicyEvaluationGeneric lstd){
        super(lstd);
    }
    
    //use the simulator to get the next best action according the policy in lstd
    public int selectAction(int[][] board, int piece){
        return getLSTD().observeAndAct(0.0, piece, board);
    }

}