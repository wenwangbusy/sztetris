package api;

import java.util.*;

import agent.*;
import features.*;
import tetrisengine.*;
//import explorator.*;

import Jama.*;

/* Offline LSPI that uses LSTD for policy evaluation
 @see OfflineAPI
 */
public final class OfflineLSPI extends OfflineAPI{
    
    public OfflineLSPI(int numIterations, PolicyEvaluationGeneric lstd){
        super("Offline-LSPI", numIterations, new UpdateLSTDFromSimulator(lstd));
    }
    
  }
