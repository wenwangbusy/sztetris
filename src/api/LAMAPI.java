package api;

import java.util.*;

import agent.*;
import features.*;
import tetrisengine.*;
//import explorator.*;

import Jama.*;

/* Offline LAMAPI that uses LSTD for policy evaluation
 @see OfflineAPI
 */
public class LAMAPI extends UpdateLSTDBasic{
    
    /* action models */
    final static int Nactions = TetrisGame.getNactions();
	final Matrix[] F = new Matrix[Nactions];
	final Matrix[] f = new Matrix[Nactions];

    /* FTw[a] = F[a]^{T} w; */
    final double[][] FTw = new double[Nactions][];
    
    //regularization factor for learning the model
    final static double deltaD=1000;//1500;//500;//2000;//3000;
        
    public LAMAPI(PolicyEvaluationGeneric lstd){
        super(lstd);
    }
    
    @Override
    //learn the action model
	public void learnModel(List<Transition> samples){
        
        int d=getLSTD().getFeatureExtractor().getNumFeatures();
        
        System.out.println("In learnModel(); deltaD="+deltaD);
		final long startTime = System.currentTimeMillis();
		
		Transition tmp = new Transition();
		int[] phiIndex, phinextIndex;
		
		Matrix[] e=new Matrix[Nactions];
		Matrix[] D=new Matrix[Nactions];
		Matrix[] E=new Matrix[Nactions];
		for(int a=0; a<e.length; a++){
			e[a] = new Matrix(d,1);
			D[a] = Matrix.identity(d,d);
			D[a].timesEquals(deltaD);
			E[a] = new Matrix(d,d);
		}
		
		for(int ind=0; ind<samples.size(); ind++){
			tmp = samples.get(ind);
			int a = tmp.getAction();
			double reward =  tmp.getReward();
			phiIndex = ((BinaryFeatureExtractor)(getLSTD().getFeatureExtractor())).getFeatureIndices(tmp.getBoard());
			
			//important: terminatal feature vector is 0
			if(tmp.getAbsorb()){
				phinextIndex=null;
            }else
				phinextIndex = ((BinaryFeatureExtractor)(getLSTD().getFeatureExtractor())).getFeatureIndices(samples.get(ind+1).getBoard());
			
			//e[a] = e[a] + phi*r; D[a]=D[a]+phi*phi'; E[a] = E[a] + phi*phinext
			for(int i=0; i<phiIndex.length; i++){
				e[a].set(phiIndex[i],0,  e[a].get(phiIndex[i],0) + reward);
				for(int j=0; j<phiIndex.length; j++){
					D[a].set(phiIndex[i],phiIndex[j],  D[a].get(phiIndex[i],phiIndex[j]) + 1 );
				}
				if(phinextIndex!=null){
					for(int j=0; j<phinextIndex.length; j++){
						E[a].set(phiIndex[i],phinextIndex[j],    E[a].get(phiIndex[i],phinextIndex[j]) + 1 );
					}
				}
			}
		}
        
//        for(int a=0; a<Nactions; a++){
//            System.out.println(toString(e[a]));
//        }

		for(int a=0; a<Nactions; a++){
			f[a] = D[a].solve(e[a]);
			F[a] = (D[a].solve(E[a])).transpose();
		}
		
		System.out.println("Time spent: " + (System.currentTimeMillis()-startTime)/1000 + " seconds");
        System.out.println("Model is \n"+this);
	}
    
    @Override
    //we use this function to precompute FTw before looping over all the samples
    public void preUseModel(){
//        System.out.println("In lamapi:preUseModel()");
        Matrix w = new Matrix(getLSTD().getWeights(), getLSTD().getWeights().length);
        for(int a=0; a<Nactions; a++){
            FTw[a] = ( (F[a].transpose()).times(w) ).getRowPackedCopy();;
        }
    }

    //select the best action according to LAM
    public int greedyActionWithLAM(int[][] board, int piece){
        //return getLSTD().observeAndAct(0.0, piece, board);
        
//        System.out.println("In lamapi:getGreedyAction()");
        //final long startTime = System.currentTimeMillis();
		int[] phiIndex = ((BinaryBITabular)(getLSTD().getFeatureExtractor())).getFeatureIndices(board);
		double valMax = TetrisGame.getINF();
		int aStar=-1;
//        double reward;
		for(int a=0; a<Nactions; a++){
			double val = projectReward(phiIndex, a) +  getLSTD().getGamma() * projectNextValue(phiIndex, a);
			if(val>valMax){
				valMax=val;
				aStar = a;
//				reward = faTphi;
				//phinext = getPhinextProjection(phiIndex, aStar);
			}
		}
        //phinext = getPhinextProjection(phiIndex, aStar);
        
//        System.out.println("Time spent in getGreedyAction: " + (System.currentTimeMillis()-startTime) + " miliseconds");
		//System.out.println("aStart="+aStar+",valMax="+valMax);
		assert(0<=aStar && aStar<Nactions);
		return aStar;
        
    }
    
    /* 
     @param phiIndex sparse binary feature vector
     @param a an action
     @return f{a}^{T}*phi
     */
    public double projectReward(int[] phiIndex, int a){
        double faTphi=0;
        for(int i=0; i<phiIndex.length; i++){
            faTphi += f[a].get(phiIndex[i],0);
        }
        return faTphi;
    }
    
    /*
     @return (F{a}*phi)^{T}*w
     */
    public double projectNextValue(int[] phiIndex, int a){
        double Vaphi=0;
        for(int i=0; i<phiIndex.length; i++){
            Vaphi  += FTw[a][phiIndex[i]];
        }
        return Vaphi;
    }
    
    @Override
    public String toString(){
        String res="";
        for(int a=0; a<Nactions; a++){
            for(int i=0;i<f[a].getRowDimension(); i++){
                res += f[a].get(i,0)+", ";
            }
            res += "\n";
        }
        return res;
    }
    
}
