package api;

import java.util.List;
import agent.*;
import tetrisengine.*;
import features.*;
//import explorator.*;


/* Different methods of update LSTD
 */
public class UpdateLSTDBasic{
    
    final boolean debug = false;
    
    final PolicyEvaluationGeneric lstd;
    
    public UpdateLSTDBasic(PolicyEvaluationGeneric lstd){
        this.lstd=lstd;
    }
    
    public PolicyEvaluationGeneric getLSTD(){return lstd;}
    
    public class GreedyNextBasic{
        int aGreedy;
        double reward;
        boolean absorb;
        public GreedyNextBasic(int aGreedy, double reward, boolean absorb){
            this.aGreedy=aGreedy;
            this.reward=reward;
            this.absorb=absorb;
        }
        public int getAction(){return aGreedy;}
        public double getReward(){return reward;}
        public boolean getAbsorb(){return absorb;}
    }

    
    public void learnModel(List<Transition> samples){;}
    
    public void preUseModel(){;}
}