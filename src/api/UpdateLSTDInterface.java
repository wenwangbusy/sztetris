package api;

import agent.*;
import java.util.List;


public interface UpdateLSTDInterface{
    
    //perform a greedification operation on the samples
    public void greedify(List<Transition> samples);

    //given the current policy in lstd, and current board and piece, select an action
    public int selectAction(int[][] board, int piece);
}