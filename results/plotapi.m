%plot api results
% close all;
% % deltas=[100,300, 500, 600, 700, 900, 1000];
% deltas=[100,300, 600, 1000];
% for d=1:length(deltas)
%    plotapi(deltas(d));  
% end

function scores=plotapi(delta)
%delta is the reguarizer for lamapi

%file4 = ['/Users/hengy/sztetris/results/scores_delta' num2str(delta) '_allruns.txt'];
file4='/Users/hengy/sztetris/results/scores100_lspi_deltaindex0.txt.allruns';
%fid4 = fopen(file4, 'r');
%scores=fread(fid4,[17,100]);
%fclose(fid4);
scores=dlmread(file4);
m4 = mean(scores, 1);
plot(m4); hold all;

score_ave = mean(scores,1); 
score_std = std(scores,0,1);
score_ebs = 1.96 * score_std ./ sqrt(size(score_std,1));
errorbar([1:size(scores,2)],score_ave,score_ebs);  
  
% legend('delta=100','delta=300','delta=600','delta=1000');
% legend('delta=100','delta=300','delta=500','delta=600','delta=700','delta=900','delta=1000');
xlabel('Iteration');
ylabel('Scores');

