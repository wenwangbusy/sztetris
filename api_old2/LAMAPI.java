package api;
import java.util.*;
import features.*;
import agent.*;
import tetrisengine.*;
import Jama.*;


/* This is a LAMAPI algorithm; 
 right now implemented with Jama, which is very slow
 note: the piece is not considered (should be)
 
 @see <a href="http://webdocs.cs.ualberta.ca/~hengshua/papers/lamapi.pdf">http://webdocs.cs.ualberta.ca/~hengshua/papers/lamapi.pdf</a> 
 
 Note this doesn't assume an access to the simulator after samples are collected.
 */
public class LAMAPI extends APIGeneric{
	
    final boolean debug=false;
    
    final static double deltaD=2000;//1000;//1500;//500;//2000;//3000;
    
    final int d=lstd.getFeatureExtractor().getNumFeatures();
	final static int Nactions = TetrisGame.getNactions();
    
    /* action models */
	final Matrix[] F = new Matrix[Nactions];
	final Matrix[] f = new Matrix[Nactions];

    
    public LAMAPI(FeatureExtractorGeneric f){
        super(f, false, false, new ArrayList<Transition>(), "LAMAPI");
        
    }
	public LAMAPI(FeatureExtractorGeneric f, List<Transition> tr){
		super(f, false, false, tr, "LAMAPI");
		System.out.println("In LAMAPI(), samples.size="+samples.size());
        
	}
    
    {
		for(int a=0; a<Nactions; a++){
			f[a] = new Matrix(d,1);
			F[a] = new Matrix(d,d);
		}
	}

	
    @Override
	//learn the action model
	public void learnModel(){
        System.out.println("In learnModel(); deltaD="+deltaD);
		final long startTime = System.currentTimeMillis();
		
		Transition tmp = new Transition();
		TransitionPhiBinary tmpB =  new TransitionPhiBinary();
		int[] phiIndex, phinextIndex;
		
		Matrix[] e=new Matrix[Nactions];
		Matrix[] D=new Matrix[Nactions];
		Matrix[] E=new Matrix[Nactions];
		for(int a=0; a<e.length; a++){
			e[a] = new Matrix(d,1);
			D[a] = Matrix.identity(d,d);
			D[a].timesEquals(deltaD);
			E[a] = new Matrix(d,d);
		}
		
		for(int ind=0; ind<samples.size(); ind++){
			tmp = samples.get(ind);
			int a = tmp.getAction();
			double reward =  tmp.getReward();
			phiIndex = ((BinaryFeatureExtractor)(lstd.getFeatureExtractor())).getFeatureIndices(tmp.getBoard());
			
			//important: terminatal feature vector is 0
			if(tmp.getAbsorb()){
				phinextIndex=null;
            }else
				phinextIndex = ((BinaryFeatureExtractor)(lstd.getFeatureExtractor())).getFeatureIndices(samples.get(ind+1).getBoard());
			
			//e[a] = e[a] + phi*r; D[a]=D[a]+phi*phi'; E[a] = E[a] + phi*phinext
			for(int i=0; i<phiIndex.length; i++){
				e[a].set(phiIndex[i],0,  e[a].get(phiIndex[i],0) + reward);
				for(int j=0; j<phiIndex.length; j++){
					D[a].set(phiIndex[i],phiIndex[j],  D[a].get(phiIndex[i],phiIndex[j]) + 1 );
				}
				if(phinextIndex!=null){
					for(int j=0; j<phinextIndex.length; j++){
						E[a].set(phiIndex[i],phinextIndex[j],    E[a].get(phiIndex[i],phinextIndex[j]) + 1 );
					}
				}
			}
            
            if(tmp.getAbsorb()){
                ind++;
                if(debug)
                    System.out.println("in modeling: seeing an absorbing state; skiping the transtion from the absorbing state");
            }
		}
//        for(int a=0; a<Nactions; a++){
//            System.out.println(toString(e[a]));
//        }
        
		for(int a=0; a<Nactions; a++){
			f[a] = D[a].solve(e[a]);
			F[a] = (D[a].solve(E[a])).transpose();
		}
		
		System.out.println("Time spent: " + (System.currentTimeMillis()-startTime)/1000 + " seconds");
        System.out.println("Model is \n"+this);
	}

    @Override
    public int greedyResult(int[][] board, int piece){return -1;}
    
	//@Override
	public int greedyResult(double[][] FTw, int[][] board, int piece){
		int aGreedy=getGreedyAction(FTw, board,  piece);
		return aGreedy;
	}
	
    //compute the vector FTw[a] = F[a]^{T} w;
    //we use this function to precompute FTw out of looping over all the samples
    double[][] getFTw(){
        double[][] FTw=new double[Nactions][];
        final Matrix w = new Matrix(lstd.getWeights(), lstd.getWeights().length);
        for(int a=0; a<Nactions; a++){
            FTw[a] = ( (F[a].transpose()).times(w) ).getRowPackedCopy();;
        }
        return FTw;
    }
    
	//get the greedy action; also update phinextIndex and reward
    double [] phinext;
    
    @Override
    public int getGreedyAction(int[][] board, int piece){return -1;}
    
    /*update phinext and reward; very slow if FTw is computed for every board/sample
     improved by precomputing FTw because it is sample independent
     */
	//@Override
	public int getGreedyAction(double[][] FTw, int[][] board, int piece){
        //final long startTime = System.currentTimeMillis();
		int[] phiIndex = ((BinaryBITabular)(lstd.getFeatureExtractor())).getFeatureIndices(board);
		final double valMax = TetrisGame.getINF();
		//double[] FTw;
		int aStar=-1;
		//Matrix w = new Matrix(lstd.getWeights(), lstd.getWeights().length);
		for(int a=0; a<Nactions; a++){
			//FTw = ( (F[a].transpose()).times(w) ).getRowPackedCopy();
			double faTphi=0;//f{a}^{T}*phi
            double Vaphi=0;//(F{a}*phi)^{T}*w
			for(int i=0; i<phiIndex.length; i++){
				Vaphi  += FTw[a][phiIndex[i]];
				faTphi += f[a].get(phiIndex[i],0);
			}
			double val = faTphi + lstd.getGamma()*Vaphi;
			if(val>valMax){
				valMax=val;
				aStar = a;
				reward = faTphi;
				//phinext = getPhinextProjection(phiIndex, aStar);
			}
		}
        phinext = getPhinextProjection(phiIndex, aStar);
        
        //System.out.println("Time spent in getGreedyAction: " + (System.currentTimeMillis()-startTime) + " miliseconds");
		//System.out.println("valMax="+valMax);
		assert(0<=aStar && aStar<Nactions);
		return aStar;
	}
	
	/* get the projected phinext according to F
	 */
	public double[] getPhinextProjection(int[] phiIndex, int a){
		double[] phi=((BinaryBITabular)(lstd.getFeatureExtractor())).getFeatures(phiIndex);
		Matrix phiM = new Matrix(phi, phi.length);//a column vector
		Matrix res = F[a].times(phiM);//speed up?
		return res.getColumnPackedCopy();//same as getRowPackedCopy (for vector matrices)
	}
    
    @Override
    /*
	 greedify LSTD through greedification on the samples
	 */
	public void greedifyLSTD(boolean online){
		if(online){
			samples =  lstd.gettransitsInOnlineSamples();
		}
        //do not collect samples in greedyResult()
        lstd.setSaveOnPolicySamples(false);
        lstd.setSaveAllActionSamples(false);
        
		System.out.println("samples size in greedifyLSTD(boolean online) is "+samples.size());
		assert(samples.size()>0);
		Transition tr;
		int[] phiIndex;
		double totRewards=0;
        //AfterState s = new AfterState();
        
        double[][]FTw = getFTw();
		for(int ind=0; ind<samples.size(); ind++){
			if(debug)
                System.out.println("Sample ind=" + ind+", samples.size()="+samples.size());
            final long startTime0 = System.currentTimeMillis();
            
			tr = samples.get(ind);
            
			phiIndex = ((BinaryFeatureExtractor)(lstd.getFeatureExtractor())).getFeatureIndices(tr.getBoard());
			int aGreedy=greedyResult(FTw, tr.getBoard(), tr.getPiece());
            
            assert(phinext!=null);
			assert(reward!=-100);
			
//            if(tr.getAbsorb()){
//                //System.out.println("resetting the terminal feature to 0");
//                for(double tmp:phinext){
//                    tmp=0;
//                }
//            }
            
            
			/*
			 AfterState state=new AfterState(tr.getBoard());
			 TetrisAction aGreedy=getGreedyAction(state,  tr.getPiece());
			 AfterState[] stateNextS = state.putTile(tr.getPiece(), aGreedy, true);
			 phinextIndex = ((BinaryFeatureExtractor)getFeatureExtractor()).getFeatureIndices(stateNextS[1].getBoard());
			 double reward = stateNextS[1].getReward();
			 */
			if(debug){
                System.out.println("Online reward = "+ tr.getReward()+"; approximate reward ="+reward + " of api action");
            }
            
            
			totRewards += reward;
            /*
             int Nshow;
             if(samples.size()>1e5)
             Nshow=10000;
             else
             Nshow=1000;
             if(ind%Nshow==0){
             System.out.println("Sample " + ind);
             System.out.println("Taking action " + aGreedy+": position="+TetrisAction.getPositionFromIndex(aGreedy)+"; rotation="+TetrisAction.getRotationFromIndex(aGreedy) );
             System.out.println("The reward is " + reward);
             }*/
            
            final long startTime = System.currentTimeMillis();
			//copied from LSTDBinary::greedifyAb()---code should be reused rather than copied!!!
			for(int i=0; i<phiIndex.length; i++){
                lstd.setb(phiIndex[i], lstd.getb(phiIndex[i]) + reward);
				//A = A + phi*phi'
				for(int j=0; j<phiIndex.length; j++){
                    lstd.setA(phiIndex[i],phiIndex[j],  lstd.getA(phiIndex[i],phiIndex[j]) + 1 );
				}
                //A = A - gamma * phi* phinext'
                for(int jn=0; jn<phinext.length; jn++){
                    if(phinext[jn]!=0){
                        lstd.setA(phiIndex[i],jn,   lstd.getA(phiIndex[i],jn) - lstd.getGamma()* phinext[jn] );
                    }
                }
			}
            
            if(tr.getAbsorb()){
                if(debug)
                    System.out.println("seeing an absorbing state; skiping the transtion from the absorbing state");
                ind++;
            }
            
            //System.out.println("Time spent for updating A,b: " + (System.currentTimeMillis()-startTime) + " miliseconds");
            //System.out.println("Time spent per sample in total: " + (System.currentTimeMillis()-startTime0) + " miliseconds");
        }
		//System.out.println("total Rewards in greedifyAb() is "+totRewards);
	}
		
    @Override
    public String toString(){
        String res="";
        for(int a=0; a<Nactions; a++){
            for(int i=0;i<d; i++){
                res += f[a].get(i,0)+", ";
            }
            res += "\n";
        }
        return res;
    }
    
    public String toString(Matrix A){
        String res="";
        for(int i=0; i<A.getRowDimension(); i++){
            for(int j=0;j<A.getColumnDimension();j++){
                res += A.get(i,j)+", ";
            }
            res += "\n";
        }
        return res;
    }
    
}
