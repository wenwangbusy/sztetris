package api;

import java.util.*;

import agent.*;
import features.*;
import tetrisengine.*;
import explorator.*;

import Jama.*;

/* A general approximate policy iteration algorithm class that uses LSTD0 for policy evaluation
 @see PolicyEvaluationGeneric
 */
public abstract class APIGeneric implements APIInterface{
    
    LSTDBinary lstd;
    List<Transition> samples;
    
    final String name;
    
    static final int numIterations=15;
    
    final double[][] errWeights;//error of API
	final double[][] scoresOfDeltaAIteration;//scores of API
	//final int[][] totalSamples = new int[getDeltaA().length][numIterations+1];
    
    final double[] deltaA;
    final int nFeatures;
    
    //this classs is mutable because of updating this field.
	double reward=-100;
    boolean absorb=false;
    
    boolean initialPolicyProvided=false;
	double[] w0;
    
    final boolean debug = false;//true;
	
	public APIGeneric(FeatureExtractorGeneric f, boolean saveOnPolicySamples, boolean saveAllActionSamples, String name){
	    this.name=name;
        lstd =new LSTDBinary(f, saveOnPolicySamples, saveAllActionSamples);
        deltaA = lstd.getDeltaA();
        nFeatures=lstd.getWeights().length;
        scoresOfDeltaAIteration = new double[deltaA.length][numIterations+1];
        errWeights= new double[deltaA.length][numIterations];
        samples = new ArrayList<Transition>();
	}
	public APIGeneric(FeatureExtractorGeneric f, boolean saveOnPolicySamples, boolean saveAllActionSamples, List<Transition> tr, String name){
		this(f, saveOnPolicySamples, saveAllActionSamples, name);
        samples=tr;
	}
    
    public String getName(){return name;}
    
    public void setSamples(List<Transition> tr){
        samples=tr;
    }
    
	public void setInitialPolicy(double[] w_given){
		initialPolicyProvided=true;
        assert(w_given.length==nFeatures);
        w0=new double[nFeatures];
		System.arraycopy(w_given,0, w0, 0, w_given.length);
	}

    public double[] getDeltaA(){
        return lstd.getDeltaA();
    }

	/* Offline approximate policy iteration 
	 * given the samples saved in file fileName, 
	 * at each feature vector, select an action for the feature vector, and get a reward and the next feature vector
	 * update policy evaluation structures accordingly using this transition experience
	 * solve the weight vector
	 */
	//public abstract void api(String[] samplefiles, boolean addOnSamples);

    
    public void initializewLSTD(){
        if(!initialPolicyProvided){
            lstd.resetWeights();//lstd.randomizeWeights();
            System.out.println("Starting with a zero weight policy");
            //System.out.println("Starting with a random policy (w=rand)");
        }else{
            System.out.println("Initializing to the initial policy");
            lstd.setWeights(w0);//printWeights();
        }
    }
    
    public double calculateL1fromLSTD(double[] w){
        assert(w.length==nFeatures && nFeatures>0);
        double iterErr=0;
        for(int i=0; i<nFeatures; i++){
            iterErr += Math.abs(lstd.getWeight(i) - w[i]);
        }
        return iterErr;
    }
    
    public void resetAbOfLSTD(){
        lstd.resetA();
        lstd.resetb();
    }
    
    /*offline api, sample set precollected and held fixed; no exploration is applied */
    public void apiOfflineNoExploration(){
        System.out.println("Starting off-line API ");
		System.out.println("In apiOfflineNoExploration(), samples.size="+samples.size());
        
        learnModel();
        
        lstd.setSaveOnPolicySamples(false);
        lstd.setSaveAllActionSamples(false);
        lstd.setEpsilon(0);
        
        double[] w_old = new double[nFeatures];
		for(int d=0; d<deltaA.length; d++){
			double delta = deltaA[d];
			System.out.println("\n\n###### Delta = " + delta + "##############\n");
			
            initializewLSTD();
            
			for(int iter=0; iter<getNumIterations(); iter++){
				System.out.println("\n\n------- " +getName()+" Iteration " + (iter+1));
				final long startTime = System.currentTimeMillis();
				
				resetAbOfLSTD();
                
                if(iter==0){
                    System.out.println("\n###Evaluate performance of the initial policy.");
                    scoresOfDeltaAIteration[d][iter] = lstd.evaluate(lstd.getNumTestGames(), false, true);
                }
                
                System.out.println("\n###Greeify: update LSTD's A, b according to greedification");
				greedifyLSTD(false);

                System.out.println("\n###Solve LSTD's weight vector according to A, b");
                System.arraycopy(lstd.getWeights(),0, w_old,0, nFeatures);
                lstd.solveWeights(delta);
                errWeights[d][iter] = calculateL1fromLSTD(w_old);
                
                System.out.println("\n###Evaluate performance of the induced LSTD policy");
                double scoreLast = scoresOfDeltaAIteration[d][iter];
                double score=lstd.evaluate(lstd.getNumTestGames(), false, true);
                if(score>scoreLast){
                    scoresOfDeltaAIteration[d][iter+1] =  score;
                }else{
                    System.out.println("Switching back to the policy of last iteration");
                    lstd.setWeights(w_old);
                    scoresOfDeltaAIteration[d][iter+1] =  scoreLast;
                }
                
                System.out.println(String.format("------Summary: Average score %f, iteration error: %f\n", scoresOfDeltaAIteration[d][iter+1], errWeights[d][iter]));
                System.out.println("Time spent: " + (System.currentTimeMillis()-startTime)/1000 + " seconds");
            }       
        }
    }
    
	
	public static int getNumIterations(){return numIterations;}
	
    public void getAllDeltaScores(double[][] result){
        assert(result!=null);
        for(int i=0; i<scoresOfDeltaAIteration.length; i++){
            assert(scoresOfDeltaAIteration[i].length==result[i].length);
            System.arraycopy(scoresOfDeltaAIteration[i],0, result[i], 0, scoresOfDeltaAIteration[i].length);
        }
    }
    
    public void getAllDeltaErrors(double[][] result){
        assert(result!=null);
        for(int i=0;i<errWeights.length; i++){
            assert(errWeights[i].length==result[i].length);
            System.arraycopy(errWeights[i],0, result[i],0, errWeights[i].length);
        }
    }
    
//    public int[][] getAllDeltaTotalSamples(){
//        return totalSamples;
//    }
    
    //this function udpates phinextIndex and reward using lstd's greedy result on the board and piece
    public abstract int greedyResult(int[][] board, int piece);
    public abstract void greedifyLSTD(boolean online);
    public abstract void learnModel();
}
