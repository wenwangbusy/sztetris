import java.util.Arrays;
import Jama.*;


public class TestJama{
	public static void main(String[] args){
		//testPackedCopies();
		//testMatrix();
		testTimes();
	}

	public static void testTimes(){
		Matrix A = new Matrix(2,2);
		printMatrix(A);
		Matrix b = new Matrix(new double[2], 2);
		//Matrix b = new Matrix(new double[2], 1);//error! dimension mismatch.
		printMatrix(b);
		printMatrix(A.times(b));
	
	}

	public static void testMatrix(){
		double[] a=new double[2];
		Arrays.fill(a,1);
		printMatrix(new Matrix(a,1)); 
		printMatrix(new Matrix(a,a.length));
	}


	public static void testPackedCopies(){
		//Matrix A = new Matrix(2,2);

		Matrix A = new Matrix(1,2);
		A.set(0,0, 2.0);
		A.set(0,1, 2.0);
		printMatrix(A);
		double[] a1 = A.getRowPackedCopy();
		for(int i=0; i<a1.length; i++){
			System.out.print(a1[i]+" ");
		}System.out.println();
		double[] a2 = A.getColumnPackedCopy();
		for(int i=0; i<a2.length; i++){
			System.out.print(a2[i]+" ");
		}System.out.println();

	}
	
	public static void printMatrix(Matrix A){
		for(int i=0; i<A.getRowDimension(); i++){
			for(int j=0; j<A.getColumnDimension(); j++){
				System.out.print(A.get(i,j) +" ");
			}System.out.println();
		}
	}
}
