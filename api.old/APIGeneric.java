package api;

import java.util.*;

import agent.*;
import features.*;
import tetrisengine.*;
import Jama.*;

/* A general approximate policy iteration algorithm class that uses LSTD0 for policy evaluation
 @see PolicyEvaluationGeneric
 */
public abstract class APIGeneric extends LSTDBinary{
	static final int numIterations=20;//100;
	double[][] errWeights = new double[getDeltaA().length][numIterations];//error of API in terms of weight difference norm
	double[][] scoresOfDeltaAIter = new double[getDeltaA().length][numIterations+1];//scores[i][j]: score for the policy at iteration i with deltaA[j]
	int[][] totalSamples = new int[getDeltaA().length][numIterations+1];
	
	public APIGeneric(){super();}
	
	public APIGeneric(FeatureExtractorGeneric f, boolean saveOnPolicySamples, boolean saveAllActionSamples){
	    super(f, saveOnPolicySamples, saveAllActionSamples);
	}
	public APIGeneric(FeatureExtractorGeneric f, boolean saveOnPolicySamples, boolean saveAllActionSamples, List<Transition> tr){
		super(f, saveOnPolicySamples, saveAllActionSamples, tr);
	}
	
	boolean initialPolicyProvided=false;
	double[] w0=new double[getWeights().length];
	public void setInitialPolicy(double[] w_given){
		initialPolicyProvided=true;
		w0=w_given;
	}

	
	/* Offline approximate policy iteration 
	 * given the samples saved in file fileName, 
	 * at each feature vector, select an action for the feature vector, and get a reward and the next feature vector
	 * update policy evaluation structures accordingly using this transition experience
	 * solve the weight vector
	 */
	public abstract void api(String[] samplefiles, boolean addOnSamples);

	/*Online approximate policy iteration or api using previously collected samples in transitsBin */
	public void api(boolean useOnlineSamples){
		System.out.println("Starting off-line API ");
		System.out.println("In LSPI-api, transits.size="+transits.size());
		
        boolean debug = false;
		
		double[] w=null;
		Matrix wM;
		for(int d=0; d<getDeltaA().length; d++){
			double delta = getDeltaA()[d];
			System.out.println("\n\n###### Delta = " + delta + "##############\n");
			
			if(!initialPolicyProvided){
				resetWeights();//reset the weights to 0 before iteration
				System.out.println("Starting with a zero weight policy");
				//randomizeWeights();
				//System.out.println("Starting with a random policy (w=rand)");
            }else{
				System.out.println("Initializing to the initial policy");
				setWeights(w0);
				//printWeights();
			}
			
			double iterErr=1e10;
			for(int iter=0; iter<getNumIterations() && iterErr>1e-5; iter++){
				System.out.println("\n   ----- LSPI Iteration " + (iter+1) + " -------");
				final long startTime = System.currentTimeMillis();
				
				//reset A,b at beginning of each iteration
				A = new Matrix(getFeatureExtractor().getNumFeatures(), getFeatureExtractor().getNumFeatures());
				b = new Matrix(getFeatureExtractor().getNumFeatures(), 1);
				
				if(debug){
                    System.out.println("Before this iteration, b is ");
                    for(int iw=0; iw<getWeights().length; iw++){
                        System.out.print(b.get(iw,0) + " ");
                    }System.out.println("");
				}
				
				//not adding on-policy samples; no improvement over the policy learned online
				//add on-policy samples: better?
				//setSaveOnPolicySamples(useOnlineSamples);
				//setSaveAllActionSamples(false);
				//setUseBinarySamples(true);
				
				setEpsilon(0);
				//scoresOfDeltaAIter[d][iter]=evaluate(40, false);
				
				//evaluate
				setSaveOnPolicySamples(useOnlineSamples);
				setSaveAllActionSamples(false);
				scoresOfDeltaAIter[d][iter]=evaluate(getNumTestGames(), false);
                
                /*
				 //new: add random samples
				 randomizeWeights();
				 evaluate(numTestEpisodes*10, false);
				 transitsBin = getSamplesBinary().getOnPolicyTrans();
				 int numRandSamples = transitsBin.size() - numOnpolicySamples;
				 System.out.println("Number of Random samples used: "+ numRandSamples);
				 */
                
				//setEpsilon(0.1);
				accumulateAb(useOnlineSamples);
                wM= solveWeights(delta);
                w = wM.getColumnPackedCopy();//the new weight vector
				
                if(debug){
                    System.out.println("After this iteration, b is ");
                    for(int iw=0; iw<getWeights().length; iw++){
                        System.out.print(b.get(iw,0) + " ");
                    }System.out.println("");
				}
                
				//try: still not working. is online sampling/learning important?
                if(useOnlineSamples)
                    clearSamples();
				
				//get the error of this iteration
				iterErr=0;
				for(int i=0; i<w.length; i++){
					iterErr += Math.abs(getWeight(i) - w[i]);
				}
				errWeights[d][iter]=iterErr;
				System.out.println("Time spent: " + (System.currentTimeMillis()-startTime)/1000 + " seconds");
				System.out.println(String.format("####Summary: Average score %f (before this iteration), iteration error: %f\n", scoresOfDeltaAIter[d][iter], errWeights[d][iter]));
				
				setWeights(w);//set the weights of the agent to the resulting weight vector from this iteration
				
			}//for
			
		}//while
	}//
	
	public abstract TetrisAction greedyResult(AfterState state, int piece);
	
	@Override
	public void accumulateAb(){;}
	
	boolean usePhinext=false;
	Integer[]phinextIndex;
	double[] phinext;//if usePhinext is true; use phinext; otherwise use phinextIndex
	
	double reward=-100;
	
	/*
	 Accumulate A, b through greedification on previously collected samples
	 */
	public void accumulateAb(boolean useOnlineSamples){
		//update transits and transitsBin
		if(useOnlineSamples){
			transitsBin=gettransitsBinInOnlineSamples();
			transits =  gettransitsInOnlineSamples();
		}
		System.out.println("samples size in accumulateAb() is "+transits.size());
		System.out.println("samplesBin size in accumulateAb() is "+transitsBin.size());
		assert(transitsBin.size()>0 && transitsBin.size()==transits.size());
		TransitionPhiBinary  trBin;
		Transition tr;
		int[] phiIndex;
		double totRewards=0;
		for(int ind=0; ind<transitsBin.size(); ind++){
			//System.out.println("Sample " + ind+1);
			
			trBin = transitsBin.get(ind);
			phiIndex = trBin.getPhiIndex();
			
			/*
			if(trBin.getAbsorb()){
				System.out.println("Entering a terminal state. ");
				continue;
			}*/
			
			
			tr=transits.get(ind);
			TetrisAction aGreedy=greedyResult(new AfterState(tr.getBoard()), tr.getPiece());
			if(!usePhinext)
				assert(phinextIndex!=null);
			else
				assert(phinext!=null);
			
			assert(reward!=-100);
			
			/*
			 AfterState state=new AfterState(tr.getBoard());
			 TetrisAction aGreedy=getGreedyAction(state,  tr.getPiece());
			 AfterState[] stateNextS = state.putTile(tr.getPiece(), aGreedy, true);
			 phinextIndex = ((BinaryFeatureExtractor)getFeatureExtractor()).getFeatureIndices(stateNextS[1].getBoard());
			 double reward = stateNextS[1].getReward();
			 */
			
			totRewards += reward;
			//System.out.println("Taking action " + aGreedy.getIndex()+": position="+aGreedy.getPosition()+"; rotation="+aGreedy.getRotation() );
			//System.out.println("The reward is " + reward);
			

			//copied from LSTDBinary::accumulateAb()---code should be reused rather than copied!!!
			for(int i=0; i<phiIndex.length; i++){
				b.set(phiIndex[i],0,  b.get(phiIndex[i],0) + reward);
				//A = A + phi*phi'
				for(int j=0; j<phiIndex.length; j++){
					A.set(phiIndex[i],phiIndex[j],  A.get(phiIndex[i],phiIndex[j]) + 1 );
				}
				
				if(!usePhinext){
					//A = A - gamma* phi*phinext'					
					if(phinextIndex!=null){
						for(int j=0; j<phinextIndex.length; j++){
							//A.set(phiIndex[i],phinextIndex[j],  getGamma()*(A.get(phiIndex[i],phinextIndex[j]) - 1) );	--this is wrong	(for gamma not equl 1)					
							A.set(phiIndex[i],phinextIndex[j],  A.get(phiIndex[i],phinextIndex[j]) - getGamma() );							
						}
					}
				}else{
					for(int jn=0; jn<phinext.length; jn++){
						if(phinext[jn]!=0){
							A.set(phiIndex[i],jn,   A.get(phiIndex[i],jn) - getGamma()* phinext[jn] );
						}
					}
				}			
			}
		}
		System.out.println("total Rewards in accumulateAb() is "+totRewards);
	}
	
	public static int getNumIterations(){return numIterations;}
	
    public double[][] getAllDeltaScores(){
        return scoresOfDeltaAIter;
    }
    public int[][] getAllDeltaTotalSamples(){
        return totalSamples;
    }
	
	//a handy function for action selection: get the greedy action for a given state board acc. the current weights
	public abstract TetrisAction getGreedyAction(AfterState s, int piece);
	
}
