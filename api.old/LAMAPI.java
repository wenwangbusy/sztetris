package api;
import java.util.*;
import features.*;
import agent.*;
import tetrisengine.*;
import Jama.*;


/* This is a LAMAPI algorithm; 
 right now implemented with Jama, which is very slow
 note: the piece is not considered (should be)
 
 @see <a href="http://webdocs.cs.ualberta.ca/~hengshua/papers/lamapi.pdf">http://webdocs.cs.ualberta.ca/~hengshua/papers/lamapi.pdf</a> 
 */
public class LAMAPI extends APIGeneric{
	public LAMAPI(FeatureExtractorGeneric f, boolean useBinarySamples, List<Transition> tr, List<TransitionPhiBinary> tr2){
		super(f, false, false, useBinarySamples, tr, tr2);
		usePhinext=true;
	}

	/* action models */
	int d=getFeatureExtractor().getNumFeatures();
	int Nactions = TetrisGame.getNactions();
	Matrix[] F = new Matrix[Nactions];
	Matrix[] f = new Matrix[Nactions];
	{
		for(int a=0; a<Nactions; a++){
			f[a] = new Matrix(d,1);
			F[a] = new Matrix(d,d);
		}
	}
	final static double deltaD=100;//0.01;//1;
	
	//learn the action model
	public void learnModel(){
		final long startTime = System.currentTimeMillis();
		
		Transition tmp = new Transition();
		TransitionPhiBinary tmpB =  new TransitionPhiBinary();
		Integer[] phiIndex, phinextIndex;
		
		Matrix[] e=new Matrix[Nactions];
		Matrix[] D=new Matrix[Nactions];
		Matrix[] E=new Matrix[Nactions];
		for(int a=0; a<e.length; a++){
			e[a] = new Matrix(d,1);
			D[a] = Matrix.identity(d,d);
			D[a].timesEquals(deltaD);
			E[a] = new Matrix(d,d);
		}
		
		for(int ind=0; ind<transits.size(); ind++){
			tmp = transits.get(ind);
			int a = (tmp.getAction()).getIndex();
			double reward =  tmp.getReward();
			
			tmpB =  transitsBin.get(ind);
			phiIndex = tmpB.getPhiIndex();
			
			//important: terminatal feature vector is 0
			if(tmp.getAbsorb())
				phinextIndex=null;
			else
				phinextIndex = tmpB.getPhinextIndex();
			
			//e[a] = e[a] + phi*r; D[a]=D[a]+phi*phi'; E[a] = E[a] + phi*phinext
			for(int i=0; i<phiIndex.length; i++){
				e[a].set(phiIndex[i],0,  e[a].get(phiIndex[i],0) + reward);
				for(int j=0; j<phiIndex.length; j++){
					D[a].set(phiIndex[i],phiIndex[j],  D[a].get(phiIndex[i],phiIndex[j]) + 1 );
				}
				if(phinextIndex!=null){
					for(int j=0; j<phinextIndex.length; j++){
						E[a].set(phiIndex[i],phinextIndex[j],    E[a].get(phiIndex[i],phinextIndex[j]) + 1 );							
					}
				}
			}
		}
		for(int a=0; a<Nactions; a++){
			assert(D[a]!=null);
			assert(e[a]!=null);
			f[a] = D[a].solve(e[a]);
			assert(f[a]!=null);
			F[a] = D[a].solve(E[a]);
			F[a] = F[a].transpose();
		}
		
		System.out.println("Time spent: " + (System.currentTimeMillis()-startTime)/1000 + " seconds");
	}

	@Override
	public TetrisAction greedyResult(AfterState state, int piece){
		TetrisAction aGreedy=getGreedyAction(state,  piece);
		return aGreedy;
	}
	
	//get the greedy action; also update phinextIndex and reward
	@Override
	public TetrisAction getGreedyAction(AfterState s, int piece){ 
		Integer[] phiIndex = ((BinaryBertsekasIoffeFeatureExtractorTabular)getFeatureExtractor()).getFeatureIndices(s.getBoard());
		double valMax= TetrisGame.getINF();
		double val=0, faTphi=0, Vaphi=0;
		double[] FTw;
		int aStar=-1;
		Matrix w = new Matrix(getWeights(), getWeights().length);
		for(int a=0; a<Nactions; a++){
			FTw = ( (F[a].transpose()).times(w) ).getRowPackedCopy();
			faTphi=0;
			for(int i=0; i<phiIndex.length; i++){
				Vaphi += FTw[phiIndex[i]];
				faTphi += f[a].get(phiIndex[i],0);
			}
			val = faTphi + getGamma()*Vaphi; 
			if(val>valMax){
				valMax=val;
				aStar = a;
				reward = faTphi;
				phinext = getPhinextProjection(phiIndex, aStar);
			}
		}
		System.out.println("valMax="+valMax);
		assert(0<=aStar && aStar<Nactions);
		return TetrisAction.getAction(aStar);
	}
	
	/* get the projected phinext according to F
	 */
	public double[] getPhinextProjection(Integer[] phiIndex, int a){
		phinext = new double[d];
		double[] phi=((BinaryBertsekasIoffeFeatureExtractorTabular)getFeatureExtractor()).getFeatures(phiIndex);
		Matrix phiM = new Matrix(phi, phi.length);//a column vector
		Matrix res = F[a].times(phiM);
		return res.getColumnPackedCopy();//same as getRowPackedCopy (for vector matrices)
	}
	
	@Override
	public void api(String[] file, boolean b){;}
		
}
