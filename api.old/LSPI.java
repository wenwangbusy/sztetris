package api;
import java.util.*;
import features.*;
import agent.*;
import Jama.*;
import tetrisengine.*;

/* This is a special LSPI algorithm that uses state features. 
 It takes advantage of a simulator in obtaining the transition following taking an action at a feature vector. 
 */
public class LSPI extends APIGeneric{
	
	public LSPI(FeatureExtractorGeneric f, boolean useBinarySamples, List<Transition> tr, List<TransitionPhiBinary> tr2){
		super(f, false, false, useBinarySamples, tr, tr2);
		System.out.println("In LSPI(), transits.size="+transits.size());
		usePhinext=false;
	}
	
	@Override
	public void api(String[] file, boolean b){;}
	

	//greedification: get phinextIndex and reward
	@Override
	public TetrisAction greedyResult(AfterState state, int piece){
		TetrisAction aGreedy=getGreedyAction(state,  piece);
		AfterState[] stateNextS = state.putTile(piece, aGreedy, true);
		phinextIndex = ((BinaryFeatureExtractor)getFeatureExtractor()).getFeatureIndices(stateNextS[1].getBoard());
		reward = stateNextS[1].getReward();
		return aGreedy;
	}
	
	@Override
	public TetrisAction getGreedyAction(AfterState s, int piece){
		setAgentState(s);
		return putTileGreedy(piece);
	}
}
