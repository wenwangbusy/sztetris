package api;

import agent.*;
import features.*;

/* A general approximate policy iteration algorithm class that uses LSTD0 for policy evaluation, and record samples
 @see PolicyEvaluationGeneric
 */
public abstract class SampleCollectorBinary extends LSTDBinary{
	static final int numIterations=APIGeneric.getNumIterations();
	double[][] errWeights = new double[getDeltaA().length][numIterations];//error of API in terms of weight difference norm
	double[][] scoresOfDeltaAIter = new double[getDeltaA().length][numIterations+1];//scores[i][j]: score for the policy at iteration i with deltaA[j]
    
	int[][] totalSamples = new int[getDeltaA().length][numIterations+1];
	
	public SampleCollectorBinary(){super();}
	
	public SampleCollectorBinary(FeatureExtractorGeneric f, boolean saveOnPolicySamples, boolean saveAllActionSamples, boolean useBinarySamples){
	    super(f, 0.0, saveOnPolicySamples, saveAllActionSamples, useBinarySamples);
	}
	
	/* Offline approximate policy iteration 
	 * given the samples saved in file fileName, 
	 * at each feature vector, select an action for the feature vector, and get a reward and the next feature vector
	 * update policy evaluation structures accordingly using this transition experience
	 * solve the weight vector
	 */
	//public abstract void api(String[] samplefiles, boolean addOnSamples);

	/*Online collector (add samples to transits)*/
	public abstract void adaptOnline();
	
	public static int getNumIterations(){return numIterations;}
	
    public double[][] getAllDeltaScores(){
        return scoresOfDeltaAIter;
    }
    public int[][] getAllDeltaTotalSamples(){
        return totalSamples;
    }
	
}
