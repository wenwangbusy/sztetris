package api;

import agent.*;
import tetrisengine.*;
import features.*;

import java.io.*;

import Jama.Matrix;

/* LAM-API with accurate projection using BinaryBertsekasIoffeFeatureExtractorTabular features
 @see SampleCollectorBinary
 Collecting samples using LAM-API with an exact LAM model (simulator)
 */
public class LAMAPIAccurateBinaryBertTabular extends SampleCollectorBinary{	
	final static int numEffectiveFeatures=30;
	Integer[] phiIndex = new Integer[getNumEffectiveFeatures()]; 
	TetrisAction tA;
	Integer[] phinextIndex = new Integer[getNumEffectiveFeatures()];
	double reward;
	
	public LAMAPIAccurateBinaryBertTabular(){super();setName("LAM-API-Accurate");}
	public LAMAPIAccurateBinaryBertTabular(FeatureExtractorGeneric f, boolean saveOnPolicySamples, boolean saveAllActionSamples, boolean useBinarySamples){
		super(f, saveOnPolicySamples, saveAllActionSamples, useBinarySamples);
		setName("LAM-API-Accurate");
	}
	
	
	/* Using both samples collected by CE and collected by the current policy. 
	* 	
	 */
    /*
	public void apiAddOnPolicySamples(String[] sampleFiles, boolean addOnPolicySamples){
		double[] w_old=null, w=null;
		Matrix wM=null;
		for(int d=0; d<getDeltaA().length; d++){
			double delta = getDeltaA()[d];
			System.out.println("\n\n###### Delta = " + delta + "##############\n\n");
			resetWeights();//reset the weights to 0 before iteration
			setSaveOnPolicySamples(false);
			setSaveAllActionSamples(false);
			setUseBinarySamples(false);
			scoresOfDeltaAIter[d][0]=evaluate(getNumTestGames(), false);//performance of w=0: a baseline	
			System.out.println(String.format("####Average score for initial weight vector: %f", scoresOfDeltaAIter[d][0]));
			
			clearSamples();
			for(int iter=0; iter<getNumIterations(); iter++){
				//delta *= Math.pow(10, iter);
				//System.out.println("\n\n###### Delta = " + delta + "##############\n\n");
				
				System.out.println("\n----- Iteration " + iter + " -------");
				final long startTime = System.currentTimeMillis();
				
				//reset A,b at beginning of each iteration
				A = new Matrix(getFeatureExtractor().getNumFeatures(), getFeatureExtractor().getNumFeatures());
				b = new Matrix(getFeatureExtractor().getNumFeatures(), 1);
				
//				System.out.println("Before this iteration, Weights are ");
//				for(int iw=0; iw<getWeights().length; iw++){
//					System.out.print(getWeights()[iw] + " ");
//				}System.out.println("\n");
				
				//save the weights before this iteration starts
				w_old = new double[getWeights().length];
				System.arraycopy(getWeights(),0,w_old,0,getWeights().length);
				
				for(String file: sampleFiles){
					buildAbFromFile(file);
				}
				
				wM = solveWeights(delta);
				w = wM.getColumnPackedCopy();
				setWeights(w);//set the weights of the agent to the resulting weight vector from this iteration
				scoresOfDeltaAIter[d][iter+1]=evaluate(getNumTestGames(), false);//evaluate the weight vector and recollect samples
				System.out.println("The score Before adding on-policy samples is " + scoresOfDeltaAIter[d][iter+1]);
				
				if(addOnPolicySamples){
					setSaveOnPolicySamples(true);
					setSaveAllActionSamples(false);
					setUseBinarySamples(true);
					int numEisodedsForCurrentPolicy = 20;
					int numInnerIterations = 20;
					
					assert(gettransitsBin().size()==0);
					evaluate(numEisodedsForCurrentPolicy, false);//collect samples for the current policy
					for(int rep=0; rep<numInnerIterations; rep++){
						System.out.println("\n>>>rep= " + rep);
						//option: reset A,b at beginning of each inner iteration: seems bad; no! a large delta is fine
						A = new Matrix(getFeatureExtractor().getNumFeatures(), getFeatureExtractor().getNumFeatures());
						b = new Matrix(getFeatureExtractor().getNumFeatures(), 1);
						
						transitsBin = getSamplesBinary().getOnPolicyTrans();
						assert(transitsBin.size()>0);
						System.out.println(">>>using "+transitsBin.size() + " on-policy samples");
						accumulateAb();
						clearSamples();

						double[] w_old_inner = new double[getWeights().length];
						System.arraycopy(getWeights(),0,w_old_inner,0,getWeights().length);
						
						//option: solve here: better than solving after rep is finished; this basically apply an inner api process to the on-line samples
						wM = solveWeights(delta);
						w = wM.getColumnPackedCopy();
						setWeights(w);//set the weights of the agent to the resulting weight vector from this iteration
						
						//get the error of this inner iteration
						double errInner = 0;
						for(int i=0; i<w_old_inner.length; i++){
							errInner += Math.abs(w_old_inner[i] - w[i]);
						}
						System.out.println(">>iteration error " + errInner);
						
						scoresOfDeltaAIter[d][iter+1]=evaluate(getNumTestGames(), false);//evaluate the weight vector and recollect samples
						System.out.println(">>>score is " + scoresOfDeltaAIter[d][iter+1]);
					}
				}
				
//				System.out.println("After this iteration, Weights are ");
//				for(int iw=0; iw<getWeights().length; iw++){
//					System.out.print(getWeights()[iw] + " ");
//				}System.out.println("\n");
				System.out.println("After this iteration, b is ");
				for(int iw=0; iw<getWeights().length; iw++){
					System.out.print(b.get(iw,0) + " ");
				}System.out.println("");
				
				//get the error of this iteration
				errWeights[d][iter] = 0;
				for(int i=0; i<w_old.length; i++){
					errWeights[d][iter] += Math.abs(w_old[i] - w[i]);
				}
				//set w_old to the weights after this iteration
				for(int i=0; i<w.length; i++){
					w_old[i] = w[i];
				}
				System.out.println("Time spent: " + (System.currentTimeMillis()-startTime)/1000 + " seconds");
				System.out.println(String.format("####Iteration %d: Average score %f, iteration error: %f\n", iter, scoresOfDeltaAIter[d][iter+1], errWeights[d][iter]));
			}//for iter
			for(int iter=0; iter<getNumIterations(); iter++){
				System.out.println(String.format("####Iteration %d: Average score %f, iteration error: %f", iter, scoresOfDeltaAIter[d][iter+1], errWeights[d][iter]));
			}
			
			
		}//for d
		String resultFile="";
		for(String file: sampleFiles)
			resultFile += file;
		
		try{
			writeAPIResults(resultFile + getName() + "_scores", scoresOfDeltaAIter);
			writeAPIResults(resultFile + getName() + "_iterErrors", errWeights);
		}catch (IOException e) {
			e.printStackTrace(System.out);
		}
	}
	
	*/
	
	/* The first iteration use CE samples, afterwards all iterations use on-policy samples only. stablized, no (150, -0.6, 140, 0.5) chattering, but doens't improve.  */
    /*
	@Override
	public void api(String[] sampleFiles, boolean addOnPolicySamples){
		double[] w_old=null, w=null;
		Matrix wM=null;
		for(int d=0; d<getDeltaA().length; d++){
			double delta = getDeltaA()[d];
			System.out.println("\n\n###### Delta = " + delta + "##############\n\n");
			resetWeights();//reset the weights to 0 before iteration
			setSaveOnPolicySamples(false);
			setSaveAllActionSamples(false);
			setUseBinarySamples(false);
			scoresOfDeltaAIter[d][0]=evaluate(getNumTestGames(), false);//performance of w=0: a baseline	
			System.out.println(String.format("####Average score for initial weight vector: %f", scoresOfDeltaAIter[d][0]));
			
			clearSamples();
			for(int iter=0; iter<getNumIterations(); iter++){
				//delta *= Math.pow(10, iter);
				//System.out.println("\n\n###### Delta = " + delta + "##############\n\n");
				
				System.out.println("\n----- Iteration " + iter + " -------");
				final long startTime = System.currentTimeMillis();

				//reset A,b at beginning of each iteration
				A = new Matrix(getFeatureExtractor().getNumFeatures(), getFeatureExtractor().getNumFeatures());
				b = new Matrix(getFeatureExtractor().getNumFeatures(), 1);

				
//				System.out.println("Before this iteration, Weights are ");
//				for(int iw=0; iw<getWeights().length; iw++){
//					System.out.print(getWeights()[iw] + " ");
//				}System.out.println("\n");
				
				w_old = new double[getWeights().length];
				System.arraycopy(getWeights(),0,w_old,0,getWeights().length);

				if(addOnPolicySamples){
					setSaveOnPolicySamples(true);
					setSaveAllActionSamples(false);
					setUseBinarySamples(true);
					int numEisodedsForCurrentPolicy = 10;
					//the first iteration starts with samples
					if(iter==0){
						assert(gettransitsBin().size()==0);
						for(String file: sampleFiles){
							buildAbFromFile(file);
						}
						wM= solveWeights(delta);
						w = wM.getColumnPackedCopy();
						setWeights(w);//set the weights of the agent to the resulting weight vector from this iteration
						scoresOfDeltaAIter[d][iter+1]=evaluate(numEisodedsForCurrentPolicy, false);//evaluate the weight vector and collect samples
						transitsBin = getSamplesBinary().getOnPolicyTrans();
						System.out.println("gettransitsBin().size()="+gettransitsBin().size());
					}else{//from the second iteration on, use only on-policy samples
						//reset: not using the A, b from the first iteration
						System.out.println("gettransitsBin().size()="+gettransitsBin().size());
						assert(gettransitsBin().size()>0);
						for(int rep=0; rep<2; rep++){
							System.out.println(">>>Adding on-policy samples loop " + rep);
							transitsBin = getSamplesBinary().getOnPolicyTrans();
							System.out.println(">>>using "+transitsBin.size() + " on-policy samples");
							accumulateAb();
							clearSamples();
							assert(transitsBin.size()==0);
							evaluate(numEisodedsForCurrentPolicy, false);
							assert(transitsBin.size()>0);
						}
						clearSamples();
						assert(transitsBin.size()==0);
						wM = solveWeights(delta);
						w = wM.getColumnPackedCopy();
						setWeights(w);//set the weights of the agent to the resulting weight vector from this iteration
						scoresOfDeltaAIter[d][iter+1]=evaluate(numEisodedsForCurrentPolicy, false);//evaluate the weight vector and recollect samples
					}
				}else{
					//System.out.println("Internal sample size before receiving outside samples: " + gettransitsBin().size());
					assert(gettransitsBin().size()==0);
					for(String file: sampleFiles){
						buildAbFromFile(file);
					}
					wM= solveWeights(delta);
					w = wM.getColumnPackedCopy();
					setWeights(w);
					setSaveOnPolicySamples(false);
					setSaveAllActionSamples(false);
					setUseBinarySamples(false);
					scoresOfDeltaAIter[d][iter+1]=evaluate(getNumTestGames(), false);
				}
				
//				System.out.println("After this iteration, Weights are ");
//				for(int iw=0; iw<getWeights().length; iw++){
//					System.out.print(getWeights()[iw] + " ");
//				}System.out.println("\n");
				System.out.println("After this iteration, b is ");
				for(int iw=0; iw<getWeights().length; iw++){
					System.out.print(b.get(iw,0) + " ");
				}System.out.println("");
				
				//get the error of this iteration
				errWeights[d][iter] = 0;
				for(int i=0; i<w_old.length; i++){
					errWeights[d][iter] += Math.abs(w_old[i] - w[i]);
				}
				//set w_old to the weights after this iteration
				for(int i=0; i<w.length; i++){
					w_old[i] = w[i];
				}
				System.out.println("Time spent: " + (System.currentTimeMillis()-startTime)/1000 + " seconds");
				System.out.println(String.format("####Iteration %d: Average score %f, iteration error: %f\n", iter, scoresOfDeltaAIter[d][iter+1], errWeights[d][iter]));
			}//for iter
			for(int iter=0; iter<getNumIterations(); iter++){
				System.out.println(String.format("####Iteration %d: Average score %f, iteration error: %f", iter, scoresOfDeltaAIter[d][iter+1], errWeights[d][iter]));
			}


		}//for d
		String resultFile="";
		for(String file: sampleFiles)
			resultFile += file;
		
		try{
			writeAPIResults(resultFile + getName() + "_scores", scoresOfDeltaAIter);
			writeAPIResults(resultFile + getName() + "_iterErrors", errWeights);
		}catch (IOException e) {
			e.printStackTrace(System.out);
		}
	}
    */
	
	/* Sweep a sample file, apply action selection and update A,b 
	 *
	 */
	public void buildAbFromFile(String sampleFile){
		String sampleFilePhi = sampleFile + "phi2.txt";
		String sampleFilePhinext = sampleFile + "phinext2.txt";
		String sampleFileR = sampleFile + "reward2.txt";
		
		System.out.println("Reading Phi samples from " + sampleFile);
		System.out.println("Reading all-action Phinext samples from " + sampleFilePhinext);
		System.out.println("Reading all-action Reward samples from " + sampleFileR);
		BufferedReader fPhi, fPhinext, fR;
		String strPhi, strPhinext, strR;
		
		final int Nactions = TetrisGame.getNactions();
		double bestVnext=TetrisGame.getINF();
		int bestActionIndex=-1;
		Integer[] bestPhinextIndex=null; 
		double bestReward=-1;
		TransitionPhiBinary bestTransitionBinary=null;
		boolean absorb=true;
		int epi=0;
		int line =0;
		
		double[] w_old=null, w=null;
		Matrix wM;
		
		try{
			fPhi = new BufferedReader(new FileReader(sampleFilePhi));
			fPhinext = new BufferedReader(new FileReader(sampleFilePhinext));
			fR = new BufferedReader(new FileReader(sampleFileR));
			while( (strPhi=fPhi.readLine())!=null){	
				//processing action selection result for the last state
				//System.out.println("Line " + line);
				if((line+1)%Nactions==0){	
					if(absorb){//a failure state, construct a fake best transition
						bestPhinextIndex = phiIndex;
						bestTransitionBinary = new TransitionPhiBinary(getFeatureExtractor(),4, phiIndex, new TetrisAction(), bestPhinextIndex, -1, true);	//piece and action does't matter in policy evaluation	
					}else{
						//System.out.println("Selecting action " + bestActionIndex);
						bestTransitionBinary = new TransitionPhiBinary(getFeatureExtractor(),4, phiIndex, new TetrisAction(), bestPhinextIndex,bestReward, bestReward<0);	//piece and action does't matter in policy evaluation	
					}	
					addSamples(bestTransitionBinary);
					if(absorb){
						epi++;
						//System.out.println("Episode: " + epi + " ends at state " + line/Nactions);
						//System.out.println("Episode: " + epi + " ends at line " + line);
						accumulateAb();
						clearSamples();
					}
					//starting processing for a new state; selecting the best action for it
					bestVnext = TetrisGame.getINF();
					bestActionIndex =-1;
					bestPhinextIndex = null;
					bestReward = -1;
					absorb = true;
				}
				//System.out.println("Processing phiIndex:");
				stringsToArray(strPhi, phiIndex);
				
				//System.out.println("Processing phinextIndex:");
				strPhinext = fPhinext.readLine(); assert(strPhinext!=null);
				stringsToArray(strPhinext, phinextIndex);
				
				strR = fR.readLine(); assert(strR!=null);
				reward = Double.valueOf(strR);
				if(reward>=0) absorb=false;
				
				double valnext = reward + ((BinaryFeatureExtractor)getFeatureExtractor()).getValue(phinextIndex, getWeights());
				//System.out.println(String.format("action %d: valnext = %f", line%Nactions, valnext));
				if(valnext>bestVnext){
					bestVnext = valnext;
					bestActionIndex = line%Nactions;
					bestReward=reward;
					//bestPhinextIndex = phinextIndex;//reference???
					bestPhinextIndex = new Integer[phinextIndex.length];
					System.arraycopy(phinextIndex,0,bestPhinextIndex,0, phinextIndex.length); 
				}
				line++;
			}//while
			//System.out.println("There are " + line + " lines of all-action samples");
			fPhi.close();fPhinext.close(); fR.close();
		}catch (IOException e) {
			e.printStackTrace(System.out);
		}	
		System.out.println("There are " + epi +" episodes of samples");
	}
	
    /* aggressive API: collecting samples online. Self-collecting-improving
     */
	@Override
	public void adaptOnline(){
        int SCORE_GOOD=100;//1;//20;
        boolean debug = false;
        
		System.out.println("Starting on-line policy iteration ");
		System.out.println("Collecting " + getNumTestGames() + " episodes of samples for the current policy at each iteration");
		double[] w_old=null, w=null;
		Matrix wM;
		for(int d=0; d<getDeltaA().length; d++){
			double delta = getDeltaA()[d];
			System.out.println("\n\n###### Delta = " + delta + "##############\n");
			//resetWeights();//reset the weights to 0 before iteration
			randomizeWeights();
			System.out.println("Starting with a random policy (w=rand)");
            
        for(int iter=0; iter<getNumIterations(); iter++){
				System.out.println("\n   ----- Online Iteration " + (iter+1) + " -------");
				final long startTime = System.currentTimeMillis();
				
				//reset A,b at beginning of each iteration
				A = new Matrix(getFeatureExtractor().getNumFeatures(), getFeatureExtractor().getNumFeatures());
				b = new Matrix(getFeatureExtractor().getNumFeatures(), 1);
				
								
				if(debug){
                    System.out.println("Before this iteration, b is ");
                    for(int iw=0; iw<getWeights().length; iw++){
                        System.out.print(b.get(iw,0) + " ");
                    }System.out.println("");
				}
                    
				//save the weights before this iteration starts
				w_old = new double[getWeights().length];
				System.arraycopy(getWeights(),0,w_old,0,getWeights().length);
				
				//collect samples for the current policy
				setSaveOnPolicySamples(true);
				setSaveAllActionSamples(false);
				setUseBinarySamples(true);
				setEpsilon(0);
				scoresOfDeltaAIter[d][iter]=evaluate(getNumTestGames(), false);
                
                System.out.println("Total score: " + scoresOfDeltaAIter[d][iter]);
                transitsBin = getSamplesBinary().getOnPolicyTrans();
				transits = getSamples().getOnPolicyTrans();
            
            //these 3 numbers:
                int numOnpolicySamples = transitsBin.size();
                System.out.println("Number of samples collected for this policy (transitsBins.size()): "+ numOnpolicySamples);
                System.out.println("Number of samples for the trajectory samples for this policy(getSamplesBinary().getOnPolicyTrans().size()): "+ getSamplesBinary().getOnPolicyTrans().size());
                System.out.println("Number of samples for the trajectory samples for this policy (getSamples().getOnPolicyTrans().size()): "+ getSamples().getOnPolicyTrans().size());
            
            
                if(scoresOfDeltaAIter[d][iter]>=SCORE_GOOD){
                    System.out.println("Now stopping the agressive API sample collection because average score is bigger than " + SCORE_GOOD);
                    System.out.println("Finally, we collected "+numOnpolicySamples+ " samples for off-line API");
                    break;
                }
                
                
                /*
                //new: add random samples
                randomizeWeights();
                evaluate(numEisodedsForCurrentPolicy*10, false);
                transitsBin = getSamplesBinary().getOnPolicyTrans();
                int numRandSamples = transitsBin.size() - numOnpolicySamples;
                System.out.println("Number of Random samples used: "+ numRandSamples);
                */
                
                totalSamples[d][iter] = transitsBin.size();
                System.out.println("The total number of samples is "+ totalSamples[d][iter]);
                
                accumulateAb();
                wM= solveWeights(delta);
                w = wM.getColumnPackedCopy();
                setWeights(w);//set the weights of the agent to the resulting weight vector from this iteration
                
                
                //clear the samples after every iteration: used for the summarized exp
                clearSamples();
				assert(transitsBin.size()==0);
				
				System.out.println("After clearSamples():\n Number of samples collected for this policy (transitsBins.size()): "+ transitsBin.size());
				System.out.println("Number of samples for the trajectory samples for this policy(getSamplesBinary().getOnPolicyTrans().size()): "+ getSamplesBinary().getOnPolicyTrans().size());
				System.out.println("Number of samples for the trajectory samples for this policy (getSamples().getOnPolicyTrans().size()): "+ getSamples().getOnPolicyTrans().size());
         
                if(debug){
                    System.out.println("After this iteration, b is ");
                    for(int iw=0; iw<getWeights().length; iw++){
                        System.out.print(b.get(iw,0) + " ");
                    }System.out.println("");
				}
                
				//get the error of this iteration
				errWeights[d][iter] = 0;
				for(int i=0; i<w_old.length; i++){
					errWeights[d][iter] += Math.abs(w_old[i] - w[i]);
				}
				//set w_old to the weights after this iteration
				for(int i=0; i<w.length; i++){
					w_old[i] = w[i];
				}
				System.out.println("Time spent: " + (System.currentTimeMillis()-startTime)/1000 + " seconds");
				System.out.println(String.format("####Summary: Average score %f, iteration error: %f\n", scoresOfDeltaAIter[d][iter], errWeights[d][iter]));
				
			}//for
		
		}//while
		
	}//
	
    	
	public void writeAPIResults(String file, double[][]res) throws IOException{
		BufferedWriter f = new BufferedWriter(new FileWriter(file));
		for(int d=0; d<getDeltaA().length; d++){
			for(int iter=0; iter<getNumIterations(); iter++){
				f.write(res[d][iter] + " ");	
			}f.write("\n");
		}
		f.close();
	}
	
	public void 	stringsToArray(String strPhi, Integer[] phiIndex){
		String[] slist = strPhi.split(" +");
		assert(slist.length==getNumEffectiveFeatures());
		for (int i=0; i<getNumEffectiveFeatures(); i++){
			phiIndex[i] = Integer.valueOf(slist[i]);
			//System.out.println(slist[i] + "=? feature[" + i + "]=" + phiIndex[i] + "; ");
		}
	}
	
	
	public static int getNumEffectiveFeatures(){return numEffectiveFeatures;}
	
}
