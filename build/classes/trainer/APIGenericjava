package trainer;

import agent.*;
import features.*;
import tetrisengine.*;

import java.io.*;
import java.util.Arrays;
import java.util.Random;
import java.util.List;

import Jama.Matrix;


/** Achieve the same function as api.APIGeneric, but in a better way (not extending the agent, but have the agent as an field). This is a better design.
    With an agent acting as a behavior policy to collect samples, we apply API to get a policy. 
    We also evaluate the induced policy.
 
    move evaluate() to here from Evaluator
 */
public abstract class APIGeneric extends LSTDBinary{
    static final int numIterations=20;//100;
	double[][][][] scores = new double[Nruns][game.getNumGames()][deltaA.length][numIterations];//scores[r][e][d][i] is the (average) score of the policy trained for some API algorithm run r and delta d, given e episodes at iteration i
    
	/** this evaluates an agent (with a feature extractor and weights learned and saved previously) by playing a number of games, 
	 and getting the average score; averaged over a number of runs
	 @param game Tetris Game
	 @param agent a feature agent, which acts and collects samples according to its weight vector
	 */
    public APIGeneric(TetrisGame game, FeatureUserAgent collectionAgent)
    {
        super(game, collectionAgent);
    }

	@Override
	public String toString(){
		String str=super.toString();
		
		str += "--Average scores for API from samples collected from the agent: \n";
		for(int run=0; run<TetrisGame.getRUNS(); run++){
			str += "run "+ run + "\n";
            for(int epi=0; epi<game.getNumGames(); epi++){
                str += "----episode "+ epi + "\n-----";
                for(int del=0; del<deltaA.length; del++){
                    str += scores[run][epi][del] + ",";
                }
                str += "\n";
            }
			str += "\n";
		}
		return str;
	}
	
	/* Write the results to files */
	public void writeResults(){
		try{
			//write the agent peformance
			String file = "scores" + game.getNumGames()+ "epis_agent.txt";
			BufferedWriter fScoresAgent = new BufferedWriter(new FileWriter(file, true));
			for(double sc:scoreAgent)
				fScoresAgent.write(sc + " ");
			fScoresAgent.close();

            //write API results
            String fileScores0 = "scores" + game.getNumGames() + "_";
            for(int del=0; del<deltaA.length; del++){
                String fileScores = fileScores0 + "deltaindex"+del + ".txt";
                BufferedWriter fScores = new BufferedWriter(new FileWriter(fileScores, true));
                for(int run=0; run<TetrisGame.getRUNS(); run++){
                    for(int epi=0; epi<game.getNumGames(); epi++){
                        fScores.write(scores[run][epi][del] + " ");
                    }
                    fScores.write("\n");
                }
                fScores.close();
            }
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
    
    @Override
    public void evaluate(){;}
    
    public abstract void runAPI();
    
}
