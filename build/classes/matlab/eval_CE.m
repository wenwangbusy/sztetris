%added regression on the CE score function
%evaluating the CE policy; data obtained from java running CE for a number
%of games
% close all;
homedir = '/Users/admin/sztetris/'
% dir =[homedir 'CE_BI_run00_formatlab.txt_samples_policyOnly_']%CE samples only%samples fro w run00
% dir =[homedir 'CE_BI_run01_formatlab.txt_samples_']%samples fro w run01
dir =[homedir 'CE_BI_run01_formatlab.txt_samples_policyOnly_']

% fphi=[dir 'phi.txt'];
fphi=[dir 'phiBITab.txt'];
fpiece=[dir 'piece.txt'];
fa=[dir 'action.txt'];
% fphinext=[dir 'phinextAfterErase.txt'];
% fphinextBeforeErase=[dir 'phinextBeforeErase.txt'];
fr=[dir 'reward.txt'];
fabs=[dir 'absorb.txt'];
fpolicy=[dir 'policy.txt'];
% fVs=[dir 'Vs.txt'];
fboard=[dir 'board.txt'];
% fboardAfterErase=[dir 'boardAfterErase.txt'];
% fboardBeforeErase=[dir 'boardBeforeErase.txt'];

% fid = fopen(fboard);
% board=fscanf(fid, '%416d', [416, inf]);
% board=board';%slow
% fclose(fid);
% size(board)

% fid = fopen(fboardAfterErase);
% boardAfter=fscanf(fid, '%416d', [416, inf]);
% boardAfter=boardAfter';
% fclose(fid);
% size(boardAfter)
% 
% norm(boardAfter(1,:) - board(2,:))%this is not 0, fishie!
% fprintf(board2Chars(board(2,:)))
% fprintf(board2Chars(boardAfter(1,:)))
% %so that's because the decolorization in java; fine. just don't use Phinext
% %in LSTD
% 
% fid = fopen(fboardBeforeErase);
% boardBefore=fscanf(fid, '%416d', [416, inf]);
% boardBefore=boardBefore';
% fclose(fid);
% size(boardBefore)
% 
% fid = fopen(fpiece);
% piece=fscanf(fid, '%d');
% fclose(fid);
% size(piece)
% 
fid = fopen(fpiece);
piece=fscanf(fid, '%d');
fclose(fid);
size(piece)
% 
fid = fopen(fabs);
absorb=fscanf(fid, '%d');
fclose(fid);
size(absorb)

fid = fopen(fr);
reward=fscanf(fid, '%f');
fclose(fid);
size(reward)


fid = fopen(fa);
action = fscanf(fid, '%d %d %d', [3,inf]);
fclose(fid);
size(action)
actionIndex = action(1,:) +1;
% unique(actionIndex)


fid = fopen(fphi);
% Phi = fscanf(fid, '%21f', [21,inf]);
Phi = fscanf(fid, '%630f', [630,inf]);
fclose(fid);
Phi=Phi';
size(Phi)

% Phi_tab=tableLookup(Phi(:,1:10),20);
% Phi_tab=tableLookup(Phi(:,1:20),20);

terms=find(absorb==1);

[R]=getReturn(reward, absorb);
%verified same with ls for lstd1
deltaA=1;
numEpisodes=length(terms);
[w, R_app, D, e]= leastSquares(Phi, R, deltaA);

deltaA_array=[4:8];%[-2:8];
lambda=[0];%[0:0.2:1];
I=eye(size(Phi,2));
Nactions=40;
Niterations=5;
for ind_lam=1:length(lambda)
    lam = lambda(ind_lam);
    [A,b]=lstd_tetris(lambda, Phi, reward, terms, 1.0, numEpisodes, R);
    if(lam==1)
        assert(norm(b-e)==0)
        assert(norm(D-A)==0);
    end

    %lamapi
    [D, e, G]=learnLam_tetris(Phi, piece, actionIndex, reward,terms, 1.0, numEpisodes);
        
    for ind_delta=1:length(deltaA_array)
        deltaA = 10^deltaA_array(ind_delta);
        wlstd{ind_lam}{ind_delta}= (A+deltaA*I)\b;
        fweight=[dir 'wlstdBITab_lam' num2str(lam) '_deltaA' num2str(deltaA)];
        fid=fopen(fweight,'w');
        fprintf(fid, '%s', num2str(wlstd{ind_lam}{ind_delta}'));
        fclose(fid);
        getJava(fweight, 100)
        
        
        for a=1:Nactions
            for p=1:2
                f{p}{a} = (D{p}{a}+deltaA*I)\e{p}{a};
                F{p}{a} = G{p}{a}/(D{p}{a}+deltaA*I); %A/B is the matrix division of B into A, which is roughly the same as A*INV(B)
            end
        end
        [wlamlstd, err, errAction]=lamapi_tetris(Phi, piece, actionIndex, terms, F, f, deltaA, Niterations, numEpisodes);
    end
end




%using tabular representation for BI
% [wtab, R_apptab]= leastSquares(Phi_tab, R, 1.0);
% [wtab, R_apptab]= leastSquares(Phi_tab, R, 1e8);
% figure;hold all;grid on;
% color={'-k+', '--ro', '-gx','--bs', '-m<', '--yh', ...
%         '-r*', '--k.', '-bp','--ms'};
% for i=1:10
%     st = (i-1)*21 +1;
% 	ed = st + (21-1); 
%     plot(wtab(st:ed), color{i})
% end
% legend('column 1', 'column 2', 'column 3', 'column 4', 'column 5', 'column 6', 'column 7', 'column 8', 'column 9', 'column 10');
% 
% figure;hold all;grid on;
% for i=11:19
%     st = (i-1)*21 +1;
% 	ed = st + (21-1); 
%     plot(wtab(st:ed), color{i-10})
% end
% legend('column 1-2', 'column 2-3', 'column 3-4', 'column 4-5', 'column 5-6', 'column 6-7', 'column 7-8', 'column 8-9', 'column 9-10');


stop

% fid = fopen(fphinext);
% Phinext = fscanf(fid, '%21f', [21,inf]);
% fclose(fid);
% Phinext=Phinext';
% size(Phinext)


% 
% fid = fopen(fphinextBeforeErase);
% PhinextBefore = fscanf(fid, '%21f', [21,inf]);
% fclose(fid);
% PhinextBefore=PhinextBefore';
% size(PhinextBefore)


% fid = fopen(fpolicy);
% policy=fscanf(fid, '%d');
% fclose(fid);
% size(policy)
% 
% fid = fopen(fVs);
% Vs=fscanf(fid, '%f');
% fclose(fid);
% size(Vs)


Nfeatures = length(w_CE);

%zerolize terminal feature vector
% Phinext(terms,:) = zeros(length(terms), Nfeatures);
% PhinextBefore(terms,:) = zeros(length(terms), Nfeatures);
% assert(sum(reward(terms)) == -length(terms));



%check board: good
% indr1=find(reward==1);
% fprintf(board2Chars(board(31,:)))
% fprintf(board2Chars(boardBefore(31,:)))
% fprintf(board2Chars(boardAfter(31,:)))


w_CE=[-3.4668 -8.5783 -15.6468 -6.1543 -9.8836 1.4784 -16.2427 -9.3328 -3.4838 -9.9378...
    -15.8604 0.1275 -23.8577 0.8403 -8.8838 2.0852 -32.9442 1.6128 -26.2846 2.8297 -57.4184]';

w_CE01=[1.7355 -27.9023 -12.3488  -8.0775  -4.7457 -11.6872 -16.2636 ...
    -5.1657 -47.2480  23.9255 -23.0378   8.1120 -48.4653   5.6447 -24.1567   9.3576 -23.5505   4.4985 -11.2574   6.8310 -95.5623]';

w_CE02=[1.5081 -17.1254  -4.0356  -7.7953  -8.7444  -6.3575  -7.0459  -3.2795 -25.1985  11.5106 -19.4298   2.4666 -14.5301   3.3913 -29.3009   2.9572 -24.1761   3.5276 -20.0305   5.4368 -61.4840 ]';

w_CE03=[2.2319 -20.2263  -4.8077  -9.5949 -10.9941  -7.6284 -12.9424  -0.6077 -17.6192   0.9344 -15.5646   1.0775 -11.2141   1.7807 -23.1390   1.2176 -14.3875   2.1272 -25.0089   2.2120 -48.7180]';
% figure; hold all;grid on;
% plot(w_CE, '-k+');hold on;
% plot(w_CE01, '-ro');hold on;
% plot(w_CE02, '-gs');hold on;
% plot(w_CE03, '-bp');hold on;
Vs01 = Phi* w_CE01;
Vs02 = Phi* w_CE02;
Vs03 = Phi* w_CE03;
figure; hold all;
plot(Vs(1:terms(1)), '-k+');hold on;
plot(Vs01(1:terms(1)), '-ro');hold on;
plot(Vs02(1:terms(1)), '-gs');hold on;
plot(Vs03(1:terms(1)), '-bp');hold on;
% fprintf('%d weights in w_CE  is negative.\n', length(find(w_CE<0)));
% 
% %check Vs
% Vs01 = Phi * w_CE;
% assert(norm(Vs01-Vs)==0);

VsnextBefore = PhinextBefore * w_CE;
Vsnext= Phinext * w_CE;



% states = [1:Nactions:size(Phi,2)];
gamma =1;
I=eye(Nfeatures, Nfeatures);
MIN=-1e10;

%more efficient least-squares
D= Phi'*Phi;
% y=Phi'*Vs;
% w_ls = D\y;
% fprintf('training error for the least-squares procedure is %e\n', norm(w_ls - w_CE));
% cond(D2)%2e4. so condition number is not a problem for least-squares.




% deltaA=10
% deltaA=10000
% deltaA=100000
% deltaA=1
% deltaA=0.1;
deltaA=0


%Now let's try using gradV as the reward function to learn the LSTDlambda
%solution: Good. we can learn w_CE.  Now, next question, how to learn the
%reward function of CE (gradV)? Does this reward function make sense?
%this clearly shows the we only need a good reward function. 
%We can learn this reward function or simply by roll out. 
%BTW, this reward function should be revealed in the learning process of
%cross entropy. 

gradV=zeros(terms(length(terms)),1);
for ind_epi=1:numEpisodes
        if(ind_epi==1)
            sbegin=1;
        else
            sbegin=send+1;
        end
        send=terms(ind_epi);
        
        for s= sbegin : send
            if(s~=send)
                gradV(s) = Vs(s) - Vs(s+1);%for state s, we want the return to be V(s)
            else
                gradV(s) = Vs(send);
            end
        end
end

lambda=[0:0.2:1];

%1. using the difference of V as the reward; match the ls fit of V for
%lambda=1. good 
[wlstd_gradV, A_gradV,b_gradV]=lstd_tetris(lambda, Phi, Phinext, gradV, absorb, deltaA);
plotGame(Phi, PhinextBefore, Phinext, absorb, 1, wlstd_gradV{6}, '-b+' )

%2. using board after erasing as the next feature vectors; decreasing shape in global, but local shape is not correct 
[wlstd, A,b]=lstd_tetris(lambda, Phi, Phinext, reward, absorb, deltaA);
plotGame(Phi, PhinextBefore, Phinext, absorb, 1, wlstd{6}, '--ks' )
plotGame(Phi, PhinextBefore, Phinext, absorb, 1, wlstd{1}, '--kx' )

%3. using board before erasing as the next feature vectors; similar to 2.  
[wlstdBefore, A,b, R]=lstd_tetris(lambda, Phi, PhinextBefore, reward, absorb, deltaA);
plotGame(Phi, PhinextBefore, Phinext, absorb, 1, 10*wlstdBefore{6}, '--rs' );
plotGame(Phi, PhinextBefore, Phinext, absorb, 1, 10*wlstdBefore{1}, '--rx' );


R1 =R(1:terms(1)); 
Phi1 = Phi(1:terms(1), :);
w_R1 =  (Phi1'* Phi1) \ (Phi1'*R1) ;
R1_app = Phi1 * w_R1;
w_R=( (Phi'* Phi) \ (Phi'*R) );
R1_app_2 = Phi1 * w_R;
figure;title('game 1');
plot(R1, '-b+');hold all;
plot(R1_app, '--ko');hold all;
plot(R1_app_2, '--rx');hold all;

fweight=[dir 'w_fromfittingR1'];
fid=fopen(fweight,'w');
fprintf(fid, '%s', num2str(w_R1'));
fclose(fid);

%almost same
% R1 =R(1:terms(1)); 
% Phi1 = [Phi(1:terms(1), :), ones(terms(1),1)];
% R1_app_b = Phi1 * ( (Phi1'* Phi1) \ (Phi1'*R1) );
% plot(R1_app_b, '--gx');hold all;




%weights plot
figure; hold all;grid on;
plot(wlstd{1}, '-g+');hold on;
plot(wlstdBefore{1}, '-k+');hold on;
figure; hold all;
plot(w_CE, '-bo');


%using board as features to
% traindata = 1:terms(1);
% traindata = 1:terms(2);
traindata = terms(1)+1:terms(2);
% traindata = 1:terms(3);
% traindata = 1:terms(10);
% target = Vs(traindata); %looks good
target_train = R(traindata);% looks good
% target = reward(traindata);%fit the rewards
D1_board= board(traindata, :)'*board(traindata, :);
y1=board(traindata, :)'*target_train;
% deltaD=1;
% deltaD=100000;
deltaD=1000;
D1_delta = D1_board+deltaD*eye(size(board, 2));
cond(D1_delta)
w1_ls_board = D1_delta\y1;
% testdata = 1:terms(1);
testdata = terms(1)+1:terms(2);
target_test = R(testdata);
approx = board(testdata, :) * w1_ls_board;
figure;hold all;
plot(target_test, '-b+');
plot(approx, '--ko');
rms(approx, target_test)
fweight=[dir 'w1_boardfeatures'];
fid=fopen(fweight,'w');
fprintf(fid, '%s', num2str(w1_ls_board'));
fclose(fid);

%using board as features to evaluate CE policy
Nfeatures=size(board, 2)
boardAfter(terms,:) = zeros(length(terms), Nfeatures);%reset terminal features
boardBefore(terms,:) = zeros(length(terms), Nfeatures);


% A = board(traindata,:)'*(board(traindata,:) - boardAfter(traindata,:));
% b = board(traindata,:)'*reward(traindata);
% %fast lstd0
% w{1}=(A+deltaD*eye(Nfeatures))\b;
%incremental lstd
% [w, A,b, Ret]=lstd_tetris([0,1], board, reward, absorb, deltaD, 10);%lstd0, 1
[w, A,b]=lstd_tetris([0,1], board, reward, absorb, deltaD, 1);%lstd0, 1
R_app0 = board(traindata,:)*w{1};
R_app1 = board(traindata,:)*w{2};
rms(Ret(traindata), R_app0)
rms(Ret(traindata), R_app1)
figure;hold all;
plot(Ret(traindata), '-b+');
plot(R_app0, '--ko');
plot(R_app1, '--rs');
fweight=[dir 'w0_boardfeatures'];
fid=fopen(fweight,'w');
fprintf(fid, '%s', num2str(w{1}'));
fclose(fid);
fweight=[dir 'w1_boardfeatures'];
fid=fopen(fweight,'w');
fprintf(fid, '%s', num2str(w{2}'));
fclose(fid);


%this should be 0: checked, yes. 
% norm(D - A{6})

% figure(31);hold all;plot(w_CE, '-b+');plot(wlstd{ind_lam},'-kx');


%globally decreasing trend. good. (for gamma=1)
%values from step 1 to 50 is decreasing for the score function of CE, but
%for lstd, it is not. why? use gamma=0.9 doesn't work (which actually make
%the values for steps 1 to 50 increasing. why? setting gamma=1.1 has a
%decreasing trend, but then thenumberof holes get too large.



%now see if the CE reward can be approximated well by the features
%seems not. 
% gradV= gradV/ 1000;
% gradV_app= Phi* (D\(Phi'*gradV));
% figure; hold all; plot(gradV, '-b+'); plot(gradV_app, '-kh');

