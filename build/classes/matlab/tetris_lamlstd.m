%so far the learned w performs very poor. learning is slow. will rewrite in
%Java. One poss
%directions: 1. samples are not enough? 2. fitting Vs? first try to see the
%quality of Vs?
%evaluating board positions following the CE policy, i.e, trying to approximate V^{CE policy}
close all;
homedir = '/Users/admin/sztetris/';
%dir =[homedir 'samplesCE_BI_run00_']%[homedir 'samplesRandom_BI_100episodes_'];
% dir=[homedir 'CE_BI_run00_formatlab.txt_samples_'];
dir=[homedir 'CE_BI_run01_formatlab.txt_samples_'];

%using CE samples
fPhi= [dir 'phi.txt'];
fa = [dir 'action.txt'];
freward = [dir 'reward.txt'];
fPhinext= [dir 'phinext.txt'];
fpicece=[dir 'piece.txt'];
fboard=[dir 'board.txt'];

fid = fopen(fboard);
board= fscanf(fid, '%416d', [416,inf]);
fclose(fid);
board=board';
size(board)


fid = fopen(fpiece);
pieceCE= fscanf(fid, '%f');
fclose(fid);
size(pieceCE)

fid = fopen(freward);
reward= fscanf(fid, '%f');
fclose(fid);
size(reward)
min(reward)
max(reward)

fid = fopen(fPhi);
Phi=fscanf(fid, '%21f ', [21,inf]);
fclose(fid);
size(Phi)
Phi = Phi';

fid = fopen(faCE);
action= fscanf(fid, '%d %d %d', [3,inf]);
fclose(fid);
size(action)
actionIndex= action(1,:)+1;
fprintf('action index should starts from 1: %d\n', min(actionIndex));
% max(actionIndex)
%assert(norm(actionIndex-policy)==0);

fid = fopen(fPhinext);
Phinext=fscanf(fid, '%21f', [21,inf]);
fclose(fid);
size(Phinext)
Phinext = Phinext';

terms=find(absorb==1);
numEpisodes=length(terms);%number of games 
fprintf('There are %d episodes/games in the samples\n', numEpisodes);
Phinext(terms,:) = zeros(length(terms), Nfeatures);
PhinextBefore(terms,:) = zeros(length(terms), Nfeatures);
assert(sum(reward(terms)) == -length(terms));


w_CE=[-3.4668 -8.5783 -15.6468 -6.1543 -9.8836 1.4784 -16.2427 -9.3328 -3.4838 -9.9378...
    -15.8604 0.1275 -23.8577 0.8403 -8.8838 2.0852 -32.9442 1.6128 -26.2846 2.8297 -57.4184]';

%%%%%%LAM-API 
deltaD=0.1
% [F, f, D, e, G]=learnLam_tetris(Phi, piece,actionIndex, Phinext, reward,absorb, deltaD);
[F, f, D, e, G]=learnLam_tetris(Phi, piece,actionIndex, PhinextBefore, reward,absorb, deltaD);

Niterations=5;
deltaA=0;
[w, err, errAction]=lamapi_tetris(Phi, piece,action,  absorb, F, f, deltaA, Niterations);


%%%%%%%%lstd0
b0=Phi'* reward;
deltaA_array=[0 -1e4 -1e5 -1e6 -5e6 -1e7 -1e8];
for ind=1:length(deltaA_array)
    deltaA=deltaA_array(ind); 
%     A0{ind} = Phi' * (gamma*Phinext-Phi);
    A0{ind} = Phi' * (gamma*PhinextBefore-Phi);
    wlstd0{ind}=-(A0{ind}+deltaA*I)\b0;
    
%     fweight=[dir 'wlstd0Phinext_deltaA' num2str(deltaA)];
    fweight=[dir 'wlstd0PhinextBef_deltaA' num2str(deltaA)];
    fid=fopen(fweight,'w');
    fprintf(fid, '%s', num2str(wlstd0{ind}'));
    fclose(fid);

%     figure;hold all;grid on;
%     plot(w_CE,'-b+');
%     plot(50*wlstd0,'-rs');

%     plotGame(Phi, PhinextBefore, Phinext, absorb, 1, w_CE, '-b+' );
    plotGame(Phi, PhinextBefore, Phinext, absorb, 1, wlstd0{ind}, '-rs' );
     
    command=getJava(fweight,1);
    fprintf('%s\n', command);
end

cond(A0)%about 2000
D0 = Phi' * Phi;
cond(D0)%2e4
figure; imgagesc(D0);colormap('hot');colorbar;

Phi_h = Phi(:,[1:10,21]);
D0_h = Phi_h'*Phi_h;
cond(D0_h)%still 1e4
figure; imgagesc(D0_h);colormap('hot');colorbar;

%too slow
% u=rand(size(Phi,1),1);
% u=u/sum(u);
% D0_h = Phi_h'*diag(u)*Phi_h
% cond(D0_h)

% fweight=[dir 'w_NoresetAb_deltaA' num2str(deltaA) '_deltaD' num2str(deltaD) '_' num2str(Niterations) 'iterations' ];
% fid=fopen(fweight,'w');
% fprintf(fid, '%s\n', num2str(w'));
% fclose(fid);

