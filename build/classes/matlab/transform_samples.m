close all;
homedir = '/Users/admin/sztetris/';
dir =[homedir 'samplesCE_BI_run00_'];
% dir = [homedir 'samplesRandom_BI_100episodes_'];
% fphi=[dir 'phi.txt'];
% fpiece=[dir 'piece.txt'];
% fa=[dir 'action.txt'];
% fphinext=[dir 'phinext.txt'];
% fr=[dir 'reward.txt'];
% fabs=[dir 'absorb.txt'];
% fpolicy=[dir 'policy.txt'];

% 
% fid = fopen(fpiece);
% piece=fscanf(fid, '%d');
% fclose(fid);
% size(piece)
% 
% fid = fopen(fabs);
% absorb=fscanf(fid, '%d');
% fclose(fid);
% size(absorb)
% 
% fid = fopen(fr);
% reward=fscanf(fid, '%f');
% fclose(fid);
% size(reward)
% 
% 
% fid = fopen(fa);
% action = fscanf(fid, '%d %d %d', [3,inf]);
% fclose(fid);
% size(action)
% actIndex = action(1,:) +1;
% unique(actIndex)
% 
% 
% fid = fopen(fphi);
% Phi = fscanf(fid, '%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f ', [21,inf]);
% fclose(fid);
% size(Phi)
% 
% fid = fopen(fphinext);
% Phinext = fscanf(fid, '%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f ', [21,inf]);
% fclose(fid);
% size(Phinext)

% fid = fopen(fpolicy);
% policy=fscanf(fid, '%d');
% fclose(fid);
% size(policy)



w_CE=[-3.4668 -8.5783 -15.6468 -6.1543 -9.8836 1.4784 -16.2427 -9.3328 -3.4838 -9.9378...
    -15.8604 0.1275 -23.8577 0.8403 -8.8838 2.0852 -32.9442 1.6128 -26.2846 2.8297 -57.4184]';
% [maxwCE, ind_maxwCE] = max(w_CE);
% [minwCE, ind_minwCE] = min(w_CE);

Nactions=40;
Nfeatures = size(Phi,1);
states = [1:Nactions:size(Phi,2)];
gamma =1% 0% 0.9;
deltaA=0%-1;%-0.1%-1;%-0.1;

terms=find(absorb==1);
Phinext(:,terms) = zeros(Nfeatures, length(terms));
sum(reward(terms)) == -length(terms)


T=100;
err=zeros(T,1);
w= rand(size(Phi,1), 1);%w_CE;
amax=-ones(length(states),1);
Vmax=-100*ones(length(states),1);
for iter=1:T
    A=deltaA* eye(length(w), length(w));
    b=zeros(length(w),1);
    for ind=1:length(states)
        s = states(ind);
        phi_s = Phi(:,s);
        
%        V_s = phi_s'* w;
%         Vmax=-1e10;
%         amax=-1;
%         for aind=0:Nactions-1
%             if absorb(s+aind)==1
%                 phinext_sa =zeros(Nfeatures,1);
%                 %fprintf('s+aind=%d is terminating\n',  s+aind);
%             else
%                 phinext_sa = Phinext(:,s+aind);
%             end
%             Vnext = reward(s+aind) + gamma * phinext_sa'*w;
%             if(Vnext>Vmax)
%                 Vmax = Vnext;
%                 amax=aind;
%             end
%         end
%         r_s(ind) = max( reward(s:s+Nactions-1));
%         if(r_s(ind)==-1) r_s(ind)=0; end
%         b = b + phi_s*r_s(ind);

        [Vmax(ind), amax(ind)]=max( reward(s:s+Nactions-1)+ gamma*Phinext(:,s:s+Nactions-1)'*w );
        line=s+ amax(ind)-1;
        b = b + phi_s* reward(line);
        A = A + phi_s* (gamma* Phinext(:,line)  - phi_s)';
    end
    w_new = -A\b;
    err(iter) = norm(w-w_new);
    w = w_new;
end
err
w
% Vmax
% amax


% fweight=[dir 'w_CE_BI_run00.txt'];
% fid=fopen(fweight,'w');
% fprintf(fid, '%s', num2str(w'));
% fclose(fid);

fweight=[homedir 'w_LAMAPI_fromRandomsamples_BI.txt'];
fid=fopen(fweight,'w');
fprintf(fid, '%s', num2str(w'));
fclose(fid);

% rapp=Phi(:,states)'*w;
% figure;hold all;
% plot(r_s,'-b.');
% plot(rapp,'-k+');

figure;hold all;
plot(w_CE, 'b.'); 
plot(w, 'k+'); 


